export const SITE = {
  author: 'Kosmokritik',
  title: 'L’Épreuvre de l’individu',
  description: 'L’Épreuvre de l’individu',
  url: 'https://www.antilivre.org/lepreuvre-de-lindividu/',
  titlelink: 'https://abrupt.cc/marc-verlynde/lepreuve-de-lindividu',
  lang: 'fr',
  locale: 'fr_FR',
  warning: 'warning',
  credits: 'Antitexte&nbsp;: Marc Verlynde<br>Infosculpture&nbsp;: AAA'
};

// // Theme configuration
// export const PAGE_SIZE = 10;
export const THREE_SITE = true;
