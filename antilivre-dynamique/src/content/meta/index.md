<div class="epigraphe">

*Je ne me suis pas franchi.*

&mdash;&nbsp;Pierre Cendors, *Seuil du Seul*.

</div>

<div class="text" data-id="0">

# D’un non de l’individu

On commencerait par l’ignorance. Nous ne voulons pas savoir qui est l’individu, nous ne pouvons savoir qui en parle.

<div class="notemarge"><a class="notemarge__lien" href="#m01-l" id="m01-n"><sup>&ast;</sup></a>À l’œuvre, une logique de jeu de mots : aucun non, non plus. Nous ne confions l’épreuve de l’individu à aucune définitive négation. Seulement une suite d’impasses, d’apparitions en redéfinition.</div>

Un On derrière lequel aucun nom<a data-link="01-a" id="l01-a" href="#l01-b"><sup>α</sup></a> ne se cache<a class="notemarge__lien" href="#m01-n" id="m01-l"><sup>&ast;</sup></a>. L’individu, une question corollaire : quel est ce On qui parle, de qui usurpe-t-il, limite-t-il, l’identité ; qui est-ce qui parle ?

Affirmons notre ignorance totale, définitive. Non tant faute de vouloir en approcher les instances, les masques, les surgissements, mais pour en maintenir pluralité : <span id="contradictions">contradictions</span> et flux ; variantes et vides. Nous n’en savons rien, rêvons que ne s’y immisce aucune instance supérieure --- conscience surplombante --- nul individu, <span class="citation">cette <span id="pauvre">pauvre</span> vague dans le flot du devenir</span>, n’y transparaîtrait, transfiguré ou rédimé.

<div class="auteur">
[#Nietzsche]
</div>

La pensée, différenciée dans une écriture, mobiliserait des mécanismes mentaux, des constellations<a data-link="02-a" id="l02-a" href="#l02-b"><sup>β</sup></a> de mots où se figerait ce que nous sommes au moment de cette affirmation. <span class="citation">D’un objet sans nom nous ne savons que faire.</span>

<div class="auteur">
[#Leiris #Blanchot]
</div>

Rien, un vide, du concret par dépersonnalisation, un *estrangement*, une absence de Soi à Soi ; la manifestation d’une inadéquation, d’impossibilités essayées. Mirages dont dire la pluralité des noms.

Saper le fascisme de la langue, la détourner de ses précisions. Une phrase a sujet et objet : à toi, je parle. L’épreuve de l’individu ici traversée serait détournement de l’incertitude pronominale. À toi, je parle<a data-link="03-a" id="l03-a" href="#l03-b"><sup>γ</sup></a>, à l’écrit, qui est qui ? Toi, je, nous, on, tu<a data-link="04-a" id="l04-a" href="#l04-b"><sup>δ</sup></a> et vous, au fond, la même absence. On parle à cette part de toi qui ne sait pas qui elle est. « On » est miroir d’ondoyance, aveu d’ignorance, un hasard sémantique pour atteindre une langue sans propriété ni propriétaire. Un flottement.

<div class="auteur">
[#Barthes #Montaigne]
</div>

J’emploie ici les pronoms par distanciation, désappropriation, altération, et signe vers la confusion. Si je dis : nous n’y comprenons rien, je te prête un ressenti sensiblement similaire à celui que je prends pour mien. À l’essai ici, les pronoms, comme<a data-link="05-a" id="l05-a" href="#l05-b"><sup>ε</sup></a> les moins mal venus, font signe vers la part d’incertitude, de pluralité, d’inquiétude, de flottement et de revenance, de cette parole de désappropriation. Pas plus qu’ils ne la désignent, ils n’en recouvrent la signification.

Pour mieux ne pas savoir de quoi on parle quand on dit individu, on écoute les substitutions de ce mot, un miroir des conceptions changeantes qui en préservent les altérations, inventent des variations[^1-lepreuve-de-lindividu_1]. Manière de ne pas savoir qui en parle. Forme la plus évidente de l’épreuve de l’individu.

<div class="auteur">
[#Raphmaj]
</div>

Tout ce que je dis, virtuellement, concerne quelqu’un d’autre.

On ne saurait dire Moi tant des milliards de Moi, précédemment et simultanément<a data-link="06-a" id="l06-a" href="#l06-b"><sup>ζ</sup></a>, ont informé la conception des stimulants simulacres de ce terme. Le Moi est somme de <span id="piratages">piratages</span>, amalgame de constructions sociales et de désirs d’émancipation, de diktats et d’instants où leur vacuité happe, rattrape et revient aussi.

Nous ne saurions penser seuls. Se réclamer d’une spécificité, ostensiblement cachée derrière l’incompréhension, ramènerait à l’incertitude de l’individu. Pourtant, les pronoms aident à miroiter (comprendre spéculer : figurer une <span id="absence">absence</span> de reflet) cette incertitude. Une part indistincte de Moi réfléchit l’individu à partir de ce qu’il prend pour son ressenti propre, qu’il s’essaie à outrepasser.

<span class="citation">S’excepter</span> de cette pensée séculaire : implication, explication. Partir de ce que l’on sait, ou sent, pour forger une forme, une formulation mentale et verbale, susceptible de prendre en compte l’autre, de se plier aux canons de la compréhension, de la stricte, spécieuse<a data-link="07-a" id="l07-a" href="#l07-b"><sup>η</sup></a>, transparence entre Moi et Toi, entre ce que je dis et ce que tu comprends, entre ce que je dis et ce que je veux dire, entre ce que tu entends et ce que tu veux entendre, entre ce que je dis et ce que tu veux que je dise. Éternel interstice du sens.

<div class="auteur">
[#Surya]
</div>

L’épreuve de l’individu, s’écarter de l’illusion de l’<span id="inedit">inédit</span><a data-link="08-a" id="l08-a" href="#l08-b"><sup>θ</sup></a>.

On réduit, on simplifie, on veille à se croire compréhensible. Se méfier, se méprendre, de ce nouveau renversement : explication, implication. Se définir par ce que l’on parvient à expliquer, donner une valeur de part commune à ce qu’un raisonnement aurait fini par créer, par grever ? Une <span id="impasse">impasse</span>, une issue ? Un essai.

Varions l’incertitude pronominale : entre Je et Je aucune adéquation, pas plus, pas moins, de similitude qu’entre Toi et Moi. Une différence qui fait <span id="lien">lien</span>. Pas plus que toi, je ne sais où je vais. Est-ce si assuré ?

Tout cela pour te dire (lectrice & lecteur) que les altérations pronominales sont concertées. On y laisse une part de distraction, d’aléatoire, de poétique serais-tu même prêt à dire. Un pronom plutôt qu’un autre : une question de rythme, de son, de hantise.

Individu, je ne me pense pas seul. On en effleure les limites dans une informe spontanéité. On rêve --- répétition et redite --- d’épuiser ce que l’on a à dire.

On pourra(it) se confier à la <span class="citation">constante infortune de l’écriture automatique</span> comme, d’ailleurs, une sorte de retour différencié de ce qui a constitué celui que je semble être.

<div class="auteur">
[#Breton #Leiris]
</div>

Ne pas se limiter à une identité indiquerait, malgré tout, que cette identification à un nous-mêmes passe par une évidente continuité de méthodes, de préoccupations --- le retour à vide des mêmes hantises. Épargne-nous tes propres fixités.

Plus décisif, crois-tu, en regard avec cette indétermination pronominale apparaissent les <span id="trouees">trouées</span> de la grisaille. Ce marqueur typographique signifie qu’une autre voix que la mienne parle, que j’en ai conscience.

<div class="notemarge"><a class="notemarge__lien" href="#m02-l" id="m02-n"><sup>&ast;</sup></a>L’épreuve de l’individu consiste à considérer l’illusoire duperie avec laquelle nous nous envisageons, avec laquelle nous jouons de toutes ces idées que nous ne souhaitons faire <span id="notre">nôtre</span>.</div>

Faire comme si<a data-link="09-a" id="l09-a" href="#l09-b"><sup>ι</sup></a> tout partait d’une scène initiale, faire comme si<a data-link="05-b" id="l05-b" href="#l05-a"><sup>ε</sup></a><a class="notemarge__lien" href="#m02-n" id="m02-l"><sup>&ast;</sup></a> on déjouait ainsi l’aspect autobiographique (écriture, toute seule, automatique, d’un Je sans identité définitive, sans limites, utopie, avec un inconscient sans forme fixe) de tout essai. Le moment, qu’importe où et quand, où soudain tu te rends compte que tu ne possèdes aucune individualité, aucun noyau perceptif propre, que l’histoire de tes incompréhensions et de tes mystères t’indiffère et t’exproprie au point de ne plus t’appartenir, pourrait ne pas concerner un autre, mais tous les autres. À peine un <span class="citation">néant au travail</span>.

<div class="auteur">
[#Blanchot]
</div>

En commun, cet instant où l’on n’est plus personne, où l’on ne sait quel pronom pourrait désigner ce noyau de confusion qu’il serait mal venu d’appeler ton intériorité et qui demeure le plus secret, le plus problématiquement vrai peut-être au sens du plus dissimulé de ce que, éperdument, tu es.

Ce n’est que cela, un vertige à vide, une perte plénière, que nous voulons faire voir, maintenir dans son altération. La suite indistincte, et aléatoire, de pronoms ne parle que dans cet instant sans Soi. La majuscule signale que Soi, amalgame de tous les pronoms, signe vers le vide qui en est l’appréhension la plus commune, est peut-être la forme la moins incertaine de cet outrepassement.

Soi : un Moi qui rentre en contact avec un toi, qui s’effleurent et se reconnaissent dans l’absence ? Sans doute aussi.

</div>

<div class="text" data-id="1">

# La mort de l’individu

L’individu existerait-il en s’inventant d’autres origines, en tentant de les repousser plus loin, plus avant, vers un ailleurs, possiblement primitif ? <span class="citation">Tu es, si tant est que cette possibilité existe, la possibilité de faire un commencement.</span> Peut-être n’apparaîtrait-il ici, sous sa forme la plus transitoire, la plus informe hors l’inquiétude qui l’anime.

<div class="auteur">
[#<span id="kafka">Kafka</span>]
</div>

L’individu, expropriation, mortifère saisine de ce qu’il croit être.

<span class="citation">Que personne ne soit né, et puis après ?</span>

<div class="auteur">
[#<span id="marias">Marías</span>]
</div>

L’occasion d’une nouvelle précision, d’une précaution nouvelle. On pressent l’individu dans ce que l’on tente d’en figurer, dans l’invention *in absentia*, dans les écrits des auteurs où des individus racontent leur approche de l’impersonnel, <span class="citation">ce remuement</span><a data-link="10-a" id="l10-a" href="#l10-b"><sup>κ</sup></a> <span class="citation">vide où j’évoluais.</span>

<div class="auteur">
[#<span id="blanchot">Blanchot</span>]
</div>

Hors « son » <span id="evanouissement">évanouissement</span><a data-link="11-a" id="l11-a" href="#l11-b"><sup>λ</sup></a>, l’individu n’a aucun intérêt.

<div class="notemarge"><a class="notemarge__lien" href="#m03-l" id="m03-n"><sup>&ast;</sup></a>Nous témoignons seulement de la mort d’<span id="autrui">autrui</span>. Souvent, mal, à partir de Soi, pour dire ce que cette abstraite absence est et sera pour nous. Épreuve momentanée, redondante pourtant dans son temps étiré d’une partition de Soi.</div>

On ne sait rien de notre propre mort, on ne la raconte pas[^1-lepreuve-de-lindividu_2], on la vit sous la sujétion d’un pronom incertain. Cette délocution<a class="notemarge__lien" href="#m03-n" id="m03-l"><sup>&ast;</sup></a> du récit de l’instant décisif serait l’expérience littéraire même. Parler de ce que l’on ne saurait vivre et qui, pourtant, nous définit, nous relie, nous fait communiquer dans l’évanouissement de la conscience.

*La mort d’Ivan Ilitch*.

Un avènement qui serait déjà son point terminal. L’approche de la mort, le sens souverain prêté à l’agonie, conduit, pas seulement ici, à une prise de conscience bourgeoise, individuelle : ce que l’on est au moment de l’abandonner. Fumerolle mortifère de regrets. <span class="citation">En outre, on y lisait une sorte de reproche ou d’avertissement à l’adresse des vivants.</span>

<div class="auteur">
[#<span id="tolstoi">Tolstoï</span>]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m04-l" id="m04-n"><sup>&ast;</sup></a>Nous ne voulons pourtant point détromper le lecteur, tirer fierté, existence, de faire chuter ses illusions. Ensemble, nous nous trompons. Cette confusion sans doute se nomme individu.</div>

Énoncer revient à pointer les limites de la vérité<a class="notemarge__lien" href="#m04-n" id="m04-l"><sup>&ast;</sup></a> : dans un automatisme harassé, individu s’accole à moderne, à une centralité de la prise de conscience individuelle.

Si l’individu s’énonce dans la certitude de sa propre mort, autant la percevoir comme absence de survie, altération définitive de la matière à laquelle il se réduit. La mort, l’athéisme, l’individu : vertige sans espoir. Mais, dérivation et aimantation, aussi. Les substitutions à ce que l’on ne saurait être.

L’individu dans l’évidence de ses échappées.

La <span id="litterature">littérature</span> serait saturée d’instants de mort, de virevoltes autour de son point aveugle, terminal : <span class="citation">les trompettes d’alarme du néant.</span> Nous n’en ferons le catalogue ; nous n’épuiserons rien. On effleurera l’absence de ceux qui les ont réfutés, s’en sont écartés, en ont inventé des figurations autres, ont relié un vécu invivable avec ce que nous, maintenant, nous pouvons en concevoir.

<div class="auteur">
[#Kafka]
</div>

<span class="citation">Et cette chose effroyable pour lui était accompagnée d’une autre torture : il avait peur, il voulait y entrer lui-même, et cependant il résistait et, en luttant, s’enfonçait toujours davantage.</span>

<div class="auteur">
[#Tolstoï]
</div>

Si nous parlons de mort, c’est pour situer des revenants, des fantômes, des dédoublements et autres hasardeuses corrélations qui, un instant avant d’être rattrapés par le doute (l’autre nom idéal<a data-link="12-a" id="l12-a" href="#l12-b"><sup>μ</sup></a> de l’individu ?), permettraient de s’échapper de la causalité de la linéarité. Peut-être est-ce cela, la littérature.

La mort, la modernité. Le roman<a data-link="13-a" id="l13-a" href="#l13-b"><sup>ν</sup></a>, l’individu modernes, s’inventent à partir de cette possibilité éperdue : raconter la vie d’un individu, d’un anonyme, croisé par hasard, dont l’histoire ne sera que récits suspendus, autrement que comme une suite, linéaire, de moments reliés entre eux par la ligne coupable de cause à effet.

<div class="auteur">
[#Diderot]
</div>

Posons cette hypothèse sans inédit<a data-link="08-b" id="l08-b" href="#l08-a"><sup>θ</sup></a> : l’individu moderne dote son existence d’importance non tant en la soumettant à une sanction commune, mais quand il prétend ne plus s’y astreindre pour mieux imposer son propre regard, son propre jugement sur lui-même.

Ou pour le dire autrement, toujours avec cette incertaine absence d’inédit qui caractérise toute appréhension : l’individu s’écrit à l’épreuve d’une émancipation paradoxale.

Si l’Histoire est perpétuel retour, je ne pense aucunement ce retour désirable, envisageable autrement que dans l’écriture, une fiction, le regret ironique[^1-lepreuve-de-lindividu_3] de ce qui aurait pu avoir lieu. Une réaction à toute pensée réactionnaire.

Dans un strict<a data-link="07-b" id="l07-b" href="#l07-a"><sup>η</sup></a> refus de l’appropriation, empruntons le mythe réactionnaire de la Chute. Pour apparaître, l’individu serait dévoyé, abîmé dans le péché, sali par la connaissance de lui-même, de sa nudité. Les mythes fondateurs partagent ce regret de l’unité, le transforment en aristocratique âge d’or. Si nous parlons de ce mythe dont nous ne voulons rien savoir<a data-link="14-a" id="l14-a" href="#l14-b"><sup>ξ</sup></a>, c’est pour évoquer ses rituels et redites qui en actualisent l’ardeur. La Révolution française dirait autrement la Chute, à nouveau dramatiserait le tabou, ou le totem, de la connaissance de Soi.

<div class="auteur">
[#Freud]
</div>

L’épreuve de l’individu aucunement ne se situe dans une connaissance pécheresse, tentante dès lors. La non-linéarité à l’essai tiendra(it) à la manière (toujours à inventer plutôt que d’en rendre compte) dont un événement sans cesse se renverse, se déconstruit en d’autres sens, se biffure aussi en soupirs et repentirs.

<div class="notemarge"><a class="notemarge__lien" href="#m05-l" id="m05-n"><sup>&ast;</sup></a>Pour que ce texte soit épreuve, chacune de ses propositions devrait s’accompagner de cette question : et pourquoi pas ? Et pourquoi pas ?</div>

L’individu se spécule par scissions successives, infimes, quantiques comme cette constitutive matière noire (pourquoi pas<a class="notemarge__lien" href="#m05-n" id="m05-l"><sup>&ast;</sup></a> ?) dont plus on s’approche plus le sens échappe. Des substitutions, de partielles --- et pirates --- appropriations de cette naissance de l’individu dans la Chute. L’effacement, pour ce que nous en empruntons à Georges Bataille, de son aspect sacrificiel, de son anéantissement de se sentir chuter dans le ciel. Tu l’auras compris, par un facile retournement, penser la naissance et la mort de l’individu est similaire détour face à ce que l’on ne saurait approcher ailleurs que dans un jeu de dédoublements.

Parler de l’autre en nous-mêmes.

<div class="auteur">
[#Bataille]
</div>

Cette appropriation de la Chute s’éclaire de ses célestes contradictions. Un mythe apparaît dans sa clandestinité, dans la communication retardée de son échec, à sans cesse réamorcer. Penser la mort comme expérience collective, un non-savoir comme ferment social. Une décapitation pour, le jour anniversaire de la mort de Louis XVI, fonder une communauté sur cette expérience d’un savoir outrepassé (nous choisissons de le limiter à une expérience de dépersonnalisation, une épreuve de l’individu), jamais plus peccamineux, mais lumineux, à la lettre, céleste.

<div class="auteur">
[#Bataille]
</div>

Pour parler d’une mort qui ne se limiterait pas à son appréhension personnelle, ne se cantonnerait pas à une fascination morbide, inventons des substitutions, échappons à la fixité. La mort, expérience irrémédiable, intimerait une perception impossible : un franchissement de la subjectivité.

<span class="citation">Nous sommes deux abîmes face à face --- un puits contemplant le Ciel.</span>

<div class="auteur">
[#Pessoa]
</div>

On n’est plus personne, on ne peut plus s’exprimer que comme quelqu’un d’autre, comme personne.

Sous l’improbable masque de l’œil pinéal, toujours comme expérience vécue, Bataille illumine la Chute comme chute dans le ciel. Appréhension autre de l’espace, de la place de l’individu. Un échec peut-être dont le ressenti est pluralité. Autant de secrètes correspondances avec un ressenti personnel de l’impersonnel. Une suite de coïncidences, de tangences entre les textes comme nous nous approchons de ce que nous sentons ne pas être par la <span id="revenance">revenance</span> de nos béances. Ou, plus ombrageusement<a data-link="15-a" id="l15-a" href="#l15-b"><sup>ο</sup></a> : avant la scission, les contradictions.

<div class="auteur">
[#Bataille]
</div>

L’individu ou la permanente prescience d’une chute inadvenue. Vain sortilège contre son ordinaire, inéluctable, déchéance.

D’emblée une pluralité, si on se penche non tant sur l’individu que sur le miroir d’encre où la littérature rend compte de sa dissipation. Après la Chute, l’individu moderne s’invente d’autres culpabilités pour faire société. Dédoublement, l’ombre des Lumières : *Le Contrat Social* et *Les Confessions*. Dans l’amalgame entre celui qui s’excitait de recevoir une fessée pour une faute qu’il n’avait pas, tout à fait, personnellement, commise et celui qui, simultanément, ferraillait contre la propriété privée au nom d’un douteux état de nature, d’une regrettable pureté primordiale<a data-link="16-a" id="l16-a" href="#l16-b"><sup>π</sup></a>.

<div class="auteur">
[#Crevel #Rousseau #Tolstoï]
</div>

La parole sur Soi placerait l’individu dans l’examen de sa culpabilité, dire sa mort reviendrait à en retracer les motifs, leurs inscriptions collectives de figurations inventées, comme pour se prémunir du <span class="citation">seul vrai but de la détestable vie.</span>

<div class="auteur">
[#<span id="baudelaire">Baudelaire</span>]
</div>

L’individu est sa contradiction.

Peut-être s’est-il forgé dans l’idée de penser contre, de contredire pour s’inventer d’autres origines. Figure attendue du discours qui prétend réfuter en opposant un contre-exemple, un précédent. Démarche idiote, plate affirmation d’une singularité de pensée dont s’extraire.

Superposition et non opposition : simultanéité de la non-linéarité. *Jacques le Fataliste et son maître* à côté de *Rousseau juge de Jean-Jacques* : <span class="citation">acharnés à écrire contre l’écriture.</span> Le roman et l’essai pour dire l’individu dans sa confusion, comme rencontre, mise en relation. Des liens aussi hasardeux, contingents, que la rencontre de l’incipit de l’intriqué roman<a data-link="13-b" id="l13-b" href="#l13-a"><sup>ν</sup></a> de Diderot. Une bifurcation à ce que l’on croit savoir, la certitude que ce que l’on dit a déjà été dit, trouve des ressemblances, inventer d’autres filiations pour s’y soustraire, pour résister à la fixité, à cette mort dont nous nous détournons pour moins mal, qui sait, la dire.

<div class="auteur">
[#Rousseau #Diderot #Blanchot]
</div>

De clandestines correspondances, des miroirs sans morbidité, de hasardeuses anamorphoses autour de l’instant où, pour pallier la certitude de sa propre mort, pour se soustraire à son caractère individuel, l’individu invente, par écrit, l’instant où il aurait pu s’affirmer autre.

Mettre en correspondance, en confusion, des hiers troubles pour ombrager<a data-link="15-b" id="l15-b" href="#l15-a"><sup>ο</sup></a> l’informe aujourd’hui. En donner une autre image. Une pensée hantée par les retours, qui se veut consciente de ceux qu’une époque impose, qui se croit intempestive de renier les références qu’un moment historique suscite.

<div class="auteur">
[#Bataille]
</div>

Par facilité, sans doute aussi pour imposer la pertinence des modèles que l’on y a reconnus, cette vision est devenue largement majoritaire : un écrivain s’inspire d’abord de ses prédécesseurs, les imite, les pastiche, s’en déprend, fraye dans son milieu et son champ culturel, et finit par exprimer sa singularité, à informer et déformer la place qu’il aurait, se créerait et surtout que plus tard on pourra(it) lui reconnaître, ultime, et inutile, récompense posthume.

On ne saurait échapper à cette façon de penser, on l’empruntera avec cette même incroyance qui caractérise notre rapport à la mort. On n’y croit jamais entièrement, elle touche à un autre nous-mêmes, une part quasi fictive. Et pourtant...

<span class="citation">Ce que seray doresnavant, ce ne sera plus qu’un demy estre, ce ne sera plus moy. Je m’eschape tous les jours et me desrobe à moy.</span>

<div class="auteur">
[#Montaigne]
</div>

L’essai est artefact d’esquives de la mort, des fatalités de la pensée. On y emploie des personas comme miroirs menteurs de l’autoportrait, un écart à l’image de Soi qui y subsiste par hantise de la mort. Ou plutôt il s’agit là d’un des canons imposés par Monsieur de Montaigne dont la pensée française peine à se déprendre. Pas entièrement à tort, le paradoxe est là, un interstice où s’immisce l’individu. La question que lui pose, sans trêve, l’idée de la mort reste de savoir ce qu’il peut perdre, le peu qu’il peut revendiquer comme sien.

<div class="auteur">
[#Montaigne]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m06-l" id="m06-n"><sup>&ast;</sup></a>Expression employée pour se détourner de l’émancipation de cette culpabilité. Pensons outre un individu par essence coupable. Nous échouerons sans doute, mais déjouerons ainsi la fatalité.</div>

Si on fait un détour par Montaigne, ce serait comme désincarnation du fantôme qui ne cesse après lui de spéculer le sujet. La hantise de la coupable<a class="notemarge__lien" href="#m06-n" id="m06-l"><sup>&ast;</sup></a> escrivaillerie. Avancer toujours pour éclairer une démarche que l’on trouve en restant à cheval, en se soumettant soigneusement au défaut d’assiette, en esquivant la Chute.

<div class="auteur">
[#Montaigne]
</div>

Histoire non circulaire, celle qui pose des hypothèses, des rapprochements qui ne seraient que des lapsus, des hypothèses non réalisées, quelque chose comme de l’avenir dans le passé, un échappement à la mort. <span class="citation">Où est cette mort qui est l’espoir du langage ?</span>

<div class="auteur">
[#Blanchot]
</div>

Une image, une fiction : Montaigne dans sa bibliothèque, peut-être même dans son alcôve où sans se départir de son *otium*, assiste à la messe, écrit son autoportrait avec la guerre de religion autour de son clocher. L’invention de Soi par l’appropriation d’une langue nouvelle, quotidienne, par le refus, par sa pratique de l’examen de conscience qui marque la naissance du protestantisme. Peut-être est-on marqué aussi durablement par ce que l’on combat que par ce que l’on affirme.

La mort à l’œuvre interrogerait les fantômes et autres ombres qui, aujourd’hui, en animent les clairs-obscurs. Essayer cette manière de spéculer pour en souligner les impasses, pour pouvoir penser littéralement contre, toujours.

L’épreuve de la vie : continuer à se contredire.

La force opérante du cliché[^1-lepreuve-de-lindividu_4], l’emprunter comme façon de s’en extraire. Ici s’essaie une pensée spontanée, une tentative d’invalider les automatismes. *À la recherche du temps perdu* à la mort d’Alfred Agostinelli ; les *Essais* à celle de La Boétie. Stupide, efficace, réducteur, comme de penser que l’on ne comprend la mort que dans la perte, que l’on aurait besoin de vivre celle d’autrui pour en comprendre l’inéluctable. La mort n’a pas besoin de modèle.

<div class="auteur">
[#Proust #Montaigne]
</div>

La littérature se meurt de sa pratique rituelle de son pouvoir élégiaque, se condamne à un perpétuel chant du cygne, nous réduirait à en faire l’archéologie, à l’envisager comme un objet patrimonial, culturel.

Là encore, prendre garde à ce qui serait les pudeurs de nos fantômes, les silences que les auteurs n’ont pas su dire : l’individu dans ses dénis, que l’on se croirait malin de débusquer comme on exhume des vérités, comme on se repaît des cadavres dans le placard.

Avancerait-on réellement si on parlait de Montaigne et de sa belle-fille, une image pour ses proches, l’ombre du père et la maladie de la pierre ? Le combat contre une fatalité héréditaire, l’autre nom de l’obsession de la mort serait-il de donner un autre visage, d’inventer une autre fiction à son versant pratique, l’agonie physique, la douleur ? Quelque chose dont il reste indécent de parler.

<span class="citation">--- « Mais pourquoi ces souffrances ? » --- « Mais comme cela, sans raison aucune ».</span>

<div class="auteur">
[#Tolstoï]
</div>

On ne serait censé n’être choqué par rien. Ruse pour nous faire croire l’émancipation arrivée à son terme. On peut tout dire, personne ne sera touché dans ses intimes tabous. Pourtant, si on fixait l’image d’un individu seulement visible quand, si perclus de souffrances, son seul espoir demeure de sortir de son isolement corporel, fût-ce au prix de l’évanouissement de la conscience, et que ces souffrances sans mots soient l’emblème de notre condition humaine, cette image proposerait un nouvel asservissement à la douleur comme aune de la réalité d’une situation.

L’individu, une mauvaise idée. La preuve, elle prend <span id="coupablement">coupablement</span> corps. L’individu serait la désincarnation, toujours défaillante, d’une identité. L’individu, dès l’origine, gît dans la croyance au secret dont la fin serait le révélateur, pire, l’épreuve de vérité.

L’individu, l’intimité, l’intériorité.

Le corps comme frontière primordiale de l’individu, sans doute parce que l’on peine encore à l’envisager autrement que comme le réceptacle de l’âme. Peut-être une façon de rendre la mort immatérielle.

Un souffle qui s’en va.

La modernité individuelle commence quand on se met à croire que ce souffle, l’évanouissement que nous sommes<a data-link="11-b" id="l11-b" href="#l11-a"><sup>λ</sup></a>, ne va plus s’amalgamer à un grand Tout, rejoindre un inhabitable paradis, surtout de penser que s’y sauvegarderait notre périssable individualité. La question de l’individu ne revient-elle pas à se séparer de Soi pour être davantage, autrement, mieux pour les plus fous, Soi ?

<div class="auteur">
[#<span id="dostoievski">Dostoïevski</span>]
</div>

<span class="citation">En d’autres termes, la création ne serait-elle pas la chute de Dieu ?</span>

<div class="auteur">
[#Baudelaire]
</div>

L’individu, réminiscence de l’hubris ? Revenance d’un désir de mort qui, exprimé, en conjurerait la crainte. Représenter une peur serait la précipiter<a data-link="17-a" id="l17-a" href="#l17-b"><sup>ρ</sup></a>, la conjurer sans doute aussi en lui donnant un autre visage sans tout à fait croire parvenir à y échapper[^1-lepreuve-de-lindividu_5]. Explication sans doute courte, comme un craintif dérivatif de l’omniprésence de la mort : choisir sa mort comme affirmation de ce que l’on pourrait alors prendre pour sa personnalité propre ?

Face à la mort l’individu se penserait dans une fiction minimale : à partir de la noyade (en taisant pourtant le moment où cède la volonté de survivre), le récit d’une vie qui, à l’ultime instant défilerait devant nous, s’est imposé. Difficile de s’extraire totalement de l’illusion biographique : au dernier moment chacune de nos actions sera pesée, évaluée, rendue à sa véritable valeur, à son intelligibilité.

L’individu, le mythe d’un attachement à un récit cohérent.

<div class="auteur">
[#Bourdieu]
</div>

Le récit le plus cohérent qui informe (au sens qui le met en forme, le manifeste, le trahit, le rend exagérément intelligible) l’individu serait le suivant : en tant que centre absolu de l’univers, il sera(it), dostoïevskien démon, le créateur et le prouverait en se détruisant, en se tuant, lui-même.

On connaît le paradoxe, on le sait insoluble, on continue à se fonder sur son impossibilité : dans un geste plus ou moins suicidaire, une déclaration de principe plus ou moins suivie d’actes, se tuer pour devenir dieu en gardant sa personnalité.

La littérature se confond avec ce désir diabolique de survivre, braver la mort pour conserver sa personnalité. Éprouver la certitude, idiote, que si nous avons, par instant, une personnalité, elle ne nous apparaît que quand elle est insupportable, sans issue. Physiologiquement, à défaut de philosophiquement, envisager la mort sous son aspect corporel ouvre la possibilité d’une conciliation : il est temps de s’effacer, de se résoudre à n’être plus personne avant de n’être rien.

Si philosopher, c’est apprendre à mourir, ne se prépare-t-on pas à cette absence à Soi, à s’en prémunir, à en faire l’épreuve, à la mettre en représentation, à distance dans une forme anodine qui possiblement différera de ce que, douloureusement, nous en vivrons. Peut-être d’ailleurs, dans ces <span id="moments">moments</span> où nous ne sommes plus personne, l’avons-nous déjà vécue.

<div class="auteur">
[#Montaigne]
</div>

Au risque de paraître sacrément stupide, la mort reste le paradoxe terminal de la littérature, elle achève ce qui en constitue l’essence : le silence. À défaut de se taire, nous ne parlons pas de l’acte en lui-même, mais de sa mise en récit, de ses figurations qui se retrouvent dans les romans, les récits, les poèmes, partout où s’invente l’individu littéraire.

Et si, dans les livres, le suicide était une mort bavarde, par bravade ? La perpétuation d’un récit, d’un désir contraint d’explications, la survie de l’individu figé dans son mystère, préservé dans ce qu’il aurait d’obstinément incompréhensible. Ne serait-ce pas une autre façon de le dire dans ce qu’il pourrait prétendre avoir de divin ?

La mort, en son silence, tient aussi à son impossible reproduction. Impossibilité de ressentir à l’identique ce qui pourtant nous est l’expérience la plus commune.

Essayer d’outrepasser l’adéquation entre individu et incommunicabilité.

On pourra s’attarder longtemps sur les reliques de l’individu, autant de hantises crues personnelles, l’appréhension de son vide, la radicalité de son insuffisance, la pluralité des ruses mises en place pour s’en détourner, n’en demeureront pas moins sinon un mystère, une part irréfragable d’inconnu, la limite d’une langue qui doit inventer d’autres reflets, repousser autrement la fixité qui figerait l’individu dans une absence unique<a data-link="18-a" id="l18-a" href="#l18-b"><sup>σ</sup></a>.

L’absence que nous sommes mérite variations et mouvements, flux et reflux. Des figurations essayées au hasard pour en repousser le définitif ; des fragments pour que d’autres, du dehors, en perçoivent l’unité.

Silence alors sur ses fantômes. Pas assuré, pourtant, que certains animent cet essai de leur incertain retour. Ils reviendront dire à quel point, pas tout à fait, on y ressemble ; les prémices d’une séparation entendue dans cette pudique incertitude pronominale.

</div>

<div class="text" data-id="2">

# Solitude : solipsisme

<div class="notemarge"><a class="notemarge__lien" href="#m07-l" id="m07-n"><sup>&ast;</sup></a>Quand elle se prétend choisie, la solitude comme illustration d’une masculine mise en histoire ?</div>

On substitue une nouvelle définition inapplicable<a data-link="19-a" id="l19-a" href="#l19-b"><sup>τ</sup></a> (inexplicable à l’instar de l’individu), pour en diverger. Encore. L’individu serait solitude, ou plutôt <span class="citation">pyrrhonienne</span> lutte pour l’appropriation d’un isolement du point de vue, la conquête, par coupure, de la singularité, <span class="citation">l’attente de la repousse</span><a data-link="20-a" id="l20-a" href="#l20-b"><sup>υ</sup></a>. Il serait absurde de déterminer à quel moment, le premier<a class="notemarge__lien" href="#m07-n" id="m07-l"><sup>&ast;</sup></a> ou la première, un individu s’est perçu si divisé dans sa solitude qu’il s’est senti d’en reconduire la division, d’en inventer par écrit la pureté, d’en faire une exemplaire aspiration, une posture par définition intenable. Vaine archéologie : en déterminer la première trace écrite.

<div class="auteur">
[#Crevel #Bouin]
</div>

L’individu, notion intangible jusqu’à l’inexistence, s’appréhende par des concepts changeants<a data-link="21-a" id="l21-a" href="#l21-b"><sup>φ</sup></a>. On pourrait les croire déterminés d’abord par le ressenti. Des pensées qui me dépassent, des dichotomies dont je perçois, simultanément, vacuité et nécessité.

Peut-être ne parle-t-on que de ce que l’on a cru, sans doute en discourt-on avec d’autant plus d’aisance que cette croyance désespérée s’est détachée de nous, aide aussi à comprendre les stases successives de détachement, voire l’itération d’une mise en absence du Moi.

Absence sans exemplarité, elle ne propose ni modèle ni idéal. Un ressenti si solitaire qu’il n’a pu qu’être partagé par d’autres. Une compréhension métaphorique, par comparaison et reflet, ça serait comme ça sauf que ça ne l’est pas tout à fait, jamais entièrement, sporadique reflet fugitif.

Individu et solitude du dehors ne veulent rien dire. De l’intérieur non plus, mais d’une façon différente. La solitude n’a rien de relatif, mesurable, quantifiable. Chacun croit être seul à sa façon : <span class="citation">lorsque je cessais d’être seul, la solitude devenait intense, infinie.</span>

<div class="auteur">
[#Blanchot]
</div>

Puéril, narcissique retour à une expérience crue propre, qu’une vaine valorisation de la solitude. Nous pourrions en essayer ceci : un état dont on éprouve, avec une attirante aisance, l’inanité. Nous-mêmes, à l’écoute de toutes nos voix, en faisons l’épreuve, l’écart à la fatalité.

Le désir de solitude sera(it), simultanément, le moins incertain des miroirs d’une société. Comment elle la valorise, stigmatise, la tient à l’écart ou tente de s’y immiscer, de la marchander, de la normaliser, comment s’y invente un rapport plus intense à l’autre, quel visage donne-t-elle à l’espoir d’en sortir ?

La solitude est, elle aussi, pur paradoxe : elle n’existe pas ; elle est partout.

Elle renforce l’illusion de la spécificité de notre moment historique, de la singularité des moyens techniques qui la définissent. On pourrait essayer cette platitude : aujourd’hui, par nos vies sur les réseaux, la solitude est une façon d’être avec les autres. <span class="citation">Comme une fiction usée d’avoir été trop souvent contée et à laquelle il ne reste pour toute rédemption que cette fin inattendue et définitivement réelle, véridique.</span> Une connexion, en théorie, à tous et à tous les instants.

Illustration de la virtualité, ubiquité et simultanéité, de l’individu dans son illusoire maintenant.

La seule transitoire excuse à écrire cette solitude est de mettre en résonance ses précédents pour mieux faire entendre à quel point jamais elle n’adhère totalement à ses représentations, à quel point l’écrit parvient à la faire diverger, lui invente une autre fin, une autre origine.

Nous ne faisons pas de l’archéologie<a data-link="22-a" id="l22-a" href="#l22-b"><sup>χ</sup></a>, nous rendons vie à ce qui revient.

Manifestation la plus évidente de ce retour de l’individu, la solitude serait se leurrer, perdurer dans l’illusion d’être le seul à se sentir enfermé dans tel ou tel type de ressenti, de parcellaire appréhension du monde.

L’autre nom de cette solitude : l’exigence de la lecture, ses illuminations quand soudain on pourrait signer une phrase qui ombrage, met en perspective, les plus secrets de nos enfermements, les plus limitées de nos perceptions. Quand elle nous renvoie aussi à l’omniprésente inexistence de notre absence.

<div class="notemarge"><a class="notemarge__lien" href="#m08-l" id="m08-n"><sup>&ast;</sup></a>Ailleurs, empruntant toujours à Leiris, nous l’avons nommé : viduité, un écart critique.</div>

La solitude situe l’individu comme un idéal inaccessible tant que l’on n’aura(it) pas réalisé, pour paraphraser Leiris, une sorte de solitude en Soi, une sorte de totale et, au fond, inquiétante, disponibilité à un Soi répudié<a class="notemarge__lien" href="#m08-n" id="m08-l"><sup>&ast;</sup></a>, une panique transparence, ignorante et sacrificielle, à ce que l’on prétend être.

Nous sommes les excuses qui, heureusement, nous retiennent d’être conformes, ressemblants, exactement ce que nous rêvons d’être.

Nous proposons une solitude collective. Épreuve d’une expérience d’<span id="inappartenance">inappartenance</span>. Un écart, un échappement. Un retour un rien attendu, mais auquel on cède parce qu’il va plus loin, ailleurs.

*Mon corps et moi*, expérience terminale de la solitude, pose, en Suisse, la dichotomie entre un temps pour Soi et celui des autres. Au mieux, une victoire à la Pyrrhus. Le temps existe-t-il dans une perception singulière, sans personne pour imposer ses synchronies, sans quelqu’un pour affirmer, par la contrainte, que lui le maîtrise, lui peut le plier à ses exigences ? Nous n’en savons rien, ne réfléchissons jamais qu’à partir de notre ignorance.

<div class="auteur">
[#Crevel]
</div>

Temps de rappeler l’inappartenance politique de l’individu. Nous la voulons vacance, expression radicale de son échappement à toute forme de pouvoir ; sa seule appréhension sera(it) disponibilité. On revient au paradoxe définitoire. L’individu croit exister quand il est ouvert à toutes les interruptions, disponible à toutes les contradictions, et s’absente ainsi de toutes les affirmations, montre du doigt son masque lors de ses miraculeuses, malencontreuses, apparences d’une manifestation extérieure, collective.

L’individu est miroir, en abyme, de la solitude : tous deux ne surviennent que dans l’espoir de sortir d’eux-mêmes. Le temps pour l’individu ne serait pas une perception, mais une appréhension. La représentation d’une peur où crainte et espoir s’amalgament. Un temps solitaire où passé et présent sont similaires leurres cérébraux, où la présence serait substitution, comédie grinçante par la projection dans le hors-scène du théâtre d’ombres où nous revenons.

Déjouer, pourtant, tant l’idée d’un repli sur Soi que celle d’une solitude comme retrait, déperdition de réalité. Sans céder à la valorisation de l’aristocratique tour d’ivoire[^1-lepreuve-de-lindividu_6]. La solitude vaut, comme n’importe quelle modalité d’être, quand elle éprouve ses limites, coïncide avec l’individu dans sa perpétuelle tangence avec le solipsisme.

Encore --- l’individu est-il autre chose que lassante répétition ? --- une affirmation paradoxale : on veut une littérature totale. Elle ne l’est, à notre sens, que si elle propose un enfermement mental, le reflet d’une introspection, de l’exploration d’une région si vide qu’elle contraint à toutes les inventions, mirages et miroirs où reflue notre enfermement.

L’individu ou la difficulté d’en sortir.

D’autres parlent de roman-monde, de roman-monstre, affirment que l’auteur y est sorti de lui-même et aurait pour cela, dans un curieux phénomène de décompensation, analysé plus individuellement, plus en profondeur son univers verbal.

Creuser, éviscérer, dire l’individu autrement reviendrait-il à lui inventer une autre géographie ? Sortir de Soi, donner une autre forme à son enfermement. Un peu de profondeur de *chant*. Faire d’un ressenti incertain une caisse de résonance. Quel sens de s’oublier si c’est pour s’affirmer, en creux --- horreur de la fausse modestie ---, dans des personnages de substitution ? N’a-t-on rien d’autre à mettre en scène que la commune singularité d’une expérience de déréalisation ?

Dans son insuffisance, dans son confort aussi (ne faut-il pas être riche pour que la réalité cesse d’être un problème ?), une part de nous (celle qui toujours s’envisage avec défiance, celle que lectrice, & lecteur, pourrait reconnaître comme tout aussi peu sienne) souhaite tracer les esquisses, les retours dans d’<span id="imparfaites">imparfaites</span> similitudes, de cette expérience --- dite intérieure par distanciation --- où les frontières du ressenti personnel deviennent, dans une expression datée, dubitative, expérience des limites.

<div class="auteur">
[#Bataille]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m09-l" id="m09-n"><sup>&ast;</sup></a>Esquisser serait, pas seulement pour les sons, une façon d’esquiver. Dessin d’un trait dont ici est conservé le repentir, l’effacement. Biffures et variations.</div>

On pourrait, dans l’espoir d’en susciter de nouvelles manifestations, de nouvelles remises en cause, esquisser<a class="notemarge__lien" href="#m09-n" id="m09-l"><sup>&ast;</sup></a> une histoire du solipsisme. Le réfléchir au miroir de son origine trompeuse : le langage, avec l’illusion souveraine du nominalisme. On va le dire ainsi, façon encore de montrer notre méthode au moment où, en s’essayant, elle se déconstruit : on partage, on l’énonce, cette illusion souveraine consistant à faire accroire qu’écrire une phrase en crée la forteresse vide, le monument mort, le château de cartes d’une réalité insuffisante, la parade d’une perception performative. Si le mot individu, fût-ce par imagination, ne mord pas (dépasse, dévore<a data-link="23-a" id="l23-a" href="#l23-b"><sup>ψ</sup></a>), à quoi bon l’écrire ?

Si on arrivait à faire entendre la défiance et l’incroyance contenue dans ce genre d’assertion, on pourrait esquiver un nouveau pacte de lecture. Lectrice, & lecteur, tu te désengages à ne jamais croire à la suffisante, forclose, réalité de ce qu’ici je te propose, que tu la fasses toujours entrer en résonance avec le peu de réalité de ce que tu es, au secret de toi-même. Celui qui te parle est un imposteur, écoutons notre commune imposture[^1-lepreuve-de-lindividu_7] pour n’en dégager aucune certitude, des questions seulement.

Et si le solipsisme --- s’enfermer dans ses sensations, dans la perception d’un narrateur isolé, peu fiable, parlant dans la fausseté d’une première personne de convention --- était la manière la moins inadéquate, la plus emplie d’inadéquations, de moins mal ne pas comprendre le monde.

Au temps de la multiplication endémique des experts, ne se vouloir spécialiste de rien. Sans domaine. Le dire comme façon de s’en démettre, d’éviter l’automatisme de la posture. Reconnaître alors l’excès de prétention de cet essai-monde qui se teinte, avec arrangements tu le sais, lectrice & lecteur, d’un de ces monologues hallucinés où s’effleurent les limites solipsistes de la solitude.

On pourrait, comme une situation initiale qui dans un roman attend et conçoit son élément perturbateur, en fixer une origine qui, quand elle est dite, ne méconnaît pas ses précédents, les emprunts, détours et reprises avec lesquels n’importe quelle situation s’écrit.

<div class="notemarge"><a class="notemarge__lien" href="#m10-l" id="m10-n"><sup>&ast;</sup></a>Le reniement comme secrète, solitaire, communauté par coïncidence : Aragon, alors, brûle <em>La Défense de l’infini</em>. Hasard, prémices de la rupture ? Substitutions, glissements, comparaison circulaire, inachevée, infinie.</div>

<span class="citation">Une chose réussie n’est réussie que par abandon.</span> *Introduction au discours sur le peu de réalité*, André Breton. 1927. Basculement<a class="notemarge__lien" href="#m10-n" id="m10-l"><sup>&ast;</sup></a>. Acmé. Ensuite, s’engager dans le collectif, les faux-nez de la révolution. Pas une raison, malgré tout, pour renoncer à approcher de la pointe extrême du langage. Entrée du paradoxe, de son fantôme on entend. André Breton, le chantre non tant de l’individu (de sa domination dans un complexe jeu de séduction, de domination groupusculaire) que de sa vocation poétique, de la confusion entre la vie et l’image que la littérature peut en inventer, des hasards qui en font une divination. Une existence plus intense aussi.

Par une déformation de pensée, une anecdote nous hante. Peut-être parce que l’individu est infiniment anecdotique. Un des premiers procès surréalistes, encore très dadaïste, jaugeait la vie et l’œuvre d’écrivains connus, leur attribuait une note, une classification.

La seule note pour un individu : un zéro où miroiter son vide essentiel. Une révélation de ce que fut aussi le surréalisme : une déception pour toutes les manifestations où le mondain, pour ne pas dire les jeux de salon, essaie une pratique poétique collective. La volonté de tenter sans trêve, de cela au moins il faut être redevable au surréalisme.

<div class="auteur">
[#Breton #Aragon]
</div>

Le rire, sa submersion de l’esprit de sérieux qui caractérisa une part dissidente du surréalisme, fut sa plus grande réussite collective. Un rire, esprit de bande et dissidence. Breton se voyait en procureur, le jury frappa de nullité l’ensemble des personnalités des accusés. Tout se transforma en comédie, un ratage où soudain sont enterrées des conceptions datées. À cet instant, le surréalisme aurait pu devenir autre chose que la queue de comète du romantisme, l’héritier de sa transfiguration, qui encore aujourd’hui paraît efficiente, de la figure du poète. Mallarmé, Barrès, Gide, l’autre siècle qui surgit en Breton comme en Moi survit le vieux vingtième siècle. La très large inutilité de pratiquer une critique par distinction, qui reprend ce mouvement de jauger une vie.

Le romantisme de l’échec, l’individu dans la trace de ses déroutes. L’individu comme enthousiasme momentané, répété dans ses échecs : <span class="citation">il semble, notamment, qu’à l’heure actuelle, on puisse beaucoup attendre de certains procédés de déception pure dont l’application à l’art et à la vie aurait pour effet de fixer l’attention non plus sur le réel, ou sur l’imaginaire, mais, comment dire, sur l’envers du réel<a data-link="24-a" id="l24-a" href="#l24-b"><sup>ω</sup></a>.</span>

<div class="auteur">
[#Breton]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m11-l" id="m11-n"><sup>&ast;</sup></a>À l’écart aussi de la paralysie des désespoirs trop grands.</div>

Au-delà de son miroir d’encre<a class="notemarge__lien" href="#m11-n" id="m11-l"><sup>&ast;</sup></a>, l’individu se limiterait à ses défaillances, au mieux un pis-aller, un échec à reprendre, mais dont on tirerait une certaine fierté, une certaine grandeur, un attachement certain, malheureux, à sa petite personne. Toujours cette réserve d’intranquillité face à une inquiétude justificatrice. On suspend la *furor* des écritures habitées. On la préfère idéale protestation, glissements répétés vers son inachèvement. Kafka dans son *Journal* invente cette vision de la solitude inspirée. Au fond l’espoir que l’écriture se fasse sans Soi. <span class="citation">Le désir d’une solitude allant jusqu’à la perte de conscience. Seul face à moi-même.</span>

<div class="auteur">
[#Kafka]
</div>

Les citations, manifestation de l’ennui<a data-link="25-a" id="l25-a" href="#l25-b"><sup>Α</sup></a>, vraie épreuve de la solitude, incursion ironique dans <span class="citation">le jardin imaginaire de la culture libérale</span>. On tombe sur des phrases au hasard, on feuillette sa bibliothèque. Déjà on écoute une solitude devenue fiction, souhaitée comme on pourrait espérer, pétri de doutes, vouloir vivre dans un roman. Dans la solitude, ne joue-t-on pas un rôle, on se revêt des oripeaux de l’auteur, de son mythe, de l’attente prostrée de l’inspiration et autres foutaises ?

<div class="auteur">
[#Steiner]
</div>

Peut-être pourtant faut-il l’avoir vécu pour se permettre d’en parler. Kafka qui tellement s’ennuie qu’il va trois fois à la salle de bain se laver les mains. Une sorte de communauté dans les instants perdus. Universalité de la solitude, méchant trompe-l’œil. Sans y prendre garde, on se trouverait à affirmer que chaque solitude est particulière, voire, pis encore, qu’elle seule permettrait de retrouver la vérité de son être. Au mieux, on trouve dans la solitude un mensonge non tant singulier qu’une forme d’expression <span id="virtuellement">virtuellement</span>, dans l’isolement, nouvelle.

<div class="auteur">
[#Kafka]
</div>

Les tréfonds, invérifiables et sans mesure, de la solitude, ce sont peut-être l’instant où ce que l’on prend pour notre voix devient insupportable, inaudible ressassement, attente d’une forme, espoir d’une altération. La moins mauvaise, aussi, définition de notre démarche.

Considérer, aussi, l’histoire de la solitude, à travers sa divergence textuelle, comme celle d’un combat contre la réprobation. Là encore une aspiration contrariée qui place l’écrivain dans sa société. On pourrait penser à Kafka l’éternel fiancé, le fils, à ses espoirs déçus d’inventer un autre rapport à l’autre.

L’écriture, est-ce vraiment autre chose ?

Écrire, se retrancher de l’oralité, tenter de donner à entendre le poids sensible du silence entre les mots. Dans son hallucination, son incantation littéraire, le solitaire, celui qui parle trop, n’écoute pas, se montre inacceptablement centré sur l’absence à lui-même. Serait-ce à cause d’une solitude non choisie ? Mais, alors, se croire en mesure de faire ses propres choix, est-ce une épreuve de l’individualisme ?

On croit la solitude composite, impure, inévitable. Non tant le résultat d’un choix, mais la mise à l’écrit, plus ou moins obsessive, de ses justifications. Valoriser ses propres erreurs, se construire une personnalité. Son expression littéraire est elle-même hybride.

Un long combat, perdu d’avance ; écho à celui de trouver une langue qui soit propre.

Trouver sa voix, en faire entendre la spécificité impropre, s’inventer la singularité d’un style, autant de manières de se cantonner dans l’individualité. Pire d’espérer que l’écrit va en reconnaître la valeur.

La plus haute des solitudes serait de se reconnaître, une fois pour toutes, noyé indécelable dans la masse, parcelle perceptive d’un tout où enfin s’absenter.

<div class="auteur">
[#Crevel]
</div>

</div>

<div class="text" data-id="3">

# L’espace brouillé, cartographie d’un Moi en absence

<div class="notemarge"><a class="notemarge__lien" href="#m12-l" id="m12-n"><sup>&ast;</sup></a>Hasardons le néologisme pour dire la déconstruction d’un imaginaire, le désir de le laisser informe, transitoire, contradictoire.</div>

Et si la modernité était sans cesse à réinventer, absolument, dans une forme de primale et nécessaire contestation de tout ce qui la précède, dans l’invention de généalogies de traverses ? Dans l’idéal, chaque fois qu’un romancier, un poète, un diariste, évoque un personnage, lui-même par effacement, il opère une désinvention<a class="notemarge__lien" href="#m12-n" id="m12-l"><sup>&ast;</sup></a>.

<div class="auteur">
[#<span id="rimbaud">Rimbaud</span>]
</div>

<span class="citation">Dessiner jusqu’au bord du silence, les bords sans bords de cette absence, c’est le travail de la fiction.</span>

<div class="auteur">
[#Rancière]
</div>

Là encore, on peut l’envisager sous ses limites, en montrer, trop tard<a data-link="22-b" id="l22-b" href="#l22-a"><sup>χ</sup></a>, les influences et imitations. Pourquoi ne pas en écrire les écarts, les bifurcations, les parallaxes qui, à d’autres, permettront d’être ailleurs ? Une somme de possibilités, entr’ouvertes, dont on repart quand on ose, derrière la confusion pronominale, dire Je.

Nous ouvrons une nouvelle dispersion paradoxale, une nouvelle dichotomie. On évoque l’expérience d’autrui, sa prise de parole, son écriture ou sa mise en fiction, on s’en approprie les tentatives comme reflets de celles qui ne sont pas nôtres.

<span class="citation">La fiction et la réalité se mêlent, en fait, ou les réalités sont non seulement improbables et invraisemblables, mais incompatibles.</span>

<div class="auteur">
[#Marías]
</div>

À la poursuite de la différenciation, d’une démarche d’instabilité : effacements, fragments, collages. Différents aspects pour composer une anamorphose. L’individu ne parvient à se limiter à son espace cérébral. S’il apparaît dans sa désinvention, posons l’hypothèse (pour mieux ne pas tout à fait y croire) que ce sera quand il figure d’autres espaces, miroite ceux dont l’épistémologie de son moment historique opère la théorisation, la conquête par la mise en mots.

<span class="citation">Raison<a data-link="26-a" id="l26-a" href="#l26-b"><sup>Β</sup></a> chthonienne de la modernité nouvelle. Modernité <span id="chthonienne">chthonienne</span>.</span>

<div class="auteur">
[#Mascolo]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m13-l" id="m13-n"><sup>&ast;</sup></a>Tout récit est dédoublement, désir de trouver un autre cadre à ce qui, déjà, a eu lieu.</div>

Étrange comme le mot modernité renvoie à deux époques, à deux<a class="notemarge__lien" href="#m13-n" id="m13-l"><sup>&ast;</sup></a> définitions spatiales de l’individu. Pas un hasard si le projet de se peindre, d’essayer la pensée par digression, est contemporain de notre monde, et ne finit d’en découvrir un autre. Itérative propension de l’individu à se représenter aux bords des gouffres[^1-lepreuve-de-lindividu_8], dans les décombres d’un vieux monde agonisant, dans <span class="citation">la nostalgie du désastre</span>. Au seuil donc d’un nouvel espace en permanence à inventer. <span class="citation">Any where out of the world.</span>

<div class="auteur">
[#Montaigne #Steiner #Baudelaire]
</div>

Autre temps, pour une dédoublée modernité : Baudelaire, et la vie en ville, qui <span class="citation">mêle son ironie à ton insanité.</span> L’ombre de la révolution, les derniers fantômes de l’aristocratie dont la posture littéraire a tant de mal à se défaire. On surjoue ce rapprochement dans la conscience qu’il ne fonctionne pas, dans la bête distanciation de refléter ainsi les lieux communs de notre époque. La littérature finit par imposer sa relecture fragmentaire de l’Histoire, <span class="citation">les claires déchirures du tunnel</span> que serait l’écriture du moment, de sa pluralité.

<div class="auteur">
[#Baudelaire #Blanchot]
</div>

En caricaturant la posture d’auteurs comme Deville, Vuillard, Cercas, on pourrait dire l’Histoire, suite d’instants qui attendent le Moi commentateur d’un auteur pour les relier, en montrer les boitements, les points aveugles.

On le fait, à notre tour, pour surtout ne pas se penser spécialiste, assuré d’un domaine qui ne nous appartient pas. La seule question hantée : quel espace nouveau arpenter pour aujourd’hui, quelles frontières différenciées il inventera pour l’individu ?

Cette frontière, dans l’entretien de la confusion qu’est l’individu, on la nomme virtualité. Façon distanciée, dans sa répétition, de dénoncer ce que l’on pratique, de pointer l’insuffisance de ce que l’on affirme.

Le rapport à l’espace est saturé de dénominations arbitraires, réductrices. Virtualité malgré tout, hétérotopie. Espace instable, de discontinuités et de basculements, de trous noirs, de clandestins réseaux.

<div class="auteur">
[#Foucault]
</div>

Pour opérer une nouvelle définition par substitution : quel visage donner à l’individu quand il transparaît (le terme voudrait dire l’amalgame du désir de présence et de disparition) dans une utopique, combattante et donc contradictoire, cyberlittérature ? En parler en autochtone, en natif d’un pays déjà perdu<a data-link="27-a" id="l27-a" href="#l27-b"><sup>Γ</sup></a>, encore à réinventer tant il peut paraître que déjà nous n’en habitons que les décombres.

Sur Internet virtuellement se déploient les limites de l’anonymat, l’absence de visage, l’épreuve du dépassement de l’individu. En miroir s’y contemple le narcissisme le plus mercantile<a data-link="28-a" id="l28-a" href="#l28-b"><sup>Δ</sup></a>. Sans doute parle-t-on ici d’un autre Internet : celui de la gratuité, du partage à partir de l’abolition, du contournement au pire, de la propriété intellectuelle. Là-bas, on libérerait le code source de l’individu, on le diffuserait gratuitement, on pourrait en vérifier les altérations, les indispensables métissages, les tentatives et autres *reboots.*

Plus de pratique, moins de commentaires. Des instants métapragmatiques, de conscience critique de l’ordre du monde --- discontinuité.

<div class="auteur">
[#Boltanski]
</div>

La discontinuité spatiale où s’affirmerait l’effacement de l’individu ne dispense pas de préciser d’où l’on parle : de nulle part.

Encore une notion qui mérite une écoute contemporaine. L’absence de lieu, les replis d’un territoire sans appartenance comme éclairage de la cartographie contemporaine qui nous limite. Ou, dit autrement (la définition de cet essai), l’Histoire se réduirait à l’effacement, à la domestication, des non-lieux. Comprendre des endroits sans-sanction, sans pouvoir. C’est d’ici d’où j’écris.

<div class="notemarge"><a class="notemarge__lien" href="#m14-l" id="m14-n"><sup>&ast;</sup></a>La spire, selon Caillois, serait à la fois régressive et évolutive, enroulement et développement. Notre propos n’esquive rien d’autre.</div>

Le centre convenu de cet écrit aussi : le moment précis où tu ne sais plus où nous allons. La vision la plus évidente d’une littérature numérique serait celle d’une spirale<a class="notemarge__lien" href="#m14-n" id="m14-l"><sup>&ast;</sup></a> cérébrale, un jeu de liens hypertextes qui en boucle tourne.

Une figuration, en somme, de l’individu : un élancement qui retombe, repart et, sans fin, tourne en rond. On l’a dit, y échapper se fera peut-être en l’absence de toute préméditation, de tout plan, la présence parfois de celui qui se sera outrepassé.

<div class="auteur">
[#<span id="caillois">Caillois</span>]
</div>

Un égarement. Alors certes, encore une question de mode (pardon de modernité), peu de livres (au moins parmi ceux qui retiennent notre attention), aujourd’hui, promettent un parcours balisé, une évolution fléchée, sachant strictement ce qu’elle veut dire et parvenant à ne pas en signifier davantage. Sans doute pour parvenir à ceci faut-il croire en un individu aussi terminé que sont les ouvrages qu’il achève.

Nous serions donc dans la reprise d’un écart pour, suprême illusion, inventer un espace virtuel où, un instant, s’affranchir du cartésianisme. On le rappelle, on ne pense pas seul ; on propose des pistes, des invitations à l’obsédante présence de l’autre que contient tout écrit, la part de ce qui m’échappe, attend d’autrui une explication, reformule celles ailleurs dérobées. Une façon de penser donc : partir d’un cliché, s’en départir, le détourner vers autre chose. Une réserve d’ironie, toujours une distanciation dont peut-être se départir.

Reprenons, biffons les pétitions de principe. Je pense, donc je suis. Mais : pas d’état définitif de la pensée ni de l’être. Des brouillons d’individus, des stases de pensées. Ou mieux, d’autres conjugaisons pour d’autres modalités. Au futur, ce serait désirable si on pouvait l’indifférencier du conditionnel : je penserai(s), donc je <span id="serais">serai(s)</span>. Virtualité d’un ailleurs à inventer autant qu’à retracer ; l’effacer pour mieux, dans sa différence, le rendre présent, susceptible de revenance.

L’individu sera(it) alors cette <span id="contradiction">contradiction</span> : amalgame hasardeux, imprécis tant que la langue le permet, entre rétrospection et projection, entre ce qu’autrui en a tenté et ce que nous voulons en faire.

Sans espace et sans temps, cliché pour cliché, stase temporelle et figuration transitoire, la virtualité serait quantique. On s’empare de cette notion parce qu’on en ignore tout, parce qu’elle nous dérange (entendre : elle introduit un désordre spatial). Un peu comme Balzac a situé l’individu grâce à la physiognomonie, une arrangeante erreur pour portraitiste des méandres de l’individu.

Balzac en ses paradoxes. On l’aura compris, l’individu apparaît dans ce que l’on n’en accepte pas tout à fait : une sorte d’absence qui assure, par défaut d’assiette, la présence, la revenance. L’individu comme écart. Balzac serait non tant le protosociologue, l’historien que, dans une vision peut-être un rien trop scolaire, l’inventeur des récurrences. Revenance de sa romantique jeunesse, sa *Comédie humaine* est hantée par le fantastique, ses épreuves d’une perception personnelle. La précision comme façon d’atteindre aux limites du réel, la description comme un doute, un trompe-l’œil. Un désespoir aussi.

Mieux, une façon de tromper la mort, ce qui revient, ce n’est pas Vautrin, mais ses tentateurs conseils, ses diaboliques désirs d’être autre. On peut simultanément les comprendre comme diatribe contre le déplacement sur l’échelle sociale.

L’individu comme ce qui, désormais, jamais ne sera à sa place ailleurs que dans une projection, souvent le reflet artistique où se dépeint ce qu’il aurait pu être, là où se devine et se maintient sa capacité de retour, de mésinterprétation.

Alors pour cartographier, dans l’abus de langage qu’est l’emploi de cette expression, interrogeons une certaine propension à voir l’individu comme une toile abstraite, absente, les figurations indifférenciées de motifs, de formes qui feraient sens.

L’<span id="epreuve">épreuve</span> paradoxale : il faut que ça me parle.

Que les spirales cérébrales colonisent cette cartographie abstraite, inventent les revers d’une précision rêvée plus grande. Sortir de ses frontières, l’individu, dès l’origine, est si peu sûr de lui qu’il s’assure par un expansionnisme, impose un modèle qu’il ne va pas tarder à habiller des oripeaux du progrès. Notre monde vient d’en découvrir un autre. Renaissance. L’individu commencerait par dissiper les brumes d’un improbable retour à l’obscurantisme, d’une exploitation unilatérale qui cacherait une certitude collective, le peu de conscience de sa propre mort, une prétendue fragilité médiévale de la conscience de Soi, en rappelant que l’émancipation est une épreuve, toujours à recommencer.

Dit autrement : le progrès n’a rien de définitif, il est fait de retours : à chacun, sans fin, de faire l’épreuve de son défaut d’individualité. Sans finalité, un territoire à arpenter afin d’en inventer les confins, la porosité des frontières, les rêves d’un hors-carte, la permanence d’un retour fantasmatique.

Tentons, pour être un peu plus concret<a data-link="29-a" id="l29-a" href="#l29-b"><sup>Ε</sup></a>, cette définition tout de virtualité : si un livre est un monde, son territoire se définit et se redessine à chaque lecture, par chacun de ses habitants, voire par chaque modification créée chez chaque arpenteur-lecteur & lectrice-cartocryptographe, nous voulons en cartographier (montrer ce qui est déjà aboli, ne marche peut-être que pour Moi, inviter chacun à inventer les siens propres) les *glitchs*, les trous de taupes qui permettent de parcourir le territoire du livre. En le lisant, on parcourt le paysage lu par tous les lecteurs, & lectrices, nous précédant, on y amalgame ceux auxquels, dans une intuition invérifiable, on pense.

Ailleurs. Rêve d’une cyberlittérature mondiale : une bibliothèque en partage dans laquelle, encyclopédie en mouvement d’un savoir humain ondoyant, chacun pourrait inscrire ses liens hypertextes, d’un livre à l’autre ou à l’intérieur du livre, pourrait afficher ses commentaires, pastiches, imitations, réécritures, appropriations. La critique littéraire, cette libre association d’idées qui se croit valider par l’érudition et l’obscurité, s’en trouverait totalement mutée<a data-link="30-a" id="l30-a" href="#l30-b"><sup>Ζ</sup></a> (au sens anglo-saxon de réduire au silence et au sens francophone d’une permanente mutation).

Au seuil du basculement, l’individu se situe dans l’invention du paysage. L’incarnation la plus simple de cette absence à Soi qui sera(it) présence au monde. On raconte ce que l’on croit percevoir du monde, on en décrit la contemplation pour faire deviner ce qui se cache derrière, ce qu’il reste à imaginer, les perspectives à tracer. Au <span id="loin">loin</span>, on aperçoit des montagnes, comme en dessinerait, écrirait, Pamuk. Une virtualité, une échappatoire.

Une incertitude dont repartir : la littérature s’imagine qu’elle reproduit la réalité. Au mieux, elle donne à imaginer un trompe-l’œil.

Dans les paysages, souligne Pamuk, entre une part inconsidérée de souvenirs, d’appropriations et de déformations. Dans tout ce que l’on écrit, une similaire et ondoyante déformation des paysages que l’on a vus. Un personnage, si tant est qu’il puisse informer la naissance de l’individu, est constitué de cette part fantomatique d’appropriation. Mais, le paysage c’est la couleur et le concret. Dada est là, d’ailleurs selon Pamuk, par collages, dans tous les paysages. <span class="citation">J’ai déjà vu tout ce que je n’avais <span id="jamais">jamais</span> vu.</span>

<div class="auteur">
[#Pessoa]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m15-l" id="m15-n"><sup>&ast;</sup></a>Romantisme, le paysage serait reflet, torturé, de nos états d’âme, grandiloquente extériorité de nos sentimentaux secrets. Romantisme aussi dans ce paysage qui serait image sempiternelle, échappement à nos craintes de la mort.</div>

Alors, comme pour toute figuration, un paysage est une revenance. L’invention du personnage romanesque qui s’y esquisse est éternel retour. Une façon de rejouer, encore, toujours les origines. Poursuite, sous une autre forme, de la mythographie romantique<a class="notemarge__lien" href="#m15-n" id="m15-l"><sup>&ast;</sup></a>. Le paysage ou la rencontre de différentes obsessions.

Laisser alors revenir, avec une distanciation pleine de défiance (perpétuel écart ironique avec celui qui parle, avec cette incapacité à assumer ce que l’on dit) : René Crevel. En Suisse, face à la solitude solipsiste où il ne sait s’installer, face au paysage où il ne parvient à inscrire le concret de son absence au monde, lui revient la lapalissade langoureuse de Verlaine : paysage, état d’âme. Le retour de cette imbécile volonté d’exprimer, par des miroirs, l’inconsistance inspirée de l’âme. Au fond du lac, avant la venue du Loup, se noie le narcissisme, le romantisme suscité par les peintres qui sortent de l’atelier. Une filiation à refléter autrement. Ce qui ici est, en mineur, en palimpseste, cartographié esquiverait l’impuissance réitérée --- vectrice de nouvelles formes --- à sortir de Soi.

L’espoir d’une cartographie à plusieurs langues. Si un roman aussi influe sur l’individu, c’est hélas celui national. Aucun sens, au fond, de parler seulement du romantisme français, cette mauvaise doublure, de celui allemand. Impossible pourtant, si on s’en tient à une histoire centrée sur la réception, le romantisme allemand reste mal connu, dont l’empreinte fut sans doute tardive, possiblement perçue seulement dans la deuxième moitié du XX^e^ siècle.

Les frontières se redessinent, elles font apparaître, persistantes, cet individu inenvisageable : *heimatlos* et autres inventions de la nostalgie.

Et si nous la définissions autrement : la seule nostalgie est stylistique, un écart à l’impossible langue maternelle, le maintien pratique, un mot après l’autre, de cet écart à la langue parlée, à la grammaire, qui constituerait, concrètement, la permanente extraterritorialité de celui qui écrit. Sa volonté de s’inscrire alors dans un paysage.

Un paysage, inquiet, devine un détour. Derrière la colline, et pourquoi pas<a data-link="31-a" id="l31-a" href="#l31-b"><sup>Η</sup></a>, une trouée se révélera. L’individu n’est peut-être pas différent. Comme lui, le paysage, transitoire, captive par son intranquillité, la révélation suspendue de ce que l’on y cherche, de ce que l’on ne saisit pas.

Au fond d’un paysage, de la représentation mentale de ce que l’on n’est pas, entièrement ou encore, se cache, en trompe-l’œil, une bibliothèque. Silhouettes, perturbants passants qui n’ont rien à faire dans le tableau. Pirouette, une façon déjà d’être ailleurs, de s’absenter de toute description.

Du paysage, on passe, par abus de langage, au tableau. Pour dire la saisine d’une vision, son caractère intuitif. Le regard du peintre se confondant avec celui des spectateurs successifs, disons avec sa certitude d’avoir à deviner la teneur de souvenirs, de rêves, de reconstitutions d’un paysage qu’il ne connaît pas.

Un paysage en revenance serait le Lisbonne de Bernardo Soares, tel que pourrait le voir, en se gardant bien d’y aller, une lectrice de Cărtărescu, un lecteur de Vila-Matas... Les ruelles d’une absence à Soi, le rituel d’un sourire sardonique, onirique, d’un débit de boissons, le Tage dans l’ombre, la rumeur de la mer. On essaie d’être ce passant, comme une figuration non pas trop fugace, mais exagérément fantomatique, appartenant à celui que l’on n’est plus, <span class="citation">siècles intérieurs d’un paysage du dehors.</span>

<div class="auteur">
[#Pessoa]
</div>

Le paysage, le personnage, exercice de détachement. L’essai est une fugue, son personnage un motif rythmique[^1-lepreuve-de-lindividu_9], un hasard de mots comme le fortuit des couleurs esquisse un paysage. On ombrage des paysages pour situer le seuil de ce que l’on abandonne. Que sans doute l’on ne franchit pas.

<div class="auteur">
[#Cendors]
</div>

On revient sur nos pas, on détourne la méthode : penser à partir de mots-sésames, à partir de formules dont on décèle l’impossibilité, dont on se départit. Paysage, état d’âme. Est-il encore utile d’arguer de l’inexistence de l’âme ? Tentons plutôt de voir comment l’athéisme radical d’un individu qui pourrait s’affirmer (<span id="disparaitre">disparaître</span> de cette affirmation même) dissipe la possibilité même d’un paysage.

*Détours* par Crevel. Le paysage, sa cartographie, pour ne pas dire sa sérigraphie (une carte ne serait-ce pas dire : ici vous devez vous attendre à trouver ce type de paysage, la conformation géographique vous permettra de trouver tel type de sensation), interroge la croyance même en la Nature. Un sentiment périphérique, une promenade pour petit parisien, pour assurer la prédominance de l’humain.

<div class="notemarge"><a class="notemarge__lien" href="#m16-l" id="m16-n"><sup>&ast;</sup></a>Pas seulement dans un vertige spiralaire. Les idées, comme l’individu, sont interstices, images d’une revenance qui paraît tourner en rond seulement dans la virtualité du déraillement autour d’un vide.</div>

Tourner en rond<a class="notemarge__lien" href="#m16-n" id="m16-l"><sup>&ast;</sup></a> : l’individu ou l’invention vaine de l’extériorité qui viendrait confirmer son intériorité, valoriser ainsi son projet de l’éclairer. Alors qu’il s’agit seulement de réactualiser le carcan d’antiques dichotomies.

Avec Crevel, continuer à contrer toutes les séparations, outrepasser les choix qui invalideraient une possibilité. Le contemporain quantique parviendrait à les laisser obscurément ouverts. Maintenant, regarde un paysage qui serait, à l’image (manquante) du proverbial chat de Schrödinger, à la fois là et pas là.

La matière est une abstraction, plus on s’approche, plus on veut la mesurer, moins on la comprend. Les paysages dont la contemplation révélerait l’individu se composeraient pour l’essentiel d’antimatière. La belle affaire.

<div class="notemarge"><a class="notemarge__lien" href="#m17-l" id="m17-n"><sup>&ast;</sup></a>Simultanément, une certaine biologie invite à envisager l’individu comme somme instable des parasites qui le dévorent, le maintiennent en vie.</div>

Par notre inexistence, alors, nous sommes parties fissiles de cette antimatière. Peut-être résonne-t-elle quand nous contemplons ce que nous croyons être le plus secret de notre être, celui qui nécessiterait, mériterait d’être dévoilé. Autant mettre tout ceci dans un carton pour montrer l’inexistence paradoxale<a class="notemarge__lien" href="#m17-n" id="m17-l"><sup>&ast;</sup></a>, quantique, de l’individu. <span class="citation">Avec combien peu d’interest nous perdons connoissance de la lumière et de nous !</span>

<div class="auteur">
[#Montaigne]
</div>

Cartographier alors les seuils de l’évanouissement, les instants de conscience au moment où elle devient autre chose, tente de se refléter dans d’autres paysages. On aime que cette déambulation, l’autre nom de l’essai, soit dictée par d’autres voix contingentes, d’autres rencontres de traverse.

On parlerait, comme un concept qui ne nous appartient pas, de <span id="cryptocartographie">cryptocartographie</span>, celle à même de donner une idée, de faire signe vers une image imparfaite, vers la perpétuelle intrication quantique.

L’individu radicalement autre dont nous dessinons ici les absences se reconnaît avec le plus de justesse ainsi : soudain, quelqu’un d’autre que moi, bien mieux que je ne saurais le faire (avec cette précision documentaire dont je ne m’encombre pas) parvient à situer ce que je ne savais pas dire.

L’individu sera(it) brouillé<a data-link="06-b" id="l06-b" href="#l06-a"><sup>ζ</sup></a>, à la fois là et pas là, entièrement dans ses contradictions qu’aucun pôle tout à fait ne pourra réclamer. Ni assumer ni dépasser ses contradictions ; en vivre tous les états fantomatiques.

Peut-être, tout l’individu tient dans cette virtualité, se dessine une autre poétique, un autre rapport à l’espace. Si ce n’est pas le Moi restreint qui en vérifie la présence, en témoigne pour mettre en valeur sa certitude d’avoir été ici, un instant, quelle géographie s’esquivera(it) devant nos yeux ?

Paysages oniriques, noctambules, temps suspendu d’une vision qui déjà s’estompe. On finira(it) comme on a commencé : on s’invente une autre géographie.

L’individu revient dans ses échappatoires.

</div>

<div class="text" data-id="4">

# La nuit de l’individu

L’individu, tu l’auras compris, n’est peut-être qu’un jubilatoire jeu de mots : une éclipse.

Le moment de sidération où ses astres s’éteignent. Un simple instant d’obscurcissement que, par tradition littéraire, on a appelé nuit. Moments d’identité changeante où l’individu a cru s’inventer et, parfois, en absence de miroir et de confrontation, y est parvenu. Il en reste des transfigurations qui informent l’individu.

<div class="notemarge"><a class="notemarge__lien" href="#m18-l" id="m18-n"><sup>&ast;</sup></a>Contre-nuit, on sait ce que l’on te doit, on en essaie autre chose entre emprunt et déport, piratage et écoute.</div>

On tentera différentes définitions de ces nuits, toutes jamais totalement opérantes. Parcours subjectif, sans Soi pourtant, des définitions que d’autres<a class="notemarge__lien" href="#m18-n" id="m18-l"><sup>&ast;</sup></a> ont essayées, altérées. Il serait vaniteux (même si cette vanité est peut-être un des moyens de disparaître à Soi) de prétendre tracer *Une histoire de la nuit*. À peine en esquissera-t-on ici une cryptocartographie. De loin en loin, quelques noctambules illuminations qui peut-être se répondent, ensemble, phalènes, elles volettent.

<div class="auteur">
[#Raphmaj]
</div>

On revient, immuable, aux mêmes origines, faute de pouvoir les invalider ou de, sans partage, les revendiquer pour siennes : les fantômes romantiques de l’individu. Comme pour se débarrasser de cette empreinte (le comme ici n’est pas précaution oratoire, mais illustration de l’illusion<a data-link="09-b" id="l09-b" href="#l09-a"><sup>ι</sup></a>), on tente cette hypothèse. L’individu ou l’irruption du prosaïque dans l’envahissement poétique du monde.

Une nuit s’invente le retour à un passé de carte postale où mirer les souffrances grandiloquentes de celui que l’on croit être.

La nuit a ses hantises, ses souvenirs confus, remaniés. Notre démarche, <span id="perpetuelle">perpétuelle</span> altération, mimera, maladroitement, celle de la nuit, du règne de l’association d’idées quand la stricte rationalité est éclairage différé, suspendu. Des lueurs et des échos, des souvenances de livres que tu, par peur de briser l’enchantement, te refuses à relire<a data-link="32-a" id="l32-a" href="#l32-b"><sup>Θ</sup></a>.

*Gaspard de la nuit.*

La prose, l’individu est composite, t’en souviens-tu ? L’individu comme modernité poétique. Moments d’amalgame entre ontologie et esthétique<a data-link="33-a" id="l33-a" href="#l33-b"><sup>Ι</sup></a>. On le rejoue sous une autre forme : l’essai se fait poétique non tant comme la création d’une autre langue, d’une langue à Soi, mais comme l’intrusion embarrassante, contradictoire, de la beauté.

<div class="notemarge"><a class="notemarge__lien" href="#m19-l" id="m19-n"><sup>&ast;</sup></a>Pourquoi ne penserions-nous pas l’individu, s’il s’agit de s’en débarrasser, à partir de ses similitudes ? Peut-être moins vaines que ses différences, l’individu serait un commun. On reviendrait alors à ce que l’on en partage plutôt qu’à ce qui, mythe romantique, nous sépare.</div>

L’essai manifeste l’exigence d’une langue à Soi. Au risque du manque d’originalité<a class="notemarge__lien" href="#m19-n" id="m19-l"><sup>&ast;</sup></a>, il faut préciser que Montaigne pratique lui-même, sans référence, le collage de citations, d’emprunts. Il se peint lui-même en piratant allègrement ce qui l’entoure.

À la seconde revenance de la modernité, l’individu se définit, chez les poètes, dans les à-côtés de leurs poèmes où se dessinent ce qu’ils rêvent de souveraines esthétiques. Déjà la certitude de parler seulement de ce qui aurait pu être. Bien sûr, tu l’auras compris, dans un jeu de reprises et de correspondances, reprises et dédits. Aloysius Bertrand, derrière Baudelaire ; *Les Fleurs du mal* à la lumière de *Mon cœur mis à nu*. L’individu comme interstice. Écart au support qui ne parvient à le définir.

<div class="auteur">
[#Bertrand]
</div>

La nuit, symptôme de l’insuffisance des jours. Étouffement, sensation d’espace restreint, de temps trop étroit : on invente, on espère d’autres intervalles. Insomnie. La nuit, plus plate définition, comme temps gagné, soustrait. On n’échappe pas ainsi à la figuration romantique, image d’Épinal du poète.

Pour l’approcher, pour nous contenir dans du connu, revenir à Leiris. *Essai*, selon ses propres mots, de *self-fabrication*, où le mirage autobiographique viendra se confondre avec l’extériorité de la figuration du poète. Un mythe, Leiris le sait par sa fantomatique carrière d’ethnographe (fantomatique ici n’est en aucun cas dépréciatif ; il y chercha, et trouva, des fantômes : celui qu’il a été, ce qu’il pourrait toucher du doigt, les survivances des choix faits dont ainsi se maintiendraient les options, les virtualités), se vivra(it) toujours à l’extérieur. L’existence poétique devient un absolu. Parfois, s’y soustraire, pour mieux, qui sait, l’inventer autrement.

<div class="auteur">
[#Leiris]
</div>

La preuve du génie est d’inventer un cliché. La nuit en serait-elle devenue un, de cliché : des figures automatiques auxquelles on revient sans qu’elles ne mettent en jeu<a data-link="34-a" id="l34-a" href="#l34-b"><sup>Κ</sup></a> leur réalité, et la nôtre ? En partie, sans doute. Sauf si on parvient à cette définition, trop grande comme toutes devraient l’être : la nuit est le moment de l’épreuve de nos manifestations, affirmations, tentatives de définitions de ce que, le jour, agressivement, nous tentons d’être.

<div class="auteur">
[#Baudelaire]
</div>

<span class="citation">--- La figure de pierre avait ri, --- ri d’un rire grimaçant, effroyable, infernal --- mais sarcastique --- incisif, pittoresque.</span>

<div class="auteur">
[#Bertrand]
</div>

Qui n’a jamais été réveillé par l’inanité de ce qu’il est, de ses croyances, ne connaît pas l’épreuve de l’individu, la <span id="distance">distance</span> à celui qui l’éprouve. Nous sommes loin de pouvoir lui communiquer. La seule chose qu’un livre désinvente est une réserve d’incertitudes, au mieux, après la nuit, la poursuite d’une heureuse intranquillité.

La nuit romantique. Manière de déléguer l’acuité du vécu au poète ? Cette posture induit une certaine culpabilité sociale. Nous ne voulons pas réduire l’insomnie à sa possibilité bourgeoise, au confort de la vie matérielle qui la rend possible.

Nous ne pouvons pas, non plus, l’oublier. L’insomnie ne se réduit pas à un épuisement physique, néanmoins, l’asservissement et l’aliénation d’un travail physique, à horaire fixe, quotidien, sans échappatoire, lui donne un autre visage. Peut-être faudrait-il éprouver l’insomnie dans sa part de coupable comédie, de caricature aussi.

<span class="citation">Le penchant foncièrement antiscientifique de tout pessimisme romantique, qui veut transformer quelques expériences personnelles en jugements universels, les amplifiant jusqu’à vouloir condamner le monde.</span>

<div class="auteur">
[#Nietzsche]
</div>

Bien sûr, lectrice & lecteur, ce qui se dit ici ne recouvre, sans doute, pas exactement ce que tu prends pour ton expérience propre de l’insomnie. Tant mieux, diffères-en, écris-en le récit, continue à te dissocier de ce que l’on te dit de ressentir. On l’écrit quand même pour écouter tes contradictions. Pour donner un autre nom (l’essai n’est que cela : trouver d’autres non<a data-link="35-a" id="l35-a" href="#l35-b"><sup>Λ</sup></a> à ce qui a déjà été dit, échapper ainsi à l’illusion de l’originalité, de l’individualité) à l’insomnie : la fatigue de Soi.

La nuit est préservation, on l’approche de l’extérieur.

L’individu s’esquive par tout ce qui nous y ramène, dans un flottement sans Soi, mais avec une perception particulièrement intrusive de ses limites. Les jours qui suivent les nuits sont plus blancs que les nuits pâlissant à cette nuance. Là s’y trouve, je crois, l’épreuve de ce que l’on y a ressenti, son ineffable inanité, sa dissipation. Surtout d’y survivre, d’en revenir.

On parlera de nuit dans son acception la plus large pour ne pas la reléguer à d’intangibles barrières temporelles. On aspire au brouillage d’une intrication quantique. Nuit, préfiguration de l’inversion. Opacité communicative de tout ce qui nous entoure. Là encore, vision de ce que l’on n’a pas vécu, que l’on caricature par automatisme.

Nos nuits électriques ont une idée romantique de celles à la coûteuse chandelle ; nos vies minutées ont une idée confortable du temps infini d’un avant insitué. Et pourtant, miracle d’une histoire non linéaire : on touche, parfois, cette absence de limites d’un temps sans rien, pour un Soi qui n’existe pas.

Épreuve de la dissemblance, la seule chose qui importe. Peut-être.

La spontanéité rattrapée, contrastée, par l’épreuve de la nuit. Écrire le jour d’après, dans l’épuisement, le doute. Ensuite, fatigue, revient la certitude de l’imperfection, de ce qu’il faudra reprendre, redire avec une autre voix que celle limitée qui dans la nuit résonne. Des contrastes, l’expression d’un doute.

Impatience, irritation --- divagation : tout ceci ne suffit pas.

La même croyance, sceptique, en ce que j’écris que celle accordée aux pensées ressassées au long d’une insomnie. Tant qu’on ne les écrit pas, qu’on ne les formule pas, elles paraissent illuminations définitives. Ensuite tout paraît commun, déjà dit. Ou rien qu’un enclencheur, le départ d’une intuition, une concrétisation en souffrance. Qui n’a jamais été profondément insatisfait de ce qu’il est, des traductions qu’il parvient à en figurer. Tout ceci pour ceci : l’insomnie aussi insupportable, inévitable, que l’individu. <span class="citation">Attiser dans l’âme un insupportable étonnement.</span>

<div class="auteur">
[#Dickinson]
</div>

Sans résignation, enthousiastes, nous dirons les instants de soulèvement<a data-link="36-a" id="l36-a" href="#l36-b"><sup>Μ</sup></a> où l’individu s’éprouve.

À l’écart de cette littérature qui, bien à l’abri de son confort bourgeois, se livrerait à l’apologie de la douleur. Mais, il ne s’agit pas seulement de parler de ce qui nous passionne, plutôt de ce qui nous transporte. L’<span id="ombre">ombre</span> de la métaphore hante l’essai. La nuit comme mouvement. Épreuve du cliché. La nuit remue, elle serait façon de fixer ses vertiges, selon ses formules canoniques.

<div class="auteur">
[#Michaux #Rimbaud]
</div>

Un remuement<a data-link="10-b" id="l10-b" href="#l10-a"><sup>κ</sup></a> au fond des tripes. Une inquiétude qui anime, demander seulement cela à l’inexistence de l’individu. Un instant suspendu. Préserver les failles de la nuit, la garder irrécupérable.

Hélas, la nuit a été livrée à l’ambition, à la fabrique d’un individu conquérant qui rogne sur son temps de sommeil pour travailler encore, comprendre plus longtemps, conquérir les instants de doutes possibles. On ne devrait pas être étonné : un des archétypes de l’individu est Rastignac. Il paraît que l’on se doit d’être entrepreneur de soi-même, productif insomniaque. Équivoque amalgame du personnage et de l’auteur : Balzac et ses incessants cafés, écrivain de nuit pour rattraper ses déplorables investissements diurnes.

<div class="auteur">
[#Balzac]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m20-l" id="m20-n"><sup>&ast;</sup></a>Une permanente proposition, et pourquoi pas ?, une virtualité. On perd tous les noms pour dire le dédoublement, la non-linéarité, qui s’essaie ici.</div>

La nuit, méthode d’errance<a class="notemarge__lien" href="#m20-n" id="m20-l"><sup>&ast;</sup></a> et de désappartenance. Tu l’auras compris, il ne s’agit pas de dire ce que la nuit a été, ce qu’elle devrait être, mais d’en ouvrir les possibles. La nuit, le neutre. C’est de là que, de jour comme de nuit, je m’exprime. Restituons à la nuit sa vitale inutilité. Pas plus que la logique, surtout celle mise en œuvre dans cet essai, elle ne prouve quoi que ce soit. Nous avons aussi exactement tort, ou possiblement raison d’ailleurs, qu’une phrase venue dans un état de veille incertain.

Oblomov autant, simultanément, que Rastignac. Celui qui dort, s’agite dans des rêves incertains, peut-être en sait, souterrainement, autant que celui qui passe sa nuit, désespéré, à griffonner des carnets, à entasser des élucubrations qui l’enferment dans les limites de sa perception, trop souvent dans un ressassement de son ressenti.

L’individu ou l’ambition du personnage.

<div class="auteur">
[#Gontcharov #Balzac #Dostoïevski]
</div>

Le sommeil contre-pieds. Paradoxe trop facile. Peut-être ne vaut-il, comme l’insomnie d’ailleurs, que s’il est discontinu, incertain.

Ne pas réduire la nuit à l’absence de sommeil induit d’interroger la pensée comme intranquillité, l’individu comme absence de repos.

Tout est foutu quand on commence à savoir qui l’on est, à asseoir cette certitude avant que de l’imposer. Aucun vade-mecum, le sommeil est sans issue.

L’envisager pourtant comme étonnant, quotidien, consentement à la disparition. Le dormeur docile, celui capable de se perdre chaque soir, d’abandonner sans regret les oripeaux de son identité diurne ? Sans doute pas entièrement. Le rêve comme vie seconde, décryptage et enfermement dans la personnalité de notre première et unique vie. Forme de prévoyance, épargne de Soi, simultanément accès à un temps dilapidé.

Pourtant, malgré tout<a data-link="24-b" id="l24-b" href="#l24-a"><sup>ω</sup></a>, le sommeil rêve une <span id="decisive">décisive</span> épreuve de l’individu. Une des seules expériences collectives dont, tous, nous pouvons témoigner. Contrairement à la mort et sa valorisation abstraite.

On sombre, on accepte que sans nous le monde continue.

Je valorise ce que je n’ai pas, j’invente, comme dans toute parole diariste, les limites qui m’empêcheraient d’être pleinement Moi ou ce qui, ne serait-ce pas la même chose, me retiendrait de me dissoudre dans une identité collective.

<span class="citation">Dans toute morale ascétique, l’homme adore une partie de soi comme une divinité et doit pour cela nécessairement rendre les autres parties diaboliques.</span>

<div class="auteur">
[#Nietzsche]
</div>

Une façon de parler toujours de la même expérience, de reconnaître, pour tenter de s’en extraire, notre individualité dans le peu qu’elle a à taire. Une expérience, en mode mineur, du vide. Te voilà dans le mouvant du terrain conquis.

As-tu essayé cette méthode ? Le sommeil te fuit (notons que comme dans toutes les formules, on peut facilement inverser sujet et prédicat<a data-link="03-b" id="l03-b" href="#l03-a"><sup>γ</sup></a>), pour t’y absoudre, tu tentes de faire le vide, de te noyer dans un tableau blanc, un vide intersidéral vite perturbé. Idéation intense serait le nom de ce qui vient parasiter ce vain désir de vide.

Un interstice imaginaire où se désinventera(it), avec la part de retour propre à toute figuration, l’individu.

Avec une progression calquée ironiquement sur une rhétorique qu’il faudrait, et pourquoi pas, envisager de dépasser, d’en outrepasser les binaires dichotomies, la nuit idéale serait un tiers état, un équilibre entre sommeil et insomnie. Avance-t-on vraiment (est-ce le but ?) quand on dit, par un recours assez fumeux, à la physique quantique, à la vision mystique<a data-link="37-a" id="l37-a" href="#l37-b"><sup>Ν</sup></a> : on pourra(it) être endormi et insomniaque, en train de se regarder dormir, en train de rêver que l’on fait une insomnie.

On touche à nouveau à la difficulté conceptuelle, à ce que nous serions prêts à concevoir comme le cœur, fuyant plus que mis à nu, de notre sujet absent. Rien que des pirouettes autour du vide. Non, ça on l’a déjà dit. Pas pour ça que ce n’est pas à refaire. Essayons une autre transfiguration.

L’individu serait une fiction, on y croit autant qu’à un roman, on se laisse déterminer par les éclipses, failles, contradictions, temps morts d’un livre à l’autre, d’une définition à la suivante, d’un alter ego à l’altération qu’il suscite.

Là encore, avoir conscience que nous dessinons un cercle, un tableau blanc sur lequel, pour s’endormir, dessiner, effacer, les silhouettes de l’individu que l’on ne saura(it) être.

<div class="notemarge"><a class="notemarge__lien" href="#m21-l" id="m21-n"><sup>&ast;</sup></a>L’individu est ce qu’il <span id="repousse">repousse</span>. L’auteur, les sujets qu’il ne parvient à traiter et qui, de livre en livre, reviennent. Pas une présence, une hantise.</div>

Ce serait, qui sait, le point de convergence, nocturne<a class="notemarge__lien" href="#m21-n" id="m21-l"><sup>&ast;</sup></a>, du sommeil et de l’insomnie : une projection songeuse, enfermée, de ce que l’on pourrait devenir, de tout ce dont on repousse<a data-link="20-b" id="l20-b" href="#l20-a"><sup>υ</sup></a> la survenue, la traduction en gestes ou autres définitives (définitoires) apparitions.

La nuit ou le règne du passager, du transitoire. Le seul moment, peut-être, où l’on peut essayer ce que l’on n’a pas envie d’être. On écrit, sans support, le récit de ce que l’on pourrait devenir, on joue à se détourner de la fin prévisible de ce genre d’histoire ridicule. Et il mourut seul, abandonné, espérant jusqu’au dernier instant que son individualité, à laquelle il prétendait se soustraire, allait être reconnue, imitée même, qui sait.

</div>

<div class="text" data-id="5">

# Et, schize de l’individu...

L’origine de l’individu s’élancerait de sa formulation première : un défaut d’assiette dans lequel se spéculer --- se scinder. Ensuite, selon les époques et leurs contestations, on a inventé d’autres traductions à ce perpétuel dédoublement, à cette conscience des contradictions qui caractérisent l’individu.

<div class="auteur">
[#Montaigne]
</div>

Si on projette une définition nouvelle, en altération permanente, de l’individu, ce serait dans une volonté de ne pas en réduire, moins encore en résoudre, l’originelle schizophrénie. Disons une division consciente d’elle-même, séparée par cette perpétuelle conscience de cette dichotomie. On pourrait l’appeler permanente distanciation, si elle est, bien sûr, distanciée de cette distanciation. Insomnieuse incroyance.

L’individu : fantôme auquel, dans notre époque qui se croit rationnelle<a data-link="26-b" id="l26-b" href="#l26-a"><sup>Β</sup></a>, on ne parvient jamais tout à fait à croire, pas davantage cependant à se passer.

<div class="notemarge"><a class="notemarge__lien" href="#m22-l" id="m22-n"><sup>&ast;</sup></a>La circularité comme manière d’échapper à la linéarité du progrès, à sa croyance d’une émancipation progressive.</div>

Des modernités dès lors pour dire la distanciation à notre projet. Nous connaissons les meurtrières impasses de l’Homme nouveau. Considérons l’individu comme une suite d’altérations successives, pas nécessairement animées par le progrès<a class="notemarge__lien" href="#m22-n" id="m22-l"><sup>&ast;</sup></a>, souvent hantées par les régressions, les fantômes de ce qu’il a été et, croit, un instant, pouvoir redevenir.

On se rétracte face à une conception de l’individu le limitant à ses imperfections, on préfère le dire hanté par son angoisse, l’intranquillité qui le fait <span id="outrepasser">outrepasser</span> les figurations où l’on voudrait l’enfermer.

L’un des moins mauvais masques de l’individu, rendu à sa schizophrénie, serait l’hétéronyme. Ou, solution moins extrême, l’expression de l’individu dans la polyphonie romanesque, à la frontière du poétique<a data-link="33-b" id="l33-b" href="#l33-a"><sup>Ι</sup></a> pour être moins inexact. L’invention d’une origine sans trêve redite.

Une sentence en suggérerait l’incandescence primale sans qu’il ne soit question d’en juger de la primauté : <span class="citation">Je est un autre</span>. Inutile, *je* crois, de gloser sur la charge ironique de cette phrase. Celle que l’on entend en écho dans cette autre célèbre formule rimbaldienne presque trop connue : <span class="citation">il faut être absolument moderne</span>.

L’origine de l’individu moderne serait alors pure distanciation ironique, au carré pour ainsi dire, sans réparation en tout cas. Une sorte de vengeance, précise la formule, ensuite le silence et le mythe, enfin l’individu dans l’exégèse que l’on en fera, déjà dans sa réduction biographique. Rimbaud, l’incarnation terminale de l’individu.

Un peu idiotement, le penser ainsi : la formule détache de ce qu’elle exprime.

<div class="auteur">
[#Rimbaud]
</div>


L’individu, s’il se dit, déjà ne s’appartient plus, éprouve le creux des formulations où, un instant, il a cru se cerner, où à un autre moment l’individu, lectrice & lecteur, identifie ce qu’il voudrait être. Peut-être est-ce d’ailleurs dans cette mésentente réciproque que s’entend le lien entre autrice & lecteur et auteur & lectrice.

Alors, hélas peut-être, l’individu est réduit à ce qu’il fait, plus seulement à ce qu’il en dit. L’individu dans l’anecdotique, la croyance à son appartenance à l’événement, sa décisive participation à l’instant historique. Le moment moderne : celui où Fabrice del Dongo l’emporte sur Aliocha Karamazov.

<div class="notemarge"><a class="notemarge__lien" href="#m23-l" id="m23-n"><sup>&ast;</sup></a>Nommons, comme Caillois, parenthèse cette propension à ne parler que de ce que l’on a déjà lu. La valeur d’expérience de ces épreuves de l’individu tiendrait autant à cet enfermement textuel qu’à son effondrement, son outrepassement : effraction de l’irrésolue singularité dans ce qui se sait, et se veut, commun. Schizophrénie, minimale de tout ce qui se dit.</div>

Dit avec une simplicité trop grande : l’individu ravalé au rang de témoin. Son importance réduite à celle de ce qu’il vit. Une des pires apories de l’individu est d’avoir voulu, réussi en partie, à réduire l’individu à l’intelligible. L’écart dans lequel il s’exprimerait serait comme un refus de la transparence définitive que chacun de nous sait ne pas être. Fort heureusement, nous avons une conscience fort limitée de ce que nous sommes, nous n’apparaissons que dans l’interaction, fût-elle textuelle<a class="notemarge__lien" href="#m23-n" id="m23-l"><sup>&ast;</sup></a>.

<div class="auteur">
[#Stendhal #Dostoïevski]
</div>

Peut-être la séparation de l’individu est épreuve de solitude.

Acclimater, heure par heure, l’insupportable Soi pour n’exister que dans l’expression de cette désaccointance, ce perpétuel désaccord, à des degrés divers, à ce que l’on écrit. Je parle pour ceux qui vivent à l’écoute de leur petite voix du dedans, celle qui commente toutes leurs actions, les regarde comme de l’extérieur. Qui en sait, hors de toute pathologie, la profonde irréalité.

On pourrait le dire autrement<a data-link="35-b" id="l35-b" href="#l35-a"><sup>Λ</sup></a>, comprendre trouver une autre origine et donc une autre fin, une finalité nouvelle, à cette dissociation de l’individu.

Une autre phrase, de Baudelaire, pour situer cette modernité individuelle : de la vaporisation à la centralisation du Moi. Deux formules pour dédire[^1-lepreuve-de-lindividu_10] l’invention de l’individu poétique. Après le miroitement, souvent maritime, du Moi dandy baudelairien, la dissipation<a data-link="38-a" id="l38-a" href="#l38-b"><sup>Ξ</sup></a> dans le spleen (osons une dissipation de l’influence romantique), Rimbaud s’absente dans ses illuminations qui, plus nécessairement, dépendent de sa présence à ce qu’il pourrait alors nommer sensations siennes. On aime l’idée, entendre l’incertitude qu’on n’irait pas vérifier, qu’il n’en soit pas le seul auteur. Un texte sans paternité ? Peut-être est-ce ceci la modernité, une œuvre dont l’auteur ne serait qu’une attribution fautive. Une querelle de spécialistes. Après *Une saison en enfer*, Rimbaud se serait-il tu ou aurait-il écrit, en collaboration avec Verlaine et Germain Nouveau, les *Illuminations* ?

Être absolument moderne, vengeance : écrire des livres pas seulement en son nom, achever sa disparition au moment où l’individu s’impose.

Une autre forme de dissipation du Moi qui interroge la modernité serait alors la clandestinité. Très tôt, face à l’affirmation de l’individu serait apparu, presque conjointement, l’ostensible désir de s’y soustraire.

Une distinction d’abord dans la douleur. Il faut le dire, l’individu est né dans le désir idiot de se singulariser, dans l’incapacité de percevoir comme tout le monde, dans le fantasme dès lors de se fondre dans la foule. Un désir de ressemblance jusqu’à la dissemblance.

Envisager les formes malheureuses de l’individu pour en dire le surgissement équilibriste. Sans doute, entre-t-il une part de frustration irrésolue, peut-être incommunicable, mais singulière sans doute pas.

De la souffrance nue, prudemment admettre n’avoir rien à dire.

Comme par répercussion poétique, on se croit plus compliqué, noyau irréfragable de mystère. Une identité secrète dont même notre intimité serait pâle reflet. On pourrait, au mieux, la miroiter sous un autre *non*.

Au plus secret des jours, dans leur irréductible banalité, l’individu devient un personnage, joue son identité comme celle d’un autre, d’une haute ressemblance dans la fiction de l’acuité. Parfois, ça coïncidera(it) presque. Avec cependant la réserve de la grandiloquence.

On peut aussi l’envisager comme un autre discours de soumission : on se sentira(it) seulement lorsque pris en défaut. La seule façon de préserver l’exaltation sera(it) de rappeler que l’individu, comme le personnage, n’apparaît que par ellipses et intermittences.

Une illusion. Par déception, on accepte insidieusement de ne pas pouvoir retranscrire l’individualité dans son intégralité, de ne plus vouloir en donner à voir les instants nuls, les silences ou les répétitions dans lesquels il se déconstruit.

<div class="auteur">
[#Breton]
</div>

L’individu en tant que répétition : une formule qu’à lui-même il se répète, essaie d’abord devant sa glace comme on parle de répétition au théâtre, puis parvient à la conformer à ces gestes, à en imposer sa version à autrui.

L’écriture, d’emblée, vient contester, sans nécessairement complexifier, cette identité d’abord mondaine, puis sociale. Une sorte de transmission incertaine : de Montaigne aux moralistes ; de Rousseau aux diaristes. Joubert, Amiel. Une compréhensible réprobation sociale, associée à une aisance financière, fait naître l’individu dans son retrait (avec la part de comédie qui entre dans toute solitude choisie, parade malheureuse) du monde.

L’individu ou l’illusion que l’on pourrait choisir, seul, ce que l’on est.

L’illusion revient sporadiquement selon les croyances du moment. Se construire seul, à l’écart de tout discours, se déterminer autour de ce qui ne tarde pas à devenir discours unique, conformation à tous ceux qui l’ont précédé. Chaque discours libérateur porte en lui de nouvelles limites. Tu l’auras compris, en partie, avec détachement, nous en faisons ici l’épreuve.

Se soustraire de nos suppositions excessives.

Le journal : épreuve de la contingence de l’individu. On ne peut, dans ces cas manifestes, le restreindre à une thésaurisation de l’individu, pas plus d’ailleurs qu’à sa théorisation. Accumulation, l’expérience de l’inexistence. <span class="citation">Incapacité pure et simple.</span> Dans les interstices, dire l’individu quand il ne se passe rien, pour qu’il ne se passe rien. Immobile, mentalement ou physiquement, il n’existe pas. Instable, il subsiste dans sa part d’ombre.

<div class="auteur">
[#Kafka]
</div>

Nous sommes les livres que nous n’avons pas écrits, les gestes que nous n’avons pas faits. Nous sommes, tacites timidités, les retraits dont nous ne saurions nous revendiquer. <span id="defaites">Défaites</span> autant inconsistantes qu’inconscientes, sans autres spectateurs que nous-mêmes et notre désir de nous confier à cette maudite (si bien dite) esthétique du rabaissement.

Par une sorte d’imitatif automatisme de discours, l’individu apparaît dans son égotiste détestation. Le moment de préciser, encore, notre méthode qui s’invente à l’essai : elle se rêve non linéaire, c’est-à-dire non comme un exercice d’érudition (il s’agirait seulement de parler de références reconnues, de partager alors un savoir validé par ses pairs), mais comme la poursuite, le développement par des détours et autres dérobades, d’intuitions.

Suspension, silence.

Une parole sans Soi ne veut pas dire une parole qui ne se situe pas. Nous n’avons pas perdu l’illusion de dire le maintenant[^1-lepreuve-de-lindividu_11] littéraire. Comment aujourd’hui lit-on des journaux ? En quoi notre moment historique en aurait (disons par ses pratiques numériques, par le débordement du genre diariste dans une grande part de la littérature) désinventé la réception ? Parler, peut-être, de cette instantanéité ante-mortem, virtuelle, de journaux diffusés, gracieusement, au jour le jour.

L’individu, celui qui s’écrit s’entend, se heurte à l’épreuve d’un nouveau cliché. Chaque phrase qu’il commence est hantée par le spectre de passer à autre chose, de tourner la page, d’accéder, s’il est plus proustien, à l’oblitération de ses Moi différents. Le journal, et l’individu qui s’y invente, est séparation, fuite momentanée, de la primordiale pulsion de mort qui l’anime. Platitude : l’individu qui ne cesse d’y apparaître ne cesse de vouloir y disparaître.

<span class="citation">Je me demande s’il n’a pas existé d’autres êtres --- nous-mêmes autrefois --- dont nous percevons aujourd’hui, ombres de ce qu’ils furent, la plus grande complétude, mais de manière incomplète, une fois perdue leur consistance que nous pouvons tout juste imaginer, réduite aux deux dimensions de cette ombre que nous vivons.</span>

<div class="auteur">
[#Pessoa]
</div>

On pourrait penser que la mort, si présente dans un genre auparavant posthume, déplacerait l’ombre portée de la disparition. Un bel impensé d’ailleurs d’Internet : la postérité numérique. On pourrait alors te parler de l’identité numérique. On ne le fera pas : notre indifférence pour ce sujet serait sans doute trop révélatrice de notre pauvre singularité.

Croire que l’individu peut se définir autrement que dans ses refus<a data-link="01-b" id="l01-b" href="#l01-a"><sup>α</sup></a>.

Reprenons autrement. Écrire, c’est malgré tout affirmer une mémoire, ce dont il faudrait se souvenir et qui, affiché, s’expose à l’oubli. On parle bien sûr ici de notre essai expérimental : on écrit, on relit<a data-link="32-b" id="l32-b" href="#l32-a"><sup>Θ</sup></a>, on choisit ce que l’on veut garder, on écoute le hasard des citations qui reviennent.

Cette altération définitive, cette perpétuelle correction, ne dépend dans le journal que de Soi. Peut-être est-ce là ses limites, sa grandeur. La grande part d’incompréhension. Dans la lecture, souvent posthume, d’un journal se lirait ce qui ne lui appartient pas. On y présume une terrible transparence entre l’auteur et le personnage qu’il, à distance, aurait pu être.

Une des scies du journal, le perpétuel amendement de Soi, dessine dans sa litanie une sidérante distanciation de l’individu à lui-même. Admirable réussite : l’autrice & l’auteur s’y ennuient, souvent autant que la lectrice & le lecteur, à affronter, jour après jour, le détestable personnage que parvient, fort rarement, à ne pas être le diariste.

Entre poésie et journal, l’individu enfin s’invente dans sa fragmentation. De plus en plus difficile de le lire comme un tout linéaire à cause, précisément, de sa tentative désespérée de donner un ordre continu à ses jours.

À moins que ce ne soit une question de rapidité de lecture.

Devrait-on lire un journal à la lenteur où il s’écrit, une suite de phrases sans solution de continuité ? Le journal est le lieu d’une identité générique composite. Un journal sans carnets n’aurait pas le moindre intérêt. On lit des brouillons, des bribes d’idées qui enfin acceptent leurs contradictions. Un journal potentiel<a data-link="39-a" id="l39-a" href="#l39-b"><sup>Ο</sup></a> est celui où l’individu serait pareillement à l’essai. Une façon d’essayer des personnages, d’avoir plusieurs identités momentanées, avortées, ratées ou réussies, sous un seul nom.

Ne rien en vérifier : qui a décidé qu’une vie devait être un projet viable ?

Pessoa serait alors le diariste divinisé. Bien sûr, on ne saurait ramener toute sa prose à une pratique du journal, bien sûr on se moque de l’identité générique. Prendre alors garde à la réduction de l’écrivain à son mythe, son évocation dans des images trop connues (la malle de Pessoa) alors qu’il faudrait inventer à chaque fois l’individu comme décalque, détour. S’estomper derrière des hétéronymes, d’autres noms pour d’autres figurations. Des <span id="indecisions">indécisions</span> et des flottements. Chacun de nos noms est un refus. On ne saurait se résoudre à ce qu’il dit et limite.

Écrire, outrepasser, jusqu’à l’implosion<a data-link="40-a" id="l40-a" href="#l40-b"><sup>Π</sup></a>, la dénomination.

Pessoa, posthume : la révélation des identités multiples unie arbitrairement à la pluralité de ce qu’il a été. Évoquer sans réduire à une cohérence (celle, rêveuse, du refus du monde), esquisser, esquiver<a data-link="41-a" id="l41-a" href="#l41-b"><sup>Ρ</sup></a> pour une lecture <span id="plurielle">plurielle</span>. Je parle ici de Bernardo Soares, de la façon dont il transforme cette expérience de n’être plus personne, un banal comptable (l’image déformée que l’on se fait de Kafka derrière Bartleby) égaré dans ses rêveries et absences, mais toujours rattrapé par l’intranquillité.

La dépersonnalisation ne suffit pas, l’absence d’individualité est mirage transitoire.

Un instant d’inattention, nous sombrons : tu parles de toi, là, non ? La lectrice, & le lecteur, pourra s’amuser à situer ces instants de retour à Soi, conseillons-lui plutôt de s’intéresser à ce qu’il voit de strictement personnel dans ce texte plein de failles, l’illusion trop tranquille de prêter un point de vue qui, sans doute, est avant tout le sien.

Une présence comme une autre quand elle est intranquille incertitude. Interroger n’est pas enterrer[^1-lepreuve-de-lindividu_12]. On croit pointer une esthétique, la dater et donc l’enfermer dans une délimitation temporelle, façon très présomptueuse de laisser entendre qu’elle est datée, pour ne pas dire dépassée. Si on reconnaît ici la trace de mes obsessions, ce serait dans leurs revenances. Elles reviennent, mal enterrées, me hanter, éclairer qui sait le présent d’un jour non pas nouveau, mais dont la discontinuité inquiète, réapparaît comme une question irrésolue.

L’expression, une fois encore, d’une méthode dont je voudrais sinon m’affranchir, mais parfois, comme on se sent exister en pressentant l’inédit paradoxal d’une contradiction crue nouvelle, m’excepter. L’individu, l’exacerbation de la conscience de ses impasses, souvent dans toute leur matérialité. On n’écrit ici que ce sur quoi on a déjà écrit. Par une sorte de paresse, admettons-le. Par la croyance aussi de donner une autre forme à ce qui, par intranquillité, revient.

Un des multiples noms de l’individu. Une croyance un peu vaine dans l’altération, une réclamation de la polyphonie comme expérience limite (interrogation par l’épreuve) de la schizophrénie prétendument à l’œuvre dans la littérature.

Tout propos est constitué surtout de ce qu’il tait. La part d’inconscience ici pourrait se réduire aux lectures dont on ne parle pas, aux références implicites qu’elles tissent, des exceptions aussi à un jeu de références entendues, définitoires. L’individu informé par ces « mauvais » films, livres, rencontres. N’en parlons pas, pointons seulement cette dissimulation. Occasion d’admettre aussi être marqué par la volonté, vouée à l’échec, de dire l’individu dans sa totalité. La pluralité dit la folle volonté de faire le tour de ce que pourrait être l’individu, de croiser ainsi, qui sait, un de ses visages inéprouvés.

<span class="citation">J’ai cassé ma voix en plusieurs personnes, mais je ne suis pas assez nombreux.</span>

<div class="auteur">
[#Richard]
</div>

Dire aussi cette perpétuelle velléité de passer à autre chose, toujours, tout le temps : ne pas approfondir, ne pas appesantir. Ressentir en Soi ce qui serait, dans un discours certes un rien attendu, les limites de l’individu contemporain.

Continuer à situer, autrement, l’absence d’où je parle.

Comme s’il fallait à nouveau s’élancer de ce leurre : il faut savoir n’avoir rien à dire pour emprunter la parole d’autrui<a data-link="25-b" id="l25-b" href="#l25-a"><sup>Α</sup></a>, la laisser résonner dans ce vide que l’on croit, quoi que l’on fasse, sien.

À l’impasse de toute parole, surtout sur Soi, se substituent des séparations de secours.

Parler précisément en restant sautillant, insaisissable, particulier et général, comme tout individu serait l’improbable synthèse, se rêverait tel en tout cas, entre le subjectif et l’objectif. Nous ne retraçons pas l’histoire de ses échecs successifs, de ses réussites parcellaires, juste dire que cette obsession revient, aux instants creux surtout.

On lit pour capturer des figurations transitives, des individus non retenus, rescapés pour ainsi dire de toute figuration publique. Comme si le plus secret, le plus caché, le révélé trop tard, contenait une vérité, réduisait à une explication. Façon de poser une inadéquation définitive : on encode sa douleur, la vérité imparfaite sur ce que l’on est et qu’elle est censée contenir ; on n’a pas perdu l’illusion de s’en séparer en l’écrivant, avant qu’elle ne manifeste une certaine fidélité, que nous avons tous, pour ce que nous avons été. Surtout si cette figuration première se teintait d’une certaine acuité.

Crie autant la crainte de te résoudre à tes imperfections que celle de les acclimater, de les accepter, de finir par les croire tiennes. Mais, distanciation de chaque énoncé, le souvenir aussi d’une croyance, jamais abolie, malgré tout en un engagement total, une adéquation impossible, sonore et rythmique, poétique à ce que l’on dit.

Stylistique de la dissociation.

Une convention à interroger, une fiction : faire comme si, tant que l’on écrit, on pouvait s’en détacher, comme on repousserait une fatalité, on en fait une parade, un instant encore. Écrire, se séparer de la langue enfantine, de celle que l’on parle à Soi, comme s’il fallait ensuite s’efforcer d’y revenir, d’inventer un style qui permettrait de croire affirmer une singularité qui, au mieux, est loin de dépendre que de nous.

Le Journal, la désinvention de rituels pour repousser la survenue de ce que l’on appelle, craint, puisque l’écrire revient à l’amalgamer à ces deux antagonismes. L’invention d’interdits. Tenus ils valoriseraient, transgressés ils humilieraient, continueraient dans les nuances entre ces deux états à enfermer dans l’individu. On s’invente des obstacles, des oppositions. Tout pour ne pas admettre que nous n’avons rien à dire, ou plutôt rien à dire qui motive intégralement ce que nous croyons être. On s’invente des passions, on se croit plusieurs en Soi.

<span class="citation">Mon idéal, ce serait de tout vivre dans un roman, et de me reposer dans la vie --- de lire mes émotions, de vivre mon dédain pour elles.</span>

<div class="auteur">
[#Pessoa]
</div>

Et on n’a toujours pas parlé de Pessoa. Vraiment ? Peut-on parler d’un auteur ? Autant, après tout, que l’on peut parler de Soi. Pourtant, idéalement, il ne s’agit pas, ou plus, de faire commerce d’imposture.

On en vient alors à une question simple : de quoi parle-t-on avec plaisir, enthousiasme, sans reconduire l’excuse d’une universelle charlatanerie, expert cynique d’un temps censé s’abîmer dans son absence de certitude ? Ce qui revient, c’est un peu court, il faudrait alors envisager pourquoi cela nous a touchés, en quoi, comment.

Une des scissions à mettre à l’épreuve, à défaut de pouvoir réellement s’en séparer, serait celle de la répétition. On reconnaît si souvent une valeur d’expérience à ce que l’on connaît déjà, à ce que l’on revit. Une hantise intervient alors ici, nous la consignons précisément pour en suspendre la réponse : peut-on vivre (et serait-ce alors souhaitable), quoi que ce soit d’inédit ? On réduit l’envergure, on parle seulement de ce que l’on croit comprendre, de ce dont on se sentirait autoriser à parler parce que l’on (mais qui ?) en aurait fait une expérience proche.

La seule scission de l’individu dont je pourrai(s) (conditionnel ou futur, ce Je serait-il avant tout celui de la lectrice & du lecteur ?) témoigner sera celle d’un adieu permanent --- si long que tout aussi bien on peut le dire sempiternel --- à l’individuation.

Au jour le jour, la certitude que l’on sera(it) autre, que l’on se trahira(it), que l’on continuera(it) à s’inventer dans la non-conformité. Nous ne sommes pas exactement ce que nous disons, moins encore nous nous réduisons à ce que nous faisons. Un résidu, une part irréductible, une revenance, un fantôme, l’ombre d’un doute. Tout ce qui reste sans cesse, qui fixe d’ailleurs la mesure de l’instant, à tuer, à renier, à ressentir peut-être aussi.

<div class="notemarge"><a class="notemarge__lien" href="#m24-l" id="m24-n"><sup>&ast;</sup></a>Ici et maintenant, il nous est loisible de nous réclamer d’un postindividualisme. Reconnaissance de dette à tous ceux qui ont assuré, auparavant, cette impossibilité, cette utopie de l’individu.</div>

À ce moment précis, sans doute le vocabulaire de la psychanalyse s’imposerait à toi. Une piste d’interprétation<a class="notemarge__lien" href="#m24-n" id="m24-l"><sup>&ast;</sup></a> à notre goût un peu trop laissée de côté : l’individu dépend presque entièrement des conditions de sa production. Prétendre y échapper sans aucun doute nous situe, devrait dénoncer le confort patent dès qu’il serait loisible de s’inventer des échappatoires.

Ce serait d’ailleurs une des raisons de ce retour de l’individu, l’incapacité totale à le chasser : il s’inscrit toujours au miroir de sa matérialité. L’individu prend son plein essor au moment où le miroir se démocratise, où l’individu tente de se figer avec son reflet, où il se sent alors contraint de s’inventer une trouble intériorité, prémices de l’inconscient qui s’exprimerait dans la répétition et la résistance.

L’individu, une hantise. On se répète.

On se laisse toujours plus prendre qu’on le croit au discours dénoncé. Une longue proximité. Tant l’individu, fût-il écrivain, est aussi les gestes qu’il fait au secret des jours, ceux qu’il rêve au mirage des nuits.

Dans ce propos, dans son reliquat d’association d’idées, ne cesse de se manifester le déni. Le désir de le prendre à son propre jeu : faire son malin, faire <span id="signe">signe</span> vers les points aveugles pour créer de nouvelles zones d’ombre, des refuges, qui sait, pour l’individualité réticente tapie dans ce propos. En termes de littérature, la question se poserait aussi ainsi : peut-on devenir celui que l’on répète être ?

Imaginons un diariste confiné dans une solitude sans contour. Il deviendra ce qu’il affirme être : par ennui sans le moindre doute, pour continuer à écrire, par assaut d’inconscient, il finira par décrire son contexte, laisser échapper par quels minés moyens il y échappe, comment pratiquement il survit en se contemplant le nombril.

<div class="auteur">
[#Leiris]
</div>

Temps à nouveau, peut-être, de préciser cette méthode qui se déconstruit face à soi-même, dans l’épreuve continue, voulue aussi spontanée que simultanée, de ses apories. Ainsi, dans le ressassement l’individu croit échapper à sa fragmentation.

Le fragment, la parcelle d’être insuffisante qu’est tout individu, presque nécessairement reviendra.

Altération de la perception littéraire du monde qui sera(it) parvenue (à tort ou à raison, pourquoi trancher ?) à se convaincre qu’elle ne saura(it) rien percevoir en intégralité, qu’elle pourra(it) ainsi se permettre d’aborder tous ses truismes, pardon ses sujets d’une vitale importance, en les éludant, dans la conscience un peu fausse qu’il sera(it) toujours temps de leur trouver une autre forme, de les rassembler. Bref, de faire du journal, de l’être au brouillon qui s’y essaie, une <span id="figuration">figuration</span> achevée, une œuvre terminée.

<div class="notemarge"><a class="notemarge__lien" href="#m25-l" id="m25-n"><sup>&ast;</sup></a>Le spectre de Montaigne, bien sûr, hante ce désir de digressions.</div>

On le saura, l’individu est un pis-aller, un dérivatif, le support ondoyant à des digressions où, *a minima*, se rêve d’autres modalités d’être au monde, une présence-absence qui en oblitère les contours, en estompe les dominations. Ne fût-ce que langagières. Pour le moment, procédons ainsi : fixer un thème<a class="notemarge__lien" href="#m25-n" id="m25-l"><sup>&ast;</sup></a>, pour ne pas dire une identité, pour mieux l’outrepasser, en contredire les attendus, en dépasser ce que l’on en pensait.

Écrire pour entrer en dissidence avec Soi, en désaccord avec le Moi et les infinités de ses fausses apparences.

</div>

<div class="text" data-id="6">

# D’une dissociation de Soi : rêvons

Douteuse prémonition rétrospective quantique : l’individu fut d’emblée (pour ne pas dire à l’origine puisqu’on peut penser que l’individu naît dès qu’il commence à se rêver des origines) brouillé par son envers de rêve. C’est aussi, simultanément, cet envers, que nous voulons faire entendre.

Par un essai qui ne se dédouane pas de son approche poétique, parfois irrationnelle, souvent sonore, majoritairement (on l’espère) à l’écart des figures obligées de la grammaire, du style et de la rhétorique. Un essai qui pourtant ne saurait se confondre avec le rêve.

Encore un fantôme auquel il ne croit pas, pense pourtant que cette défiance en permet la survenue. Ce qui survient, naïve croyance, doit être redouté, repoussé, <span id="maintenu">maintenu</span> dans l’impossible. À toi de décider si on parle du rêve ou de l’individu.

Une défiance initiale envers le langage qui, dans le récit de rêve, fait l’expérience de ses limites. Même par un jeu d’absence de précision pronominale, il est pratiquement impossible de donner à sentir l’inexorable douceur, le flottement inquiet, dans lesquels l’individu se rêve, sans lui.

Vaine distinction, on parle seulement de ses songes. Encore un état intermédiaire incertain. Un moment d’engourdissement où, peut-être, se dessine la moins incertaine des projections de ce que l’on sera(it).

Une sorte d’indétermination heureuse dont nous te proposons cette image : cette sorte d’absence quand tu lis qui fait que tu perçois d’autres sentences, d’autres sanctions, oublies et déformes cette absence de Soi en lectrice & lecteur dont pourtant tu conserves ainsi une mémoire exacte, aux revenances d’une aléatoire évidence.

Peut-être est-ce ceci dont nous voulons faire l’épreuve : le travail inconscient du texte. L’effacement de l’individu qui s’y noctambulise, ivre errance qui remplace l’éclairage de l’expression convenue, consiste d’abord en ceci : on (celui ou celle qui parle et celle ou celui qui lit) s’efface, projette une inconsistante ignorance, croit ainsi assumer le défaut de toute instance narrative, réfléchissante, supérieure.

Une confusion égalitaire à l’image de celle qui se délite et s’aveugle dans le rêve : on y regarde celui qui agit dans la certitude, l’inquiétude qui en tient lieu, que ce n’est ni tout à fait nous ni tout à fait un autre.

<div class="auteur">
[#Verlaine]
</div>

Il est assez rare (mais peut-être n’est-ce là qu’un aveu personnel) que l’on se regarde rêver, il est si commun, hélas, que l’on se regarde écrire. Le rêve serait alors une écriture sans miroir individuel tant les fantômes qui y font l’épreuve de la revenance sont, paraît-il, sans reflet.

Apparent paradoxe : le rêve induirait un rêveur, l’écriture un individu. Une certaine réticence à réduire cette instance à un médium, vecteur sans identité de formes et de figurations.

On se trouve prisonnier de cette facilité, on change une conception dominante pour une autre, on s’enferme dans les oppositions.

Au miroir de l’inconscient freudien substituons, un instant, le lac jungien d’un inconscient collectif. Ce qui remonte du rêve ne serait ni sublimation ni décompensation d’une vie diurne, mais expression d’une structure, d’un mythe, qui le dépasse, une sorte d’insistante, et reptilienne, part commune<a data-link="42-a" id="l42-a" href="#l42-b"><sup>Σ</sup></a>. Voire.

Le rêve advient dans une défiante altération, l’abandon d’un réveil, l’inaccomplie contemplation depuis l’autre rive. Peut-être la plus simple expression de l’encombrant mythe du paradis perdu. Expression de cette séparation prétendument primordiale que la littérature s’entête à vouloir suturer, à maintenir sans doute ainsi l’individu dans une irréparable scission.

Posons cette hypothèse distanciée : le rêve fait si mal récit parce que son flux ne doit pas se figer, il ne doit jamais enregistrer un état de fait, se résoudre à ce qu’il est. On souhaite qu’il maintienne ouverte son interminable capacité d’altération, de reprise aussi.

L’individu apparaîtrait, sans fixité, dans la revenance de ses rêves, dans la tacite continuité qu’il croit y reconnaître, l’inscription du contexte devinée dans les interprétations et remémorations diurnes qui expliquent, peut-être, l’itérative survenue de son rêve.

Là encore, comme pour l’individu, dans l’illusion de le croire unique alors qu’il est sans doute volatilement pluriel.

Un désir contrarié de continuité qui, le plus souvent, trouve forme dans la linéarité de l’illusion biographique. Masochiste attachement à ce que la douleur aurait fini par accommoder pour sienne. Peut-être d’ailleurs tient-on là le meilleur lien entre rêve et individu : l’épineuse et possiblement insoluble question du style. Une composition avec ce que l’on ne parvient pas à exprimer plus clairement ; l’espoir inentamé que cette imparfaite expression soit porteuse de dérivations de sens, d’interprétations suspendues où d’autres pourront se rêver une continuité, une ressemblance dans la revenance.

<span class="citation">Toujours la même pensée, le désir, la peur. Je suis certainement plus calme que d’habitude, comme si une grande transformation était en train de s’accomplir dont je serais le frémissement lointain. C’est trop dire.</span>

<div class="auteur">
[#Kafka]
</div>

Cette ressemblance dans la revenance adviendrait alors dans l’inexactitude d’une superposition, le retour de ce que l’on n’est plus (faute, qui sait, de l’avoir jamais été, d’en reconstruire en permanence la mémoire) et qui fait signe vers d’imperceptibles altérations.

Si l’individu existe (tu l’auras compris, nous ne pouvons invalider cette hypothèse qui coexiste avec son inexistence) il apparaîtrait comme la somme fragile de ses permanentes mutations. Poussons même un rien l’hypothèse : dans l’immédiat hors-champ ouvert par le roman.

Ne pas oublier cette certitude : le romanesque met aussi en forme, en creux, l’individu. Qui n’a jamais rêvé de vivre dans un roman d’initiation, qui n’a jamais lu son parcours à travers le stupide schéma des passages obligés et autres risibles étapes rituelles ?

Qui ne rêve pas d’une détestable stabilité derrière l’instable incertitude dont sa vie, pas nécessairement par choix, se revendique ?

Restent les rejets, perdurent les reliques : des traces brouillées dont il serait aussi absurde d’être nostalgique que de l’être, au réveil, d’un rêve.

Une façon de préciser ce dont on s’écarte : la conception d’un individu terminal basée sur la rédemption. Après la traversée des illusions, après des épreuves dont la difficulté validerait la vérité, on accéderait à un individu achevé. Par une sorte, qui pis est, de retrait des idéaux de la jeunesse ou, à peine préférable, une âpre fidélité à celui qu’on a été.

Mais est-il si lointain, définitivement aboli, le temps où l’on pouvait, devait se réciter au passé simple ? À quel moment, par contamination économique, par parasitage du discours, on a voulu réduire les évolutions à un progrès dont la finalité ne serait pas pensée. Croissance et développement sans eschatologie, pas même un plafond de verre.

Le rêve viendrait alors interroger les instables revenances du progrès, de l’individu dans ses retours différenciés. Le rêve, *in fine*, est réactionnaire, un retour à Soi dont l’inscription collective reste, au mieux, fantasmatique. Il faudrait alors inventer un autre rêve, celui que rien ne soit, jamais, ce que l’on dit qu’il est.

Un interstice, une interprétation.

<div class="notemarge"><a class="notemarge__lien" href="#m26-l" id="m26-n"><sup>&ast;</sup></a>Inconvenant, sans doute, de le préciser : tout ceci s’éprouve dans, et pourquoi pas, l’ironie de l’onirique. On ne souligne jamais assez l’aspect drolatique, comique de distanciation, des projections rêveuses. Mimons-en le risible ridicule.</div>

Un doute malgré tout, une perpétuelle réticence face au manifeste. Une permanente controverse<a class="notemarge__lien" href="#m26-n" id="m26-l"><sup>&ast;</sup></a> face à que l’on affirme. Soyons précis, c’est déjà une contradiction dans les termes : l’incertitude, le flou toujours à visée artistique, serait une posture bourgeoise, un enfermement dans les limitations de son quant-à-soi, peu ou prou la reproduction de l’incommunicabilité de l’individu.

Nous voudrions marquer notre adhésion désaccordée avec ce propos. Il conviendrait de montrer, comme pour le rêve, une incompréhension passionnée. Sans doute ne comprenons-nous pas grand-chose à autrui, sans doute convient-il de réserver notre avis sans qu’il faille si résoudre. À l’instar du rêve, on sait pertinemment ne rien pouvoir comprendre à ceux d’autrui, on raconte pourtant, paraît-il, les siens[^1-lepreuve-de-lindividu_13].

On avance doucement, avec l’imprudence de la naïveté, comme pour raturer, différer de cette première stase du propos qui se laisse accroire pouvoir, dans une première étape qui --- tel le rêve --- attend son réveil, penser seul, rendre ainsi manifeste tout ce qu’il ne sait pas. S’exprimer toujours dans une réserve de silence, dans l’insidieuse rature de ce qui ne suscite aucune onirique tangence.

On parle des rêves que l’on aurait pu faire, des interprétations où se miroite le mirage d’un individu auquel, il faut bien l’admettre, il nous arrive, pas uniquement par commodité, de nous laisser prendre.

Cette démarche mime, dans la différenciation d’une reconnaissance onirique, la solitude en partage du rêve. S’il nous arrive d’y apparaître, le plus souvent réduit au sain statut de spectateur, ce ne serait que comme l’ombre d’un contexte, d’une culture, la retranscription (souvent silencieuse donc) des images amalgamées au sens qu’elles procèdent surtout d’un collage de mots, d’imaginaires livresques.

La lecture comme poursuite d’un rêve, recherche d’interprétation à tout ce que l’on a aperçu sans le comprendre.

Si l’individu existe, ce serait comme une intuition pour quelqu’un qui peine à y croire, mais, simultanément, ne sait penser hors de ce cadre.

Par paresse aussi, le rêve a sa torpeur émolliente. Un instant étiré auquel on voudrait croire, peut-être pour effacer la radicale incertitude de celui d’après, de son inacceptable continuité, qui devrait caractériser toute parole.

Tour de passe-passe, feindre l’absence de continuité, à l’instar de celle du rêve, d’unité dans ce propos. Signaler (indicateur vers une voie de garage) que c’est une similaire absence, contrariée souvent, qui ici s’exprime. Chacune de ses manifestations pointant un défaut. Une approche, un détour, une certaine mélancolie dont on peine à se détacher.

Un doute toujours rattrape : on vit, par déperdition, le sentiment de la perte quand on le pense universel.

Peut-être touche-t-on à un fond de commune indistinction si l’on dit que ce que l’on approche, dans la volonté de rattrapage du rêve, est la sempiternelle sensation de l’imminence. Peut-être situer cette perception de l’imminence donnera(it) une perception historiographique en croyant savoir comment on vit et sent l’instant, comment s’en écrit l’histoire essentiellement par ceux qui en pressentent, par avance en inventent, la survenue.

Interroger alors cette conception de l’Histoire comme le trop tard de la compréhension. Une leçon, sage et tiède, emplie d’impératifs pour dire ce qu’il faudrait faire, s’inventer une posture morale, des prétentions de principe, se voir grand clerc. Bref, se croire auteur de sa propre histoire.

Pire, d’un point de vue littéraire, de juger l’œuvre par ce qui, dans la vie de l’auteur, semblerait entrer en contradiction. Manière d’enfermer l’auteur, souvent consentant, parfois construisant sciemment sa statue, dans un personnage, une expression strictement individuelle.

Nous voudrions n’être rien de tout ceci. Comme dans un rêve --- dans cette conscience que d’autres l’ont déjà fait, ont déjà cru y échapper ---, se substituer à chaque figuration dans laquelle on pourrait me reconnaître : être capturé à l’instant où l’on est, déjà, ailleurs.

En d’autres termes, inventer, dans une langue désirablement fluctuante, l’instant de la métamorphose.

L’individu au moment où il devient autre chose, son expression serait capacité d’altération.

Artificiellement, on déterminerait un troisième moment où l’individu aurait fleuri au revers, pour ne pas dire à rebours, d’une certaine modernité aux contours toujours aussi flous, toujours politiquement polémiques, la postmodernité, considérée ici dans ses instants de revirement, voire de trahison. Ce serait d’ailleurs une des scissions à évaluer : comment l’individu croit pouvoir se penser à l’écart des idéologies de son moment ?

L’individu tient aussi aux trahisons de son émancipation, disons à ses récupérations dans un discours. On pourrait en faire l’épreuve d’une contre-vérité, un raccourci historique un peu honteux. La simple exploration d’un moment de revirement pour expliquer cette certitude : l’individu n’existe qu’impensable, il s’affirme avec le plus de force au moment où, naïvement, on pensait s’en être débarrassé.

C’est bien sûr, avant tout, de mon propos qu’il s’agit ici, il ne semble pouvoir avancer que dans une interne contestation, un défaut d’assiette, une scission à ce que l’on dit, on serait presque tenté de dire une mise en défaut de notre posture d’imposteur. L’authenticité maximum est sans doute d’avancer masquer en montrant du doigt son masque. Des hypothèses incertaines, transitoires, autant de stades à dépasser, à hypostasier.

<div class="notemarge"><a class="notemarge__lien" href="#m27-l" id="m27-n"><sup>&ast;</sup></a>Poussons, non sans ironie, l’identification. L’individu s’imite lui-même à partir au moment où autrui le reconnaît. Pour se maintenir, il se pose alors dans cette vaine alternative : s’imiter ou se trahir.</div>

Chaque courant littéraire, mouvement de pensée finirait par s’imiter<a class="notemarge__lien" href="#m27-n" id="m27-l"><sup>&ast;</sup></a> lui-même, serait poursuivi par des épigones non par des disciples (poursuivre œuvres et pensées, les trahir, leur faire dire ce qu’elles ne disent pas, penser contre elles). Dit trop schématiquement : la déconstruction aurait atteint son aporie dans un retour à l’individu, on aurait alors repensé la modernité (des deux époques) à partir de ce moment. Mouvement plus complexe qui s’accompagne aussi d’une trahison du désir révolutionnaire dévoyé en émancipation, revendication de la jouissance.

<div class="auteur">
[#Barthes #Aragon]
</div>

S’écarter de cette vieille idée que le langage, à servir, perdrait du sens. À l’oral peut-être, mais à l’écrit chaque nouvel emploi est hypostase. Alors, le sens du libéralisme aurait-il vraiment changé ? Difficile de penser l’individu comme exercice de liberté ?

On voulait parler, sans doute est-ce désirable de n’y parvenir, de la façon dont la logique du sens, chez certains, d’ailleurs toujours au nom de la jouissance, derrière l’affirmation du corps, est passée, disons autour de 1977 (*Fragments d’un discours amoureux* & *Histoire de la sexualité*), d’une négation outrée, structurelle, de l’individu à son affirmation. Prémices ou conséquences d’une confortable, libéralisme oblige, prétendue fin des idéaux.

<div class="auteur">
[#Foucault #Barthes]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m28-l" id="m28-n"><sup>&ast;</sup></a>Sans doute l’auras-tu compris, nous parlons d’autre chose, toujours. Ce raccourci dysfonctionnel trouve une figuration momentanée dans les liens hypertextes qui trouent cet essai, proposent une autre circulation.</div>

De la sémiologie à l’autobiographie, raccourci dysfonctionnel<a class="notemarge__lien" href="#m28-n" id="m28-l"><sup>&ast;</sup></a>. On le pose comme la perturbation d’une intuition, une évidence faussée qui continue à laisser entendre sa dissonance, comme ce qui impose la contradiction, insère un interstice, une façon de penser qui dépasserait les dichotomies.

L’intermède, la tierce, des deleuziennes machines désirantes. Sans avoir la tête philosophique, en admettant notre ignorance, on le comprend ainsi : par la sémantique, dans l’usure du langage à laquelle elle pourrait donner lieu si on imitait au lieu de s’emparer du nouveau rapport à la langue induite dans ce concept : après *L’Anti-Œdipe*, l’individu serait fatalement, irrémédiablement (au sens encore à interroger sans doute où l’eschatologie de l’époque ne paraît prophétiser aucune terminale union), dissocié, se manifestant seulement dans sa schize.

On pourrait l’entendre dans cette définition minimale : l’individu ne peut, désormais, que prendre en compte les discours, le pouvoir qui l’impose, il se sent déchiré, rêve d’un autre modèle, par quark ou rhizome, où réinventer ses liens, se réfléchir, tenter de se penser hors de sa singularité. Un étoilement de la pensée qui, pourtant, reste contre-intuitif, semble parfois sans traduction. Néanmoins, restaurer la pure abstraction de l’individu, un concept collectif dont chacun porte en lui une conception défaillante serait peut-être la manifestation la moins faillible de ce qui est latent.

<div class="auteur">
[#Deleuze #Raphmaj]
</div>

L’individu se conçoit comme un devenir, la somme d’une mémoire en continuelle actualisation, déformation, comme pour coller avec la concrétion de ce qu’il souhaite, entre <span id="futur">futur</span> et conditionnel<a data-link="43-a" id="l43-a" href="#l43-b"><sup>Τ</sup></a> donc, devenir. On ne sait si l’on peut, si cela est désirable (comprendre apporte de décisives dérivations de pensée, des désaccords), le réduire à son déni et son refoulement.

Pourtant, l’individu est ce qu’il <span id="tait">tait</span>, en ce qu’il a, ou aura(it), d’inguérissable. On touche, alors, à l’époque, à une sorte de terminale aporie psychanalytique. Par l’envahissement de la jouissance, par la pire des croyances qu’elle puisse être unique, on l’a transformée en fonctionnel fantasme, on a cru que l’individu pourrait se construire hors de ses névroses. Traverser l’individu, par l’épreuve de ses différentes manifestations, à l’écrit surtout, n’est somme toute peut-être que l’une de mes névroses[^1-lepreuve-de-lindividu_14].

Une façon de mettre à l’essai, la sororité entre écriture et rêve. Au nom de la même histoire : confondre objet et sujet. Une écriture de rêve, qui vienne du rêve pour mieux y retourner en la suscitant chez la lectrice & le lecteur. Mais comment ? Peut-être par immersion dans le bain du rêve des autres<a data-link="42-b" id="l42-b" href="#l42-a"><sup>Σ</sup></a>, l’immersion dans le déjà-vu qui en constitue la fugitive inspiration primordiale.

Le rêve met en partage l’absence d’inédit de ses figurations ; l’écriture, dans son mouvement essentiel de retour, se souvient, se sait hantée.

L’individu, éveillé ou endormi, se manifeste comme son impropre fantôme. Impropre surtout pas au sens d’une sorte de saleté, de complaisante exposition de sa part obscure --- souillure et perversion qui peut encore le croire ? en faire un totémique tabou ? ---, mais par son épreuve perpétuelle de l’incroyance.

Pas totalement par une domination de l’esprit rationnel : nous ne saurions en juger pour notre époque, nous ne pouvons non plus nous réclamer d’un retour à la superstition. Plutôt par un recours au seul domaine d’une familière extraterritorialité : la langue. Parlons alors du déjà-dit exprimé dans l’horreur, sémantique et très française, de la répétition.

L’individu serait alors une périphrase, les contorsions de vocabulaire qu’il invente pour éviter le mantra de répétitions qui le constitue.

On en a donné des noms à cet avers du réel. On aime celui que Leiris, grand rêveur, en écho à d’autres, Lacan notamment, emprunte pour définir ce tangage de la langue qui le parle, auquel il ne cesse de vouloir trouver une règle : constellation. Un ciel de mots, une stellaire théorie lumineuse qui éclaire notre nuit, une rêveuse perspective dont les limites ne sont que nos incapacités à en altérer les perspectives, en voir les voûtes plurielles, en deviner des mots-météorites d’un univers autre.

L’individu, comme son langage, espère encore l’hapax.

Une singularité qui se frotte à ce qu’autrui peut en entendre, un néologisme qui serait à la fois écho de la première écoute, le *reusement* leirisien, incarnation transitoire du son et du sens, un mot à la fois déjà entendu, compréhensible, et pointant vers son impossible nouveauté. L’individu est tout ceci. On conserve cette vieille dénomination pour en dire l’usure, par une sorte de doute sur les jeux de mots et de conceptualisation auxquels s’adonne la philosophie.

<div class="auteur">
[#Lacan #Raphmaj #Montaigne]
</div>

Ici s’essaie, infinie prétention, une pensée à l’épreuve des mots, au défaut de son expression<a data-link="02-b" id="l02-b" href="#l02-a"><sup>β</sup></a>. Autrui, *reusement*, pourra toujours inventer, écrire mieux, une autre archéologie à ce que l’on assène.

<div class="auteur">
[#Leiris]
</div>

Le mot n’existe que dans sa syntaxe, l’individu dans ses relations. Nos constellations sémantiques dessinent une rêveuse grammaire. Quelque part entre déférence et défiance, une prudente manière d’être au monde. Un dérangement par délicate distanciation, se penser compréhensible, en se tenant à distance, en retrait, de la rhétorique, de toute transparence, de la prétendue possibilité de simplifier.

<div class="auteur">
[#Levinas]
</div>

Avancer quelque part entre la phrase agrammaticale et la stricte correction grammaticale, entre commun et solitude, unique et répétition, à distance d’une définition existentielle du style.

L’individu par son style, par un travail conscient de torsion, de distance ainsi induite à ce qu’il croyait dire, de séparation à son projet, de bifurcation à sa pensée, de déploiement d’associations sonores et sémantiques imprévues, impensées, reviendrait à cerner notre sujet par défaut, anamorphose, miroir tendu à une absence. Contentons-nous de noter l’actualisation d’un miroir différent que serait en train de devenir, en littérature, le style. Sa modernité qui, comme l’individu, s’y accole.

L’individu, dans son acception la plus romanesque (ce terme englobe la part fictive, assumée ou non, de toute autobiographie, de tout essai sans doute aussi), prétend se singulariser non tant par son style lui-même, mais par sa protestation, le commentaire qui en pointe l’échec, l’insuffisance. Bien sûr, inanité de retracer l’histoire de ce que serait le bien écrire à travers tous ceux qui, dans une entreprise plus ou moins narcissique, s’en sont revendiqués. L’ensemble, sans doute, serait fort disparate, d’un goût horrible, égotiste.

Le style nous inscrit, peut-être, dans une époque. Dans sa tension, toujours reconduite, vers l’intemporel. Un style ne se décrète en Soi, pour Soi. Seul autrui, dans la déformation de sa réception, peut en parler, l’approcher alors par comparaison, réaction, l’inscrire dans, ou contre, un contexte. Le faire accéder au statut de littérarité : un énoncé qui outrepasse ce qu’il dit.

Chaque preuve de l’individu devrait être ainsi : un lien plus ou moins lâche avec un contexte toujours, possiblement (dans cette virtualité où nous existons), outrepassé.

Tout ce qui se dit serait alors rêveuse (flottant regret auquel on ne saurait se résoudre) épreuve de la fatalité, du fascisme, de langue. Tout ce qui est dit s’impose comme seule issue possible, en principe. La prose onirique dont nous rêvons serait celle où plusieurs individualités simultanées, sans contradiction, s’éveilleraient. On pourrait la raconter avec l’évidence mal rattrapée qu’ont les récits de rêves.

Au matin d’une nuit saccagée de rêves et de réveils, tu es transi par une sensation, par la part d’incertitude qui y commande, de *déjà-vu*. Avant de l’énoncer, anodine, tu entends la phrase que tu es censé prononcer. Au prix d’une virevolte, d’un reniement vers une sentence sans importance, tu parviens à dire autre chose. La phrase non dite, contrainte, persiste en tacite hantise. On voudrait en faire entendre la persistance rétinienne, l’acouphène quasi.

On dit le rêve répétition, il l’est aussi singulièrement que pourrait l’être l’écriture.

L’illusion d’un éternel présent, d’une latence en revenir. La description de l’individu au futur du regret de ce qu’il ne saura(it) être. On ne s’éloigne pas si aisément de l’invention de l’interprétation, la conception de Soi grandit, bonne raison pour s’en extraire, à l’ombre du jugement dernier. Peut-être que le rêve, son miroir l’écriture, n’exprime pas autre chose : nos gestes et autres manquements différemment peuvent être interprétés.

L’épreuve de l’individu est invention, utopie. Une façon de s’excepter, de pointer l’impossible de cette posture, de ce qui, en lui, tient lieu de répétition. Conscient de l’imposture, disons la facilité rhétorique et donc son efficacité, d’agir ainsi comme si on croyait vraiment, intégralement, que l’individu se saisit au moment où il devient autre chose.

Brûler ce que l’on aimait hier, le reniement comme dévotion avant que d’être enfermement dans une identité fixe. Alors l’individu se définirait dans ses mantras, les formules qu’il se répète comme, peut-être, pour ne plus y croire tout à fait. Sans doute faut-il avoir éprouvé un semblant de révélation pour en ressentir, dans une sorte de contre-révélation, les contradictions, la teneur toujours fatalement fantomale.

<div class="auteur">
[#Cendors]
</div>

Ainsi, essaie-t-on, comme pour mieux s’en <span id="defaire">défaire</span> (le comme ici, dans sa lourdeur stylistique, insiste sur la portée métaphorique --- <span class="citation">nous avançons là dans la sphère de la métaphore vécue<a data-link="44-a" id="l44-a" href="#l44-b"><sup>Υ</sup></a>, du langage, à côté de lui-même</span> --- qu’essaie d’avoir cette épreuve) une rhétorique. Douteuse comme toute pensée figée dans des formes, dans la répétition donc qui permettrait de les fixer.

<div class="auteur">
[#<span id="steiner">Steiner</span>]
</div>

À chaque hypothèse suivrait un exemple où se ferait l’épreuve de la <span id="dissipation">dissipation</span> d’une conception, crue singulière, reconnue sans doute abusivement pour telle par la lectrice & le lecteur, de l’individu.

Longtemps, je me suis confié, failliblement reflété, dans cette assertion : <span class="citation">l’homme est ce qui lui manque</span>. Vraiment ? Et ?

<div class="auteur">
[#Bataille]
</div>

Ne pas se confondre avec la morne mesure du scepticisme, exercice d’équilibriste pour tout, en dernière analyse, laisser en l’état. Lutte pied à pied, dérisoire, contre la fatalité.

L’individu, comme ne cesse de le refléter le rêve, est incomplet, manqué et manquant. Peut-être.

Interroger quand même la fatalité du discours prescripteur sur lequel s’appuie cette hypothèse, cette illusion fertile par la dialectique de compensation et décompensation qu’elle induit.

On joue une autre hypothèse, un bluff<a data-link="34-b" id="l34-b" href="#l34-a"><sup>Κ</sup></a>, une carte que l’on abat non pour gagner une quelconque partie, juste pour montrer que l’on peut jouer autrement, comprendre se faire exclure d’un jeu dont on prétend ne pas vouloir comprendre les règles, en inventer de nouvelles pour continuer à croire à une communauté de ceux qui trichent, tuent le temps, même avec une mauvaise donne. La métaphore, assez poussive, du jeu de cartes sert ici à montrer le nombre assez limité de possibilités : on croit avancer une nouveauté, on combine autrement un gambit.

À l’inverse, l’individu pourrait se concevoir inacceptable totalité. Ne savons-nous, aux tréfonds, être sans échappatoire ? Ou, pour le dire autrement (d’ailleurs est-ce là ce qui fait de notre tentative un essai : une volonté de le dire autrement, de croire échapper dans cette mouvante et perpétuelle traduction, à la totalité d’un système ?) l’individu à l’épreuve de sa satisfaction.

Si on accepte d’être seulement ce que l’on est, on est foutu : on finit par manigancer, manipuler, composer, pour s’imposer à autrui.

L’autre, trop occupé à l’image de lui-même qu’il prétend imposer, finit par entériner cette identité. Bien sûr, cette hypothèse ne saurait taire les soumissions socio-économiques par lesquelles l’identité se travestit. L’individu réduit à sa fonction, au pouvoir qu’elle concède.

Nous ne parlons ici que des individus dysfonctionnels, littéraires, à l’écart de ce qu’ils produisent, peut-être même dans une idéalité utopique : l’individu qui dans sa solitude, sa nuit, son rêve, éprouve une telle plénitude qu’elle confine à l’absence, qu’elle contraint, *a minima*, à n’avoir plus à se manifester, qu’elle se soustrait à toute affirmation.

<div class="auteur">
[#Cendors]
</div>

Une curieuse et peut-être paradoxale humilité. Si l’individu est une totalité (l’hypothèse est diaboliquement fragile) ce n’est qu’en se contemplant du dehors. Dans une identification onirique, tu l’auras compris. Un rêve dont on se réveille et dont on se demande comment il a pu trahir une telle évidence.

Alors, manifester une littérature heureuse. Un paradoxe de plus. Une nouvelle approche contradictoire de l’individu.

La littérature n’est pas là pour apporter le bonheur par un individualiste, capitaliste, développement personnel. L’au-delà (sans doute conviendrait-il de dire l’en deçà<a data-link="40-b" id="l40-b" href="#l40-a"><sup>Π</sup></a> pour éviter l’idée de débordement, de conquête par l’excès) du Moi, dont on voudrait inventer le partage, se rêve heureux.

On désire une littérature, qui, dans la conscience de ses limites, la perception de ses apories passées, croit en ses possibles, les actualise, en éprouve les impossibilités, un instant se réfugie dans les failles. La possibilité politique de la joie.

Que l’on ne s’y trompe pas, même si on reprend ainsi une démarche trop accrochée à l’individu (lui seul serait porteur d’une vérité dont il nous fait l’aumône, impuissant à en percevoir la portée que nous constituons), il ne s’agit pas non plus d’une littérature béatement joyeuse, en plein déni désagréable de se vouloir sans zone d’ombre.

L’individu comme question : pourquoi sa mise en forme contemporaine à ce point se réfugie dans l’humour, dans ce qu’il peut avoir d’imitation épuisée ?

On veut un humour du détachement. Celui qui fait dire à un condamné à mort, après une chute sans importance le jour de son exécution : encore une journée qui commence mal. Une tête qui tombe, mouvements réflexes, ultime persistance de l’individu quand il se détache de lui-même. Mort de rire. Même si ainsi se préserve, à notre corps défendant, une approche par défaut, par préservation : une parade contre la mort, une timide préservation.

<div class="auteur">
[#Freud]
</div>

Pourtant le rire serait épreuve du concret de notre démarche. Malgré tout : un grand éclat de rire face à toute pureté de l’expérience, mordante ironie pour son ordalie, sourire narquois pour ses désirs de rédemption, manières de se détacher de l’individu.

Ce dont, peut-être, il faudrait rire serait de l’exemplarité de toute posture éthique. Contrairement à vous, j’agis bien, admirez-moi, admirez-vous puisque je prêche, comment faire autrement, à des convaincus. Mais le faire, pour être certain d’être grotesque, avec un terrible esprit de sérieux. Se moquer de tout serait façon d’acquiescer à l’ordre du monde.

La littérature, sans fin, agonise de son consentement à ce qui est.

Aujourd’hui, on retrouve ce contentement sous les traits d’un absurde généralisé. Puisque l’on ne peut rien changer, autant s’en amuser. Escalade de l’absurde, scission facile d’un monde dont on présuppose le dysfonctionnement fondamental. Un des jugements possibles d’un texte serait d’évaluer à quelle irréalité il se complaît, comment ce qu’il assène n’apporte aucune altération.

On veut de la distance à ce qui apparaît, de la latence face à ce qui se manifeste.

Un fond d’incertitude qui, longtemps, pour le roman, s’est nommé polyphonie, dialogisme où s’inventaient les absences et les contradictions de l’individu.

Ce que l’on cache, ce que l’on montre ; miroir menteur et asymétrique. Onirique. Une décision qui ne saurait nous appartenir, ni d’ailleurs à l’autrice & à l’auteur, à la lectrice & au lecteur ou, pire, au personnage.

On entendrait alors, absurdité, le personnage comme une monade existante, réagissante à une réalité psychologique fixe, déterminante plus que déterminée. Posons une hypothèse, pas nouvelle. Sans doute est-ce ceci que nous désignons par l’onirique latence poursuivie dans ce rêve de l’individu : le personnage est taiseux devenir dans l’interstice des livres, dans celui d’une interprétation non pas fautive, mais toujours en souffrance, en actualisation.

Vautrin, sous ses masques, ses parures de discours tentateurs, revient et invente, dans son refus de l’ordre du monde, l’individu dans le débordement de l’individu. Un masque risible et admirable, un conseil qu’il serait aussi impossible de suivre que de s’en soustraire. Rastignac, Rubempré (on s’étonne à peine que l’époque les porte au pinacle, sans entendre la mordante ironie de leur portrait) inventent Vautrin par le regret. Le pacte avec le diable poursuit l’épreuve de l’individu peut-être seulement parce que le personnage n’est pas une éthique, ce serait risible, mais une casuistique. Et Moi, dans cette situation, quel mauvais choix aurais-je fait ?

<div class="auteur">
[#Balzac]
</div>

Comme une interprétation onirique, supposons ceci : s’il existe, l’individu littéraire (celui donc façonné par l’amalgame, mélange trompeur, des débordements des déterminismes des personnages) sera(it) polyphonique. Comme on traverse un rêve en étant celui qui le fait, agit, contemple, parfois même objet de ce rêve, on ne se départit pas de ce cliché dostoïevskien : nous sommes diaboliquement polyphoniques.

On a appris à se parler à soi-même comme si on était quelqu’un d’autre. Peut-être n’es-tu pas, suffisante lectrice & hypocrite lecteur, si tu ne partages pas cette expérience simple, espérons-le, jusqu’au concret. Sempiternelle tentation de l’altération.

Une vie romanesque, ce serait cela : tu vis une situation ordinaire, tu ne la perçois que dans la certitude qu’autrement elle continue à pouvoir se passer. On aurait pu... l’histoire est à réécrire<a data-link="45-a" id="l45-a" href="#l45-b"><sup>Φ</sup></a>. Toujours.

L’individu : somme de prophéties inadvenues.

</div>

<div class="text" data-id="7">

# Des incarnations...

On dit des hantises ; on désinvente leurs désincarnations.

Revenir sur ce qui n’a pas été assez dit, entendu avec une évidence suffisante. Une façon de continuer à penser que le sujet n’est pas délimité, pourrait se construire autrement, se dire différemment.

Une aporie où achopper : il est impossible de, réellement, se penser sans corps, il est contraignant de se croire limiter à ce qui ne peut être réduit à une enveloppe.

<div class="notemarge"><a class="notemarge__lien" href="#m29-l" id="m29-n"><sup>&ast;</sup></a>On ne cesse de tourner autour : l’individu tient à sa croyance à passer à autre chose, à se défaire de ce qu’il a été, voire à suturer ce qui l’a blessé. Nous tentons de parler, autant de discours d’emprunt, seulement de ce qui nous a passagèrement définis.</div>

Un objet au passé<a class="notemarge__lien" href="#m29-n" id="m29-l"><sup>&ast;</sup></a>. Pas tout à fait un concept. On ne saurait en oblitérer l’archéologie précise, métaphorique. Le lieu de dissensions idéologiques, de revendications identitaires, d’émancipations toujours à reprendre ; la cartographie mouvante de ce non-lieu que nous désinventons.

Sans doute n’avons-nous rien de singulier à en dire. Peut-être redire, autrement qui sait, le corps comme tentative de déprise de toutes les contradictions qui fonde notre rapport avec la réalité matérielle, qu’il serait le premier à manifester.

Le corps, alors, comme mise à la question de l’origine. D’où l’on vient, où l’on finit. Entre, des échappements. Notre rapport au monde serait-il diaboliquement déterminé par nos insuffisances physiologiques, nos maladroites manières de les compenser ?

On reconduirait ainsi la diabolique dichotomie entre corps et esprit en entendant, à notre corps défendant allait-on dire, que celle, & celui, qui lit et écrit le fait par défaillance, faute de pouvoir s’adonner à une activité physique ou, plus exactement, parce qu’il a la bourgeoise chance (la culture si vous voulez) de s’y soustraire. En dépit de tout, le corps nous situe, dénonce l’ubiquité, le désir d’ailleurs dans ce qu’il aurait de compensation (confort de pouvoir passer outre) de la situation de celui qui parle.

Tout écrit est hanté par l’ombre d’une mauvaise conscience, pas seulement de classe, habité par un désir de revanche. Il prétend s’extraire des origines. Une illusion que l’on prétend désincarnée, en faire une métaphore, et encore. <span class="citation">Le concept n’est autre que le résidu d’une métaphore.</span>

<div class="auteur">
[#Nietzsche]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m30-l" id="m30-n"><sup>&ast;</sup></a>Outrepasser : revenir sur un franchissement circulaire, toujours recommencé, tout de reprise et de revenance.</div>

Corps comme sésame, déclencheur, une frontière sans cesse estompée, cartographiée pour en cerner l’incertitude, l’outrepassement<a class="notemarge__lien" href="#m30-n" id="m30-l"><sup>&ast;</sup></a> contre-intuitif.

Le corps reliquat d’une servitude au réel ? Un usage, une usure, qui nous attache à nos perceptions, au témoignage des sens, à l’infinie superficialité des sensations.

Dans une hypothèse historique plus intuitive que vérifiable, on avancera(it) alors que les brouillées modernités (Renaissance, et le pessimisme schismatique de son second humanisme, longue réaction après la Révolution française, voire moment philosophique du basculement vers une jouissance revenue à l’individu) inventent non un autre individu, mais la singularité d’un rapport à son propre corps, à sa jouissance.

À sa capacité à en dresser le portrait, à le rendre vivant, incarné, à y trouver qui sait quelque plaisir de rattrapage ou de substitution. La question du corps reviendrait à celle de l’improbable jouissance de la représentation.

Serait-ce alors la faille primordiale dans laquelle s’est conçu (se conçoit-il encore vraiment ainsi, autrement que par passéiste dévotion ?) l’individu : il se perçoit dans un miroir dont il sait l’imperfection.

L’individu, éperdu et romanesque, s’incarne dans ses mots, dans sa mise en récit et, surtout, dans les points aveugles, les débordements perceptifs, les lucides aveuglements nés de ce permanent désir de rattrapage dont le corps serait vecteur premier.

Rien n’est évident.

Le corps, mot-sésame, est ressenti échappatoire aux généralités. Épreuve d’un particularisme irrattrapable. Le corps est d’abord illusoire expérience personnelle, puis, souvent par épuisement, est renvoyé à la certitude d’avoir déjà été ressenti et donc de devoir, impérative illusion, se dire autrement.

L’individu, si intangible, ne cesse de vouloir du tangible, souvent confondu avec du tactile. Il faudrait qu’un livre nous touche. L’expression est usée, révélatrice de notre désir de contact.

L’incarnation de cette expérience limite, peut-être son seul évanouissement, sa seule désincarnation, à la portée de l’individu, sera(it) la lecture. Ressentir dans ses tripes ce que dit l’auteur. S’y confondre, s’interroger surtout : peut-on réellement entendre ce que l’on n’a pas physiquement éprouvé ?

L’érotisme serait ce paroxysme (désir circulaire de dépassement) du prurit de contact qui anime lectrice & lecteur, autrice & auteur.

L’érotisme<a data-link="46-a" id="l46-a" href="#l46-b"><sup>Χ</sup></a> serait l’évanouissement de la sexualité, l’instant où la dire reviendrait non tant à tendre le miroir de son insuffisance que porter la conscience à son incandescence, que sa mise en mots en altère le ressenti, en exprime la perte, en ouvre à l’effarante absence de sujet. Comme un spectre, éloignement et retour, l’individu apparaît à l’instant, dans le récit qui le constitue et le détourne, de sa petite mort.

On en revient au corps, à cette conception dont on ne peut se départir : elle cantonne la jouissance à la linéarité, à un anéantissement terminal.

Animal triste post-coïtum. Serait-ce cela l’individu, l’épreuve de la dissipation que donnerait une trop grande jouissance ? Un instant n’être plus personne, dans cet effacement enfin percevoir. L’effarement de la satisfaction, la nécessité de lui donner d’autres noms, d’autres désirs. Parfaite car boiteuse métaphore de l’individu.

Mais, pour être utopique, l’érotisme doit aussi faire l’épreuve de l’émancipation, n’être pas un discours normatif comme un autre. Il se révèle plein de domination, notamment masculine, de stéréotypes si révélateurs des interdits toujours à transgresser. Dans ce seul domaine, parfois, le récit semble avoir atteint sa dernière extrémité, paraît presque condamné à se répéter.

Quel érotisme pour le maintenant, comment ne pas y lire des transgressions qui sont devenues conventions ? On n’en sait rien, on continue à espérer la fulguration.

Le sujet n’est pas là. Jamais. Détour.

Dans la souffrance ? Ce serait encore plus obscène. Intéressant pourtant dans l’interrogation ainsi ouverte sur le statut de la réalité. Notre corporel rapport au réel serait-il originellement masochiste ? Qui n’a jamais pensé, plus ça fait mal, plus c’est vrai, qui n’a jamais envié les vies contondantes, signifiantes, quand du dehors elles sont racontées ?

L’écriture dénude, se satisfait mal de la nudité.

La littérature s’empare alors de la souffrance pour en dire autre chose : lui donner un sens qui lui échappe. Un rapprochement qui fait frémir. Le corps mis en écriture est élision. À l’os, on ne saurait souffrir la chronique, l’absence de rémission, de toute maladie du même nom. Intéressante, cependant, tant elle interroge la sacro-sainte identification. Veut-on, peut-on, se réjouir de ne pas vivre cela ? Le contre-modèle sert surtout de révélateur, peut-être faut-il une certaine dose de Soi dans ce que l’on parodie, sans doute tout emprunt, en retour, nous dérobe une part de nous-mêmes.

De ce corps en souffrance, comme en attente, par correspondance semblent nous suggérer les mots, on attendrait une révélation. Revenance, sous une autre forme bien sûr, de ce lien entre le singulier et le collectif. Difficile de ne pas voir dans ce miroir une féconde erreur. Le corps malade, symptôme primordial de l’illusion biographique. Un événement, une défaillance, doit excuser, voire justifier la maladie.

Pitié et compassion, vieilles scies qui incarnent malgré tout notre rapport à l’individu. Une aporie révélatrice de l’individu : il est contenu à se manifester comme une sensible abstraction, un piège de cette confuse sentimentalité à laquelle nous ne sommes pas sûrs de vouloir, ou pouvoir, échapper.

Si nous parlons de l’individu littéraire, c’est dans cette réserve sensible. Physiquement, elle se traduit, par le retrait, le silence, le repli par incertitude. Si tout écrit dessine un autoportrait, ce serait dans cette situation. Soudain, je ne suis plus avec vous, je vous regarde pour mieux n’y comprendre rien.

Se réfléchir personnage romanesque : une individualité dont l’expression serait réaction au contexte culturel, dont l’incarnation serait contestée par la mise en intrigues et en dialogues, l’infinie distance au personnage par lequel l’auteur manifeste son absence.

Comme dieu, l’auteur est mort, la créature n’apparaît que pour, sans fin ni trêve, mettre à mort son créateur. De profane, le corps serait-il devenu sacré, objet d’une dévotion funèbre dans une prière qui cherche l’absence derrière l’omniprésence d’apparence ? Ne pourrait-on y écouter un conditionnement duquel, vainement peut-être, prétendre se détacher ?

Offrir une autre perspective sur la réalité. La perception est souvent limitée à son intensité, comme s’il existait, douleur ou plaisir, une pureté de la sensation. C’est un fantasme d’intellectuel que de valoriser le corps, de le doter d’une spontanéité, d’une brutalité qui se confondrait avec l’immédiateté.

<div class="auteur">
[#Crevel]
</div>

Revient la question de la voix, son appropriation par ce qui alors serait reconnu comme un auteur : pour se rassurer la lectrice, & le lecteur, se miroite dans un style uniforme, se reconnaît dans « la littérature », l’apaisement d’une linéarité continue.

Le corps considéré dans ses climax et non dans ses extases tant, le préfixe l’indique, elles sont expulsions. Le corps comme réceptacle pensant devient organe de séparation, épreuve première d’une solitude en permanence reconduite par une perception étroite, immédiate, de l’individu.

Nous serions, dans une métaphore usée, enveloppe charnelle en attente de pénétration. Quand la sexualité rencontre la psychologie dans un désir de domination, elles inventent l’intériorité. Notre perception de l’espace serait-elle limitée par cette enveloppe, à cette encombrante certitude d’avoir un corps à Soi, de parfois pouvoir, au prix de quel sacrifice, effleurer celui d’autrui comme on reconduit une séparation.

Une lente construction sociale, intéressante quand elle <span id="inflechit">infléchit</span> la voix.

Entre mimétisme et distinction, à l’adolescence, on se forge une intonation. À l’oral, on ne voudrait surtout ne plus être pris pour un autre. On s’égare dans ce moment d’indistinction sexuelle que l’on nomme la mue. Donnons un exemple stupide : comme pour s’en affranchir.

Une expérience de dépersonnalisation datée, strictement personnelle, me parlant presque trop pour ne pas illustrer l’illusion qu’elle parle à autrui.

La perte de l’enfance, souvenir du temps où peu avaient leur propre moyen de communication. À fil, le téléphone sonne, on s’empresse d’y répondre, on entretient la croyance (notre rapport à la communication contient-il vraiment autre chose ?) que quelqu’un veuille nous parler, à nous, confier ce que nous serions le seul à pouvoir entendre. Un instant la magie fonctionne. Avant de comprendre que l’interlocuteur voulait parler à notre père, mère.

Pris pour quelqu’un d’autre, peut-être pourrait-on écouter, se taire au moins.

L’épreuve de l’individu, l’instant où il s’écartèle sur sa reproductibilité. La première séparation, peut-être, nous n’avons pas assez de guillemets pour signifier la défiance avec laquelle nous employons l’expression, l’unique péché originel. Nous ne voulons pas être confondus avec ceux qui nous ont faits. Pari que l’on dit par avance perdu. La ressemblance au père, à la mère, serait-ce façon d’inverser la création, de l’inventer, de se croire unique ?

S’il ne se prend pas pour dieu, l’individu n’a aucun intérêt, comme dieu il n’apparaît que dans des instants de douteuse déréliction. Nous sommes dieu seulement par incapacité d’intervenir, quand croire en nous-mêmes est sans nécessité : une manière de se payer de mots, un renversement de formules qui vaut vérité par contre-pieds. L’humain a créé dieu à l’image de son désir de transcendance.

Alors, portrait de l’individu par l’histoire de ses figurations picturales, le signe vers ses absences, le contournement de l’interdit de ce qui ne se laisse ni réduire ni interpréter. Progresse-t-on vraiment si on donne à voir un vide par un autre ? Peut-on vraiment faire autrement, au fond, que de la ressemblance par latence et de la dissemblance par manifestation.

Cerner encore la circularité : chaque vision rétrospective sur l’individu se rêve prophétie, mimant ainsi le rapport que nous entretenons à nous-mêmes.

Se souvenir, s’altérer. Il paraît que ça ne marche pas, pour des raisons corporelles.

Se souvenir de Soi, souvent, reviendrait à imposer sa propre version de son histoire, à comprendre se ressembler physiquement. Une conformité à une propre projection : nous entretenons un rapport aussi onirique avec notre corps qu’avec celui que l’on prête à un personnage de roman.

Ils ressemblent à quoi Vautrin, Marcel, Stavroguine : un corps qui s’absente, au mieux parade dans des déguisements. Les différentes projections que chacun leur impose, les infimes variations entre les relectures, cette forme particulière de dissipation où enfin ils ne sont que mots, flux de conscience, et rythme[^1-lepreuve-de-lindividu_15], martèlement des phrases en quête d’elles-mêmes, de la petite musique qui bien mieux que toute description les définirait. Une fugue selon la figure attendue, de la <span class="citation">découverte et de la maîtrise des purs motifs rythmiques de l’être.</span>

<div class="auteur">
[#Balzac #Proust #Dostoievski #Blanchot]
</div>

La vue veut son aveuglement, l’audition ses dissonances, l’individu sa dissipation pour échapper non tant à la description qu’à l’esthétique. La beauté, sa reconnaissance par ses contemporains, l’édiction de ses codes et dérivations, s’entend mal avec l’individu.

Ça pue la merde, tout ce que ça recrache et rejette, une personnalité percluse tout à l’intérieur de son corps.

<div class="auteur">
[#Artaud]
</div>

Ou alors rendre cette écoute autistique, sans filtre ni hiérarchie, perception folle. Le corps, pourtant, obstinément, résonne, <span id="boitement">boitement</span> métaphorique.

Tant qu’il n’est ni douloureux ni pathologique, hormis en ses rares instants de jouissance, sans doute ne croyons-nous pas à notre propre corps. Il faut entretenir la même défiance pour les métaphores qui mal le cernent. Ne jamais en méconnaître l’indécence, pure projection en jeux de l’esprit. Nous ne savons rien de cet enfermement dans un trop plein de perceptions.

Nous ne pouvons témoigner que de la séparation sensitive, de la scission des et du sens.

Conforme toujours donc à une expérience que l’on peut dire sienne précisément parce qu’elle est ressentie sans appartenance. Le corps dès lors serait dissipation, ses sensations déperdition. Nous ne prétendons, même pour en montrer le vide, en faire le tour.

Désincarnations.

Sa plus grande évidence est immatérielle, factice ressemblance, spontanéité par déguisement : la voix qui nous caractérise, signe premier de la disparition, de cette absence au monde qui nous manifeste. Pourquoi y voir de la tristesse, une perception éplorée ? Pourquoi faire revenir les voix chères qui se sont tues, peut-on vraiment s’en dispenser ?

<div class="auteur">
[#Verlaine]
</div>

Une perte impossible qui, peut-être, n’a pas eu lieu, ou pas en notre nom impropre, des fantômes sans fulgurance. On se confie à la latence de la revenance.

Un instant dont on ne saurait dire s’il a déjà eu lieu ou si l’on en ressent, sans vraiment y croire, la prophétie. Pour en répéter une incarnation concrète : la sensation de déjà-vu. Une absence prisonnière, aussi. On voudrait que ce soit la seule invérifiable revenance sur laquelle faire reposer une monstration qui, malgré tout, ne saurait renoncer à l’empirisme.

Perdurer dans ce flottement, cette physique inconnaissance corporelle. La même, si j’en juge d’après mon absence à moi-même, que celle de la lecture. Une soustraction à l’image, simple suite de mots, amalgame (le terme laisse entendre toute l’incertitude du mélange) d’images et de sons, dont reste surtout la matière verbale, signe creux dont nous avons une perception particulière. « Mon » épreuve, sans singularité, dirait la précipitation<a data-link="17-b" id="l17-b" href="#l17-a"><sup>ρ</sup></a>.

La petite musique, la construction et autre frime, je l’écoute en lisant un mot sur deux. Je retiens des passages, à les recopier, ils disent autre chose.

La lecture comme façon de se duper soi-même.

Un reflet pour les fantômes qui, on le sait, n’aiment pas les miroirs. Une sorte de précipitation, une pluie qui dilue tout, un goût à la fois pour l’économie du polar et les livres où se fait jour la résistance du sens.

Flottement corporel par une audition qui serait alors scansion, appréhension d’un rythme qui nous soit propre : comprendre avec quelle irrégularité tes fantômes se manifestent, selon quel tempo tu te reconnais, par cordialité, avec les battements, cardiaques sans doute, d’un personnage ?

À quelle fréquence le portrait d’un personnage, sa mise en mots qui ferait entendre des gestes, coïncide avec la manière dont tu voudrais entendre ta présence, pleine d’absences, au monde ?

Une expérience alors de la lecture comme inappartenance, incroyance. La seule concordance, par sporadique revenance, que nous pouvons inventer entre l’auteur & l’autrice et le personnage, tiendrait à un doute commun.

On se sent, en absence, comme le personnage, inconsistant, appartenant à une conception dépassée, d’un auteur qui ne saurait, lui aussi si démodé, décalé, ni raconter sa vie ni en inventer une autre.

On sera(it) le personnage d’un essai qui tentera(it) d’invalider tout personnage, essai, auteur. On sera(it) alors, dans les interstices, ce qui survit à ces négations, ce qui subsiste malgré tout, celui qui jamais tout à fait ne parvient à croire aux systèmes de pensées qu’il met en place.

Le moment où s’impose la lecture pour chercher autre chose que l’on croit y trouver, pour que de nouvelles contradictions donnent à entendre ce que l’on ne savait pas entièrement aspirer à être.

Maintenir ouvert, par diplomatie (écoute des territoires interstitiels), le dialogue avec ce que l’on ne parvient jamais à croire. Il faut alors revenir sur la nécessité de ne pas trop se limiter à un champ littéraire, à l’entendu de références aisément partagées.

On veut une littérature sans sérieux, sauf pour son grotesque fondamental. Celui de n’importe quel attachement à son propre corps.

Ne parler que des instants où l’on ne sent, physiquement, aucune coïncidence avec celui que l’on a été. Non que l’on ne se souvienne pas de nos différents Moi, mais ils ne nous apparaissent que dans les cernes, précisions et séparations, du rêve.

Aucune solution de continuité hormis celle inventée au réveil, sceptique interprétation. On éprouve autant de nostalgie pour les rêves que pour des personnages de roman, pour l’identification évanouie à eux dès le roman refermé, pour les étranges récurrences, emplies de superpositions avec d’autres livres.

Peut-être n’est-ce que de cette identité diluée que nous nous cachions, derrière l’individu : de qui se souvient-on si, sans le taire, on ne cherche ni à dire ni à répéter une identité rituelle, corporelle ? Quelle est cette voix qui en nous parle quand elle est réceptacle successif d’absences, de cette distanciation de Soi à Soi qu’est toute écriture ?

</div>

<div class="text" data-id="8">

# Silence : images

Reprendre autrement, donner une autre figuration de l’individu ; continuer tacitement à croire à son outrepassement.

Penser : prétendre se départir d’une logique. Sans croire au silencieux mystère de la profondeur, nous voudrions contrer l’évidence. Penser tout contre, miroitement de la réflexion. Dire autrement de vieilles dichotomies comme pour s’en excepter. Ne croire ni en la vérité profonde de l’être ni au superficiel du paraître.

Éprouver l’individu à l’écart de son image.

On travaille une intuition, on la saborde, on la rend perméable : à rebours du personnage qui se mire au flux de conscience, au monologue intérieur, à la constellation de mots, aux rythmes des associations sonores, comme du dehors, en ce moment historique dont il faudrait réfléchir pluralité et récurrence, l’individu s’exile dans l’image.

Nous aurions aimé ne point le déplorer<a data-link="28-b" id="l28-b" href="#l28-a"><sup>Δ</sup></a>. Ce sentiment spontané creuse la probable fausseté de cette intuition, narcissisme et contemporaine superficialité imagée de l’individu, dont le sens, signe vers une identité manquante, reste à déployer.

La hantise de l’image s’inventera(it) d’autres <span id="origines">origines</span>. On sait ce que la popularité du miroir, son coût moindre, doit à l’intérêt pour les théories freudiennes<a data-link="47-a" id="l47-a" href="#l47-b"><sup>Ψ</sup></a>, leur réactualisation de ce misérable tas de secrets.

Avancerait-on si on traçait un parallèle avec la multiplication des dispositifs de prise de vue, leur mise en réseau qui en intime le partage, leur immédiat affichage ? Ne laissons entendre aucun mépris pour ces instantanés amateurs, pour leur immédiateté éperdue qui en dévaloriserait le statut d’expressions artistiques, leur capacité à remettre en cause la validation par les pairs, la rareté qui en ferait des œuvres d’art.

Sans originalité, la forme numérique interroge le dur désir de durer qui distinguerait l’œuvre d’art. L’écriture à l’essai ici, tu l’auras compris, se réclame, elle aussi, de cette dissipation<a data-link="38-b" id="l38-b" href="#l38-a"><sup>Ξ</sup></a> à laquelle cette multiplication de l’image, elle aussi, répond.

Nous serions un flux, enfin informe, de données sans hiérarchie dont l’apparition manifeste l’effacement. Et pourquoi pas ?

Quel visage donner à cet individu toujours mis à la question ? En essayer <span id="plusieurs">plusieurs</span>, laisser à la lectrice & au lecteur la possibilité de surimprimer les siens.

On commencerait par la principale réticence (le mot est employé au sens que tous les termes de l’alternative boitent, reviennent comme un scrupule dans la démarche), l’image de l’auteur, son identité fixée en son image si soigneusement édifiée.

L’auteur se constitue en personnage et croit, ainsi, habiter en poète. Au risque de la caricature, disons Beckett pour, immédiatement, lui opposer l’image, à peine moins viable, de Blanchot en sa dissipation.

On ne saurait choisir entre la profusion et la rareté ; le retrait ostentatoire et les masques de la surexposition.

Ne céder à aucune fatalité, c’est ce qui serait ici à l’essai. Ne pas dire --- inutile, entendue déploration --- ce qui a été, mais ce qui, par revenance, par hypothèse et autres joyeuses projections, sera(it).

Image, interprétations : fluctuations.

Si nous voyons dans une prétendue omniprésence des photos une fixité, transparaît peut-être surtout ainsi les limites d’un point de vue uniquement, petitement, nôtre.

Le concret accepte nos insuffisances, les pose pourtant pour les contourner. L’image, comme l’individu, nous laisse songeurs, silencieux. Peut-être n’avons-nous fait qu’exprimer, sans véritablement l’expulser, cette tacite réserve.

S’exposer alors, se contraindre à l’outrepassement, mieux en saisir ce qui boite, revient en une défaillance. Offrir un point de vue excluant toute exemplarité. Manière d’interroger l’envers de l’individu excepté, malgré tout, ici.

Pointer des apories pour mieux ne point s’y résoudre. Des rapprochements arbitraires : l’écrit serait, dans un cliché, retrait spéculatif de l’oral, l’individu pourrait s’y différencier, manifester ses latences. L’image ouvre sans doute de similaires arrière-mondes, chambres d’écho où apparaître. L’image comme vertige, sidération à bon compte. Inventons-en les interstices.

<span class="citation">Dans une œuvre, la contestation de l’œuvre en est peut-être la part essentielle, mais elle doit toujours s’accomplir dans le sens de l’approfondissement de l’image qui en est le centre et qui commence seulement à apparaître, quand vient la fin, où elle disparaît.</span>

<div class="auteur">
[#Blanchot]
</div>

Les anamorphoses de l’autoportrait, autant de signes vers l’absence temporaire (rêves et sommeil), définitive.

L’image, comme l’individu, est fabrication : projection davantage que rétrospection, des épaisseurs d’art que nous voudrions jouissance, simultanéité plutôt que spontanéité, en transmuant ce point aveugle, ce vide, en plénitude.

Une radieuse ignorance, nous n’y connaissons rien.

Les images, loin de toute histoire de l’Art, reviennent, s’invalident presque. Ce que l’on en commente n’est pas, au risque de se répéter, ce qu’elles auraient dû être, mais ce qu’elles pourront, par un jeu de surimpression, de pénétration des âmes, de numériques appropriations qui sait, devenir.

<div class="auteur">
[#Marías #Steiner]
</div>

L’image, alors, comme contamination d’un imaginaire collectif, figuration un instant d’une conception déjà quasi commune<a data-link="27-b" id="l27-b" href="#l27-a"><sup>Γ</sup></a>. Comme dans le rêve, derrière chaque image se cache une bibliothèque, des couvertures de livres.

Caspar David Friedrich, l’incarnation du lamento romantique.

Crépusculaire invention du Moi dans le déchirement, <span id="esseule">esseulé</span>, du paysage. Sensitive souffrance. Une sorte de sidération dans ce seuil du vertige obstinément reproduit. Une sorte d’au-delà dont, pour mieux le contempler, les personnages peints par Friedrich occultent la perspective. Depuis leur point de vue, inexistant, on verrait tout ce que le monde aurait à nous révéler. Prudent, en le déléguant romantiquement à un porte-voix, nous en conservons seulement le seuil.

Comme on rêve de réfléchir l’individu hors de sa rédemption, songeons un instant à un individu réfracté hors de son imminence.

Dans un écart provocateur, empruntant hélas un peu trop à la pensée par antagonisme, posons cette hypothèse : l’image induit une attitude masturbatoire. Vaine tentative de s’exciter par ce qui nous entoure comme si cela allait prouver notre existence. On cherche des images, on les <span id="collectionne">collectionne</span> par collage dans notre mémoire, peut-être uniquement car on désire s’y voir plus grand, plus beau : transfiguré. Vision réductrice, figurative, de la peinture dont l’essence est l’<span id="invisible">invisible</span>.

<div class="auteur">
[#Leiris]
</div>

<span class="citation">Il est / des nébuleuses / qu’aucun œil / ne distingue.</span>

<div class="auteur">
[#<span id="sebald">Sebald</span>]
</div>

Par circonvolutions, vaines par définition, nous encerclons l’anamorphose.

Tentons de faire l’épreuve d’un individu réduit à sa volition d’invisibilité plutôt que d’invisible. Creusons, une fois encore, le cliché, la comparaison hasardeuse offerte comme une virtualité, une inopérante proposition, une traversée, un éternel dépassement : à l’image de la littérature qui commencerait quand elle effleurerait l’indicible, la peinture partirait de l’invisible. Ou, plus conformément à une histoire entendue, elle jouerait, contournerait ou oblitérerait, avec l’irreprésentable. Lutte avec la matière pour représenter --- contrer --- ce qui lui échapperait.

Ne pas se laisser prendre à une défense d’un tabou dont l’émancipation serait cause première de la naissance de l’individu. Penser <span class="citation">l’intolérable fardeau de la présence de dieu</span> dans l’interdit de sa figuration mènerait à réactiver la fiction d’une oppressive, moyenâgeuse et meurtrière, conscience collective aveugle d’être sans individualité.

<div class="auteur">
[#Steiner]
</div>

Un essai à contre-cliché pour pointer des <span class="citation">évidences dérobées,</span> une extase latente, <span class="citation">diagonale</span>, de la figuration. Une incarnation, tout de virtualité, de ce que pourra(it) devenir l’individu.

<div class="auteur">
[#Caillois]
</div>

Territoire alors arpenté dans l’erreur. Pallier ainsi cette excitation, sa limitation individuelle, qui consisterait à affirmer effleurer la vérité de l’être, que tout ce que l’on dise en soit ainsi vérifié, voire justifié. Chaque mot est aussi <span id="faux">faux</span> que l’individu, que le vain désir de s’y soustraire.

<div class="notemarge"><a class="notemarge__lien" href="#m31-l" id="m31-n"><sup>&ast;</sup></a>À l’évidence, le sacré dérange. Nous l’entendons ici comme révélation inquiète, toujours incertaine, d’une présence de l’absence, toutes les virtualités données à ses visages tout de mutation.</div>

À tenter de représenter la présence divine en son absence, l’homme s’est laissé miroiter, a commencé à se représenter. Retour de la vieille dichotomie, miroir trompeur, entre profane et sacré<a class="notemarge__lien" href="#m31-n" id="m31-l"><sup>&ast;</sup></a>.

L’individu sera(it) hanté, sans jamais parvenir à y croire tout à fait, par sa part manquante de sacré. Maladroitement, nous tournons, d’un livre à l’autre, autour de cette évidence : nous ne pouvons, voulons, représenter que ce vide, sacré, joyeux, fuyant et dubitatif.

Un athéisme qui dans les détours de l’irreprésentable concevrait une part commune. On ne partage, après tout, que le tacite de nos peurs, les récits et autres mythes qui lui esquissent cette part commune.

Signe vers autre chose, vers, pourrions-nous presque penser, l’inconscient d’une époque. Ou, pour être moins inexact, vers sa latence qui, aussi dubitativement qu’un rêve, dans un jeu d’itérations et de dédoublements, trop tard, par recomposition sera interprétée.

L’image serait alors l’incarnation la plus limpide de notre incapacité à nous satisfaire de cette indistinction, à nous contenter des solutions transitoires de la cristallisation d’un visage temporaire de ce que nous sommes par ce que nous aspirons à être.

Épreuve de l’individu : nous ne savons de quoi nous parlons, nous inventons des détours, des fictions.

Retour au romanesque : la solution de l’image. Une autre hypothèse divagante. Amalgame et confusion pour ne point, trop, céder, à la dichotomie conceptuelle, nous ne distinguons pas image et tableau. Concrétion primale, informe, de l’intuition.

La solution de l’image serait donc une intuition critique, au risque du faux. On a voulu y voir l’image qui commande à la fiction, la vision qui intime la narration. Lieu de partage entre le lecteur et l’auteur & la lectrice et l’autrice plus intime, secret, que la reconnaissance d’un personnage, de son modèle. Nous te laissons lectrice & lecteur deviner, rêver, t’approprier, l’image aux sources de cet essai.

Repartons autrement, creusons un autre cliché.

Image, individu ; imitation.

L’individu se réduirait à la passagère question de l’appartenance, à l’aporie de l’intériorité, à la superficialité de l’extériorité que l’on projette dans une image, hélas (?), continue de nous-mêmes. On se sentirait exister quand, par excitation, on voudrait s’extraire de cette image convenue, effleurer contours et replis de ce qui serait autre, atteignable, miroir de ce qui nous outrepasse. Tactile et tacite<a data-link="46-b" id="l46-b" href="#l46-a"><sup>Χ</sup></a> aveuglement.

La question de l’individu qui écrit, qui éprouve toujours la solitaire scission de ce qu’il est, sera(it) alors de savoir comment susciter ce genre d’images, de sollicitations, de désirs pas seulement dans leurs retombées.

L’individu à l’essai se miroite dans l’incapacité à ne pas s’imiter lui-même, à pressentir l’inconscient de son époque pour pouvoir, entre prophétie et rétrospection, s’en penser à l’écart.

Alors, l’échappatoire de la joie. Départissons-nous de la défaite de la diction<a data-link="48-a" id="l48-a" href="#l48-b"><sup>Ω</sup></a>, de sa délectation morose. La vieille question revient : tout ce qui advient apparaît-il immanquablement en absence ? Serions-nous seulement perceptive absence ? Pourquoi le déplorer ? Acquiescer, en une nietzschéenne revenance de l’individu, à cette immédiate fatalité.

<div class="auteur">
[#Nietzsche]
</div>

Trouver, ou retrouver, une connivence avec un monde que l’on pourrait croire sien, susciter ainsi excitation et sidération, donner une image communicable, interprétable, de l’indifférent de nos vies.

Nous voudrions en dire, union de ce mouvement d’appropriation et de différenciation, l’invention d’emblèmes, un dédoublement qui enfin ne sera(it) pas simple illustration. On revient ainsi à nos vieux démons, aux calembours qui en déplient le sens : si nous prétendons esquiver ici une histoire non linéaire de son individu, c’est dans son commentaire. Tu entends, j’espère, lectrice & lecteur : comment-taire ? <span id="image">Image</span> diffractée, *brouillée*, de l’indicible qui en spirale s’entoure autour d’un vide primordial.

<div class="auteur">
[#Steiner]
</div>

<div class="notemarge"><a class="notemarge__lien" href="#m32-l" id="m32-n"><sup>&ast;</sup></a>Façon de dédoubler, d’ironiquement outrepasser l’anamorphose, serait de proposer une pensée par errance, fantastique par ce qu’elle a de fantomal. L’individu n’est qu’un revenant, le spectre de nos croyances aussi douteuses que dubitatives.</div>

Parler de ce qui touche notre absence, l’inscrit comme une solution de l’image. Loin du symbole, pour en préserver la fantastique<a class="notemarge__lien" href="#m32-n" id="m32-l"><sup>&ast;</sup></a> inquiétude, parlons par détours des emblèmes, des illustrations qui accompagnent un texte dont elles font dériver le sens, approfondissent l’interprétation. Un dédoublement à dédoubler, une matérialité approchée dans sa scission infinie, dans sa manière d’utiliser un langage qui cherche à en approcher le silence en commentant ses dangers et trompe-l’œil.

<span class="citation">Le sentiment que les livres me cherchent n’a pas cessé de m’accompagner, et tout ce qui est passé de mes pages fictives de *Todas las almas* à la vie a également fini par trouver matérialisation sous cette forme, en forme de livre, ou de document, ou de photo, ou de lettre, ou de titre.</span>

<div class="auteur">
[#Marías]
</div>

Illustration alors des similitudes inopérantes auxquelles nous confions notre essai qui voudrait ne rien devoir --- croire ainsi par instants et effractions, s’en émanciper --- à la logique entendue.

Arrêtons-nous sur le rapport à l’emblème de Javier Marías et W. G. Sebald sans insister sur leur commun plurilinguisme, images d’une pensée de l’exil dont leur œuvre, à des degrés divers, sont emblèmes. Les images n’y apparaissent jamais comme les <span id="preuves">preuves</span> de l’individu, viendraient en attester la vérité, la reconnaissance autobiographique. Les images ne sont, ces auteurs nous invitent-ils à le penser, pas autre chose que des citations détournées, des emprunts où mirer les fantômes que nous sommes dans l’envers de la fiction, dans le dos noir du temps, la confusion de toutes les âmes.

Insuffisant lecteur, indépassable lectrice, tu auras reconnu par cette appropriation des titres de Marías, la revenance d’une obsession pour l’interstice entre deux livres, entre deux vies vécues derrière la fiction. Notons qu’aucun de ses romans, du retour à la vie civile d’un espion à ceux sur les méandres et béances des intermittences amoureuses, ne parle d’autre chose.

L’individu apparaîtrait-il seulement dans son perpétuel prurit de corriger l’image que l’on se fait de lui ?

Une usuelle erreur qui consiste à croire détenir une vérité sur Soi. La fiction permet de dire les mensonges de nos transitoires identités, des illustrations données comme vérifications surtout.

Chez Marías, les images suscitent le secret, visages prophétiques dont on pourrait, entre menace et préservation, deviner, pour ne pas dire édicter, le comportement. Évidente mise en abyme, trop pour ne pas contenir son propre trompe-l’œil, de la posture du romancier.

Un peu comme une résolution abandonnée, une idée qui a informé nos façons de penser et à laquelle nous ne parvenons plus trop à croire, fantôme sans appartenance, la solution de l’image nous hante. Une des pires illusions critiques : croire comprendre d’où s’élance un roman, quel en serait, en une formule, en une image toute virtuelle, le déclencheur. Manifeste alors d’une critique existentielle, elle propose une virtualité d’attitude dont se départir.

<div class="notemarge"><a class="notemarge__lien" href="#m33-l" id="m33-n"><sup>&ast;</sup></a>Faut-il préciser que ce texte opère par omission ? Perpétuelle manière, illusoire, de passer à autre chose. Ne se résoudre ainsi ni à l’inanité ni à l’aporie.</div>

Opposons un détour, un méandre qui fait retour et croît. L’individu sera(it) ce qu’il retient. Cette retenue, cette réserve ironique, se manifeste moins mal dans l’omission. Peut-être, une fois encore, par ressemblance, biaisée, avec le personnage romanesque. Une apparition surtout par et dans ce que l’auteur se retient d’en dire. L’existence par ellipses<a class="notemarge__lien" href="#m33-n" id="m33-l"><sup>&ast;</sup></a>.

Taire les articulations inconscientes, inconsistantes. La réserve sur ce qui nous anime, la perpétuelle remise en cause d’un mobile à nos vies. Silences et soupirs.

On connaît, on s’y confond parfois, ce vieux rêve d’une histoire des non-dits. On céderait facilement à ce miroir illusoire : l’individu, une œuvre de l’ombre<a data-link="39-b" id="l39-b" href="#l39-a"><sup>Ο</sup></a>. Il s’écrit peut-être surtout dans les livres qui n’ont pas vu le jour, se devine au mieux dans les projets avortés, dans les commentaires élogieux, préparatoires, où se conçoit l’utopie de toute création artistique.

La solution de l’image serait détour vers ce silence essentiel, emblème de ce poids du non-dit où s’amalgamerait le personnel et le collectif, l’individu et sa retenue. Sans doute dans une part d’indécidable distanciation.

Un silence s’invente d’autres origines. Dans les romans bavards de Marías, dans sa dissection des silences qui constitue l’essence de nos dialogues, de nos rêves du contact à autrui, il place la possibilité, menteuse et manipulatrice, d’écrire l’histoire autrement<a data-link="45-b" id="l45-b" href="#l45-a"><sup>Φ</sup></a>. Les infinis échos du silence.

Le retrait de l’oralité, l’accession au neutre, à une parole sans pouvoir, reste une des définitions les plus opératoires de l’écriture. Le silence intervient peut-être alors comme possibilité d’une temporalité plurielle. L’individu ou l’expectative de l’instant suspendu ? Un moment, intangible, encore, poursuivre la duperie, opposer une ultime interprétation.

Arracher une vérité derrière les apparences : ce qu’elles cachent,
chez Marías, laisse apparaître l’emprunt.

L’individu commence, peut-être, quand il sait et sent n’avoir rien d’inédit à taire.

Sous des identités d’emprunt, de creuses façades, il entreprend alors d’espionner autrui. Nous définissons ici ainsi le personnage romanesque : scruter en miroir ce que l’on n’a pas trouvé chez Soi, refléter notre introuvable mystère interprétable dans l’illusion d’un sens cohérent de faire défaut.

Souligner le principe de plaisir qui préside à ces trompeuses, transitoires, identifications imagées d’un individu romanesque.

L’individu, un sourire entendu, moins narquois qu’attristé.

Ou à double détente comme chez Javier Marías. Un personnage, souvent archétypal, grand initiateur menteur, parle au protagoniste de parler trop, le renvoie aux images de propagande de la Seconde Guerre Mondiale qui intimaient au silence et font ainsi signe vers la duperie.

L’individu comme signe[^1-lepreuve-de-lindividu_16] vers ses propres illusions dont la première serait celle de son rapport à l’Histoire, à sa déformation de sa perception en tant qu’individu.

<span class="citation">Le respect scrupuleux de la perspective historique, le patient travail de ciselure et la mise en relation, dans le style de la nature morte, de choses qui semblent fort éloignées entre elles.</span>

<div class="auteur">
[#Sebald]
</div>

On retrouve une similaire dissemblance chez Sebald : l’image pivot, mots amphibies, signe vers un outre-monde. L’invention de l’outrepassement imagée de l’alter ego. On pourrait alors évoquer, révoquer dans le même mouvement, la figure attendue d’Austerlitz comme portrait de l’autre tu<a data-link="04-b" id="l04-b" href="#l04-a"><sup>δ</sup></a> en Soi. Un enfant, l’exil, la langue, le nom d’une gare : l’auteur dans des images sans appartenance, dans le décalage qu’elle illustre avec son écriture, sa mise à distance.

<div class="auteur">
[#Caillois #Sebald]
</div>

Une présence par omission.

Par détour, par association d’idées puisque nous sommes conformes aux complexes de notre époque<a data-link="47-b" id="l47-b" href="#l47-a"><sup>Ψ</sup></a>, abordons par un nouveau détour, une image dans l’image, ce qui serait une épreuve de l’individu : la mémoire. Si elle transite par des figurations picturales, elles sont détails discordants, insuffisantes interprétations pour retenir ce qui nous y échappe, nous captive.

On pourrait alors figurer l’épreuve de l’individu : la peinture n’est pas miroir, elle sera(it) reflet. Elle nous dévisage, suit des yeux notre inquiète inanité. Une confrontation frontale, tacitement collective peut-être parce qu’elle est empruntée, outrepasse ce qui manifeste une mémoire propre, strictement limitée à un moment donné.

Faudrait-il aussi se soustraire au ravissement esthétique ?

On toucherait alors à l’intériorité de la résonance, on reviendrait à une initiale incertitude pronominale quand le ravissement esthétique (forme immédiate, contradictoire, d’une expérience spirituelle, voire mystique) se réduirait à « ça me parle ». Qu’est-ce que le ça, quel rapport peut-il entretenir, délier, avec les marques de première personne ? Concrètement, *ça* veut dire quoi une appréhension esthétique dépersonnalisée ?

Saisi par un personnage, un fantôme ? Une obstruction de la vaste, et vaine, extériorité pour croire s’inventer, espérer s’y réfugier en une <span id="interiorite">intériorité</span>. Promeneur, très sporadique[^1-lepreuve-de-lindividu_17], de musées, jamais je ne me dépare de l’impression d’y jouer une comédie, de me regarder prendre de la distance avec ce rôle, spectateur. Faire, au passage, l’épreuve d’une classe sociale. Une extase pour société du spectacle, comme s’il fallait se regarder en avoir une, en manifester tous les signes, pour obtenir un crédible ravissement esthétique. Auparavant, on mimait la prière, on imitait la dévotion comme si *ça* allait permettre une effraction de la foi. Serait-on condamné à reproduire ces attitudes pour faire d’événements culturels un soulèvement<a data-link="36-b" id="l36-b" href="#l36-a"><sup>Μ</sup></a> collectif ?

Pour mieux, toujours, en bifurquer, précisons notre démarche : une épreuve de détachement, la <span id="desinvention">désinvention</span> de ce qui retiendrait des contradictions. Proposer une situation, une vision momentanée pour mieux en dériver, lui trouver une incarnation, fantomatique figuration, contemporaine. Toute rétrospection se fait au futur conditionnel<a data-link="43-b" id="l43-b" href="#l43-a"><sup>Τ</sup></a>, ouvre des questions comme devenir : quel visage l’art numérique donnera-t-il au spectateur ?

Alors, peut-être, l’individu apparaît non dans la question est-ce que ça me parle, mais, plus égoïstement, dans l’affirmation que ce n’est pas de cela dont je veux parler.

Donner une forme à nos ignorances, timidités et autres réserves.

En retrait de l’image, le silence sans doute reconduit les impasses de l’individu. On y ressasse ses certitudes, faute de les exprimer, on les répète. L’imitation de l’individu. Peut-être qu’une image claire comme une intuition, illusoirement nous en sortirait.

L’évitement, pourtant, ne saurait être une esthétique, moins encore une éthique.

La peinture par ses mots, l’image comme interprétation : prosopopée et non exégèse. On écrit seulement des spectres. Les images impavides des hantises qui reviennent : l’incapacité. Je voulais te parler d’Odilon Redon. Voir dans ses peintures l’image à nu. À la lettre une vision<a data-link="37-b" id="l37-b" href="#l37-a"><sup>Ν</sup></a>, à l’instar du titre d’un de ses dessins. Un œil unique, planétaire et louche, scrute celui qui s’aventure dans cette contemplation. Il semble porter une perruque, sans doute symboliser l’invisible, l’inquiétude scrutés dans chaque toile. Une sorte d’insatisfaisante, et apaisante, panique. Bref, une <span id="reserve">réserve</span> de sens. Ou l’expression parfaite, sans mots ni figement, de l’expérience mystique de la vision : est-on vu ou voit-on, sujet ou objet ? On arpente cette frontière, ce seuil de la dépersonnalisation où, radicalement, intolérablement, on se sent être.

Par facilité de langage, par celle qui a fait naître l’essai, la perception peut-être même de la modernité, nous effleurons ainsi le silence qui constitue l’essence de cette dépersonnalisation. L’image, alors, n’illustre pas l’individu, elle l’accompagne comme un miroir menteur, la vérification de ce qu’il n’est pas, le souvenir surtout des états où, sans se franchir, un instant il s’est outrepassé. <span class="citation">Et le reste n’est-il / par le souvenir détruit ?</span>

<div class="auteur">
[#Sebald]
</div>

</div>

<div class="text" data-id="9">

# Un mythe tacite, l’individu ?

Repartir. La modernité, l’individu qui s’y conçoit, serait façon de s’excepter du recueillement. Contre toute évidence construite, l’individu est l’incarnation faillible de la pensée magique. Il s’esquiverait dans le silence, incertain, qui suit l’imploration.

Une attente trahie.

On voudrait la penser dans l’écart à l’étymologie trompeuse du terme religion, notion dont l’outrepasement serait le fétiche de la modernité. Ce dont on se souvient, ce qui nous relie : un silence. La possibilité d’une hétérodoxie par et dans une expérience paroxystique, mystique.

Au centre de notre sujet, à la toute fin, nous n’en savons rien, ne reste qu’un silence collectif sur ce que nous pouvons approcher seulement dans ses ratages, par ses redites. Ni déploration ni dolorisme, pourtant. Seulement, la malédiction<a data-link="48-b" id="l48-b" href="#l48-a"><sup>Ω</sup></a> de la redite.

Se souvenir de ceci : il manquera toujours un investissement au sacrifice, une <span id="reticence">réticence</span> à une explication totale, un défaut d’implication de tout un chacun en commandera la redite. Ne ferait-on que répéter un silence, croire qu’il serait l’expression d’un moment historique quand, dans son indécision, il serait circulaire itération, préservation du soupçon d’un immuable *seulement* d’être ineffable ?

Épreuve, encore et à nouveau, de l’individu : son silence ne saurait être un mystère, un refuge. À la démission, préférons la malédiction.

Ce dont on se souvient, au nom d’une modernité à désinventer, ce serait un scepticisme sceptique. L’exaltation d’une désillusion, sa tacite préservation d’un enthousiasme secret, au sens de similaire seulement en apparence avec ce qu’autrui en tait.

Le mythe, ou la mise en scène d’un effondrement, qui, dans une pensée magique dont nous ne saurions tout à fait nous défaire, à laquelle nous croyons avec la même ferveur que ceux qui jouent uniquement pour l’échec, serait appelé à périodiquement se réactualiser, se revivre sans issue.

Dès lors, croire à la fin du monde se réfléchirait achèvement, jamais apogée, d’une aporie individualiste : l’ultime chose que nous aurions à vivre en commun serait la déperdition d’un sentiment d’appartenance collective. Curieuse actualisation surtout du ressenti, de sa mise en discours, qui serait insuffisant, insatisfaisant, à l’imparfait toujours tel un rattrapage sans compensation.

Peut-être pourrait-on prêter à cette épreuve de l’individu un autre mythe, comme par substitution. En silence<a data-link="30-b" id="l30-b" href="#l30-a"><sup>Ζ</sup></a>, dans cette haute expression du doute sur Soi, s’écouterait une communion de doutes, la force de ceux qui ne croient en rien, surtout pas en eux.

Rien de bien nouveau, sans doute.

<div class="notemarge"><a class="notemarge__lien" href="#m34-l" id="m34-n"><sup>&ast;</sup></a>Peut-être une question de langage. Nous le rêvons, par ses trouées de silence, ouvert, <em>open-ended</em>.</div>

Seulement le désir de te communiquer une incarnation de cette révolte en latence, de ce bouleversement en l’éternel *à venir*. Moins un exemple qu’une diversion<a class="notemarge__lien" href="#m34-n" id="m34-l"><sup>&ast;</sup></a>.

Outrepasser le retour des antagonismes jamais aussi manifestes que lorsque l’on prétend s’en extraire : pour échapper, crois-tu, à la dichotomie entre abstrait et vécu, celle qui rejoue celle du particulier et du général, tu parles alors d’expériences, tu imposes leur aspect commun, tu crois t’en sortir par une pirouette en prétendant que ce qu’elles ne parviennent pas à dire nous relie. Dans un lassant automatisme dialectique, nous pouvons y substituer un troisième terme : le concret<a data-link="29-b" id="l29-b" href="#l29-a"><sup>Ε</sup></a>.

Encore un mot trop grand pour toi. Tu veux des mots-sésames, des déclencheurs, des saisines, de transitoires cabanes : foin des concepts<a data-link="21-b" id="l21-b" href="#l21-a"><sup>φ</sup></a>. À l’écart aussi, te crois-tu autorisé à emprunter cette pensée mystique qui, dans le langage, excède la langue, en touche l’ultime extrémité, le mot terminal pour outrepasser le silence que l’on ne saurait garder.

L’épreuve de l’individu : une incroyance dans l’inédit.

Le <span id="concret">concret</span>, manière de vérification, de reconnaissance dans l’indécidable déjà-vu. Le partage, au bénéfice du doute, de l’incertain. Toujours, donc, dans un mouvement réflexif. L’instant d’après --- et peut-être l’expérience est-elle l’autre nom de la traversée d’un doute<a data-link="12-b" id="l12-b" href="#l12-a"><sup>μ</sup></a> --- douter de cet incertain, ne vouloir rien en magnifier, juste en espérer une manifestation en apparence suffisamment autre, profondément similaire, pour donner un visage concret à cette altération d’un scepticisme sans appartenance.

On rêve alors d’une politique purement prophétique. Loin du défaut d’assiette, de sa sagesse de la résignation à moyen terme, elle ne proposera(it) que des postures intenables<a data-link="19-b" id="l19-b" href="#l19-a"><sup>τ</sup></a>. Peu de propositions applicables autrement que dans la conscience transitoire de n’être <span id="rien">rien</span> ou personne. Pourtant, simultanément, un écart à une imposture à laquelle on ne saurait se résoudre. L’individu, un appel, une translation<a data-link="44-b" id="l44-b" href="#l44-a"><sup>Υ</sup></a> de la perpétuelle sensation d’imminence dans laquelle il se vit.

<div class="auteur">
[#Steiner]
</div>

Une revenance, aussi.

Avec un certain scepticisme (moins moqueur, cynique, que hanté par le désir trahi de croire, l’obsession dubitative de la trace des dieux enfuis), l’individu s’écrirait, se représenterait, dans sa tentation démoniaque. Un Mal immuable, intangible, dans lequel figer une identité irrationnelle, redondante, tragique, à l’écart du progrès.

On y revient, malgré Soi, enferré dans les échos de l’éventualité d’un maléfice primordial de l’humain<a data-link="16-b" id="l16-b" href="#l16-a"><sup>π</sup></a>. Prétendre ne rien en savoir<a data-link="14-b" id="l14-b" href="#l14-a"><sup>ξ</sup></a> serait s’enfermer dans l’imposture.

Croire échapper aux déterminismes sociaux serait tout aussi illusoire que de les renier afin de, surtout, n’y rien changer. S’en échappe une métaphysique, plus ou moins honteuse, de l’individu. Un mystère fondateur qui, dans toute parole sur Soi, dans toute tentative de compréhension, ressurgirait.

<span class="citation">Tout mal revient.</span>

<div class="auteur">
[#Marías]
</div>

Échappatoire rieuse, posons une hypothèse (l’issue du scepticisme : proposer de fausses routes) : plus qu’un maléfice, l’individu est une malédiction. Une mauvaise façon de le dire qui, néanmoins, préserve la valeur performative du langage<a data-link="23-b" id="l23-b" href="#l23-a"><sup>ψ</sup></a>. Le seul mythe collectif à l’horizon sera(it) un langage magique, maudit aussi au sens où tout ce qu’il énonce sera(it) réalité. Les sortilèges de la prière. À l’instar de l’individu, sa pire malédiction consiste à faire advenir ce qu’elle implore. Sa part la plus tenace, immuable si on veut, obsédante sans aucun doute, tiendrait à son appréhension.

Crainte et refoulement, l’individu est ce qu’il redoute.

Automatisme des déterminismes culturels, on écrit pour se réfugier dans une marge d’imperfection, dans l’imminence de l’interprétation, dans la participation du lecteur & de la lectrice.

Trop longtemps, la poésie a été perçue comme pâle palliatif de la prière. On la considère, dans nos sociétés heureusement athées, comme résidu formalisé, ritualisé, de ce flux de conscience dont la littérature a fait l’essence, mouvante, de l’individu.

On y adjoint, hélas, l’idée que cette tacite aspiration se heurterait à l’absence de ciel collectif. On ne partagerait plus que des aspirations individuelles, l’incessante déploration d’un mythe collectif dont le souvenir, pourtant, nous réunirait. Voire.

Ne rien céder à la résignation. Nous n’avons rien d’autre, peut-être, à proposer. Fût-ce en esquissant<a data-link="41-b" id="l41-b" href="#l41-a"><sup>Ρ</sup></a> d’autres points aveugles. Ultimes revenances à inventer. Faire l’économie du résidu religieux qui hante les cosmologies, leur eschatologie, ce désir de rédemption apocalyptique que, dans une certaine platitude, tout un chacun tait aux tréfonds de lui.

<span class="citation">Nous sommes de ceux qui ne peuvent rester tranquilles dans le vide.</span>

<div class="auteur">
[#Dostoïevski]
</div>

Hubris du démiurge, épreuve historique de l’individu. Une parole véritablement athée se confondrait, elle, avec l’absence définitive de sanction, de finalité en absence de l’espoir de la rédemption. Le seul immuable, le seul démon si on veut, de l’individu est cette permanente conscience du vide. Peut-être est-ce là le fin mot de l’absence de l’individu.

<div class="notemarge"><a class="notemarge__lien" href="#m35-l" id="m35-n"><sup>&ast;</sup></a>On s’essaie alors à un Hyper-texte ; on propose autant des raccourcis, des circularités que la <span id="possibilite">possibilité</span> éperdue d’être à deux endroits différents, dans deux énonciations pas nécessairement contradictoires.</div>

Comment, dès lors, donner une forme collective à ce miroir réciproque de l’abîme ? D’autres ont trouvé une langue pour cette écriture neutre du désastre. En prolonger les articulations<a class="notemarge__lien" href="#m35-n" id="m35-l"><sup>&ast;</sup></a>, en écouter les échos et autres revenances suffit-il à se fondre dans un sentiment de communauté ?

<div class="auteur">
[#Nieztche #Blanchot]
</div>

Jamais entièrement, jamais concrètement dans un ressenti limité à l’immédiateté du personnel ; on est submergé par une nouvelle impasse de l’individu. La société se formerait par la dissociation, la dislocation, des individus qui la forment, la distance qu’ils entretiennent avec ses discours et diktats. Notre commune capacité, en retrait, d’habiter poétiquement le monde.

Fugitifs et fragiles comme les mythes transitoires, l’individu désormais se construit de temporaires cabanes, brouille des fragments, où se reflète l’ubiquité, le *brouillage*, où il se sait être autant par présence que par absence. Un repos dans la révolte, autant de spectrales zones à défendre. L’individu comme non-lieu.

<div class="auteur">
[#Michaux]
</div>

N’allons pas suggérer<a data-link="31-b" id="l31-b" href="#l31-a"><sup>Η</sup></a> que l’individu se constituerait de matière noire. Plutôt, pour conjurer son entente dans une perpétuelle perte, qu’il est incréé, par entropie, il retourne, s’enroule en spirale, autour d’un noyau d’inconnaissance auquel la modernité, tour à tour, a donné différentes dénominations.

Serait-ce seulement les limites de la parole, les frontières de l’écriture<a data-link="18-b" id="l18-b" href="#l18-a"><sup>σ</sup></a> ?

</div>

[^1-lepreuve-de-lindividu_1]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf).

[^1-lepreuve-de-lindividu_2]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf) (« l’aporie du roman sartrien est biffée », p. 7).

[^1-lepreuve-de-lindividu_3]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf), (« l’auteur parvient alors à incarner ce que l’hypothèse mélancolique doit au rêve », p. 28).

[^1-lepreuve-de-lindividu_4]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf).

[^1-lepreuve-de-lindividu_5]: S’échapper vers [*Crevel, Cénotaphe*](https://www.antilivre.org/crevel-cenotaphe/crevel-cenotaphe.pdf) (« j’ai beaucoup appris de mes erreurs, je suis prêt à les répéter », p. 63).

[^1-lepreuve-de-lindividu_6]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf) (« le romancier sort ainsi de sa tour d’y voir, d’une élite désœuvrée qui se regarde exister », p. 36).

[^1-lepreuve-de-lindividu_7]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf).

[^1-lepreuve-de-lindividu_8]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf) (« redire la disparition pour se tenir au seuil de ce trou noir qui aimante les phrases d’où elles s’élancent et se dissolvent », p. 70).

[^1-lepreuve-de-lindividu_9]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf).

[^1-lepreuve-de-lindividu_10]: S’échapper vers [*À te souvenir de l’insomnie des mondes*](https://www.error.re/a-te-souvenir-de-linsomnie-des-mondes/).

[^1-lepreuve-de-lindividu_11]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf) (« dire le vide, pas le définir donc, se ferait au participe présent », p. 9).

[^1-lepreuve-de-lindividu_12]: S’échapper vers [*Crevel, Cénotaphe*](https://www.antilivre.org/crevel-cenotaphe/crevel-cenotaphe.pdf).

[^1-lepreuve-de-lindividu_13]: S’échapper vers [*Le Tiers Essai*](https://www.error.re/le-tiers-essai/).

[^1-lepreuve-de-lindividu_14]: S’échapper vers [*Crevel, Cénotaphe*](https://www.antilivre.org/crevel-cenotaphe/crevel-cenotaphe.pdf), (« on pourrait lire l’ébauche d’un geste terminal, la chance, qui sait, de passer à autre chose », p. 28).

[^1-lepreuve-de-lindividu_15]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf) (« Siri Hustvedt, dans ce marigot de l’intelligentsia, là où tout va de soi quand l’argent est une préoccupation dont on peut s’extraire, s’amuse de la reconnaissance autobiographique, du vécu que pourrait lui prêter le lecteur », p. 41).

[^1-lepreuve-de-lindividu_16]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf) (« j’avance masquer en montrant du doigt mon masque », p. 11).

[^1-lepreuve-de-lindividu_17]: S’échapper vers [*Un vide, en Soi*](https://www.antilivre.org/un-vide-en-soi/un-vide-en-soi.pdf) (« on le tente : l’art, ce qui paraît creux. Nous n’en voulons pas les codes ; plus même y pénétrer par effraction », p. 41).
