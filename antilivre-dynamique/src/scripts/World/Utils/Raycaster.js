import * as THREE from 'three';
import World from '../World.js';
import Text from '../../Layout/Text.js';
import * as TOOLS from '../../tools/tools.js';

export default class Raycaster {
  constructor() {
    this.world = new World();
    this.sizes = this.world.sizes;
    this.camera = this.world.camera;
    this.space = this.world.space;
    this.mouse = new THREE.Vector2();
    this.raycaster = new THREE.Raycaster();
    this.currentIntersect = null;
    this.intersects = 0;
    this.solidsList = null;
    this.text = new Text();

    this.clickable = true;

    this.space.addEventListener('solidsReady', () => {
      this.solidsList = this.space.solids.solidsList;
    });

    this.world.canvas.addEventListener('mousemove', (event) => {
      this.mouse.x = (event.clientX / this.sizes.width) * 2 - 1;
      this.mouse.y = -(event.clientY / this.sizes.height) * 2 + 1;
    });

    this.world.canvas.addEventListener('click', (event) => {
      event.preventDefault();
      if (TOOLS.isTouchDevice()) {
        this.mouse.x = (event.clientX / this.sizes.width) * 2 - 1;
        this.mouse.y = -(event.clientY / this.sizes.height) * 2 + 1;
        this.raycaster.setFromCamera(this.mouse, this.camera.instance);
        this.intersects = this.raycaster.intersectObjects(this.solidsList);
      }
      if (this.intersects.length > 0 && this.clickable && !this.text.mouseInsideText) {
        this.text.showText();
      }
    });
  }

  update() {
    this.raycaster.setFromCamera(this.mouse, this.camera.instance);
    if (this.solidsList) {
      this.intersects = this.raycaster.intersectObjects(this.solidsList);

      if (this.intersects.length && !this.text.mouseInsideText) {
        document.body.style.cursor = 'pointer';
        // if (!this.currentIntersect && !this.text.mouseInsideText) {
        //   document.body.style.cursor = 'pointer';
        // } else {
        //   document.body.style.cursor = '';
        // }

        // this.currentIntersect = this.intersects[0];
      } else {
        document.body.style.cursor = '';
        // if (this.currentIntersect) {
        //   document.body.style.cursor = '';
        // }
        //
        // this.currentIntersect = null;
      }
    }

    if (this.camera.movingCamera) {
      this.clickable = false;
    } else {
      this.clickable = true;
    }
  }
}
