import * as THREE from 'three';
import * as SkeletonUtils from 'three/examples/jsm/utils/SkeletonUtils.js';
import World from '../World.js';
import * as TOOLS from '../../tools/tools.js';

export default class Solids extends EventTarget {
  constructor() {
    super();

    this.world = new World();
    this.scene = this.world.scene;
    this.resources = this.world.resources;
    this.time = this.world.time;
    this.debug = this.world.debug;

    // Debug
    if (this.debug.active) {
      this.debugFolder = this.debug.ui.addFolder('solids');
    }

    // Resource
    this.nbSolids = 18;
    this.solidsList = [];
    this.shapes = [this.resources.items.rock.scene];

    for (let i = 0; i < this.nbSolids; i++) {
      const solid = SkeletonUtils.clone(this.shapes[TOOLS.randomNb(0, this.shapes.length - 1)]);
      // const solid = this.shapes[TOOLS.randomNb(0, this.shapes.length - 1)];
      const position = {
        x: TOOLS.randomNb(-200, 200),
        y: TOOLS.randomNb(-200, 200),
        z: TOOLS.randomNb(-200, 200)
      };
      const scale = {
        x: TOOLS.randomNb(40, 120),
        y: TOOLS.randomNb(40, 120),
        z: TOOLS.randomNb(40, 120)
      };
      this.setSolid(solid, position, scale);
      solid.speedRotation = TOOLS.randombNbFloat(-0.2, 0.2);
      solid.speedPosition = TOOLS.randombNbFloat(0.1, 0.5);
      this.solidsList.push(solid);
      solid.multiplyX = TOOLS.plusOrMinus();
      solid.multiplyY = TOOLS.plusOrMinus();
      solid.multiplyZ = TOOLS.plusOrMinus();
      this.distanceMovement = 500;
    }

    // Debug
    if (this.debug.active) {
      this.debugFolder
        .add(this.solidsList[0].children[0].material, 'opacity')
        .name('opacity')
        .min(0)
        .max(1)
        .step(0.001);
    }
  }

  setSolid(solid, position, scale) {
    solid.scale.set(scale.x, scale.y, scale.z);
    solid.position.set(position.x, position.y, position.z);
    // solid.position.set(position);
    this.scene.add(solid);

    solid.traverse((child) => {
      if (child instanceof THREE.Mesh && child.material instanceof THREE.MeshStandardMaterial) {
        child.material.transparent = true;
        child.material.opacity = 0.62;
        // child.material.wireframe = true;
        // child.castShadow = true;
        // child.receiveShadow = true;
      }
    });
  }

  update() {
    for (const solid of this.solidsList) {
      const time = this.time.elapsed / 1000;
      solid.rotation.x = time * solid.speedRotation;
      solid.rotation.y = time * solid.speedRotation;
      // const solidAngle = this.time.elapsed / 1000;
      // solid.position.x = Math.cos(solidAngle) * (7 + Math.sin(this.time.elapsed * 0.32));
      // solid.position.z = Math.sin(solidAngle) * (7 + Math.sin(this.time.elapsed * 0.5));
      // solid.position.y = Math.sin(this.time.elapsed * 4) + Math.sin(this.time.elapsed * 2.5);

      if (solid.position.x > this.distanceMovement) {
        solid.multiplyX = -1;
      } else if (solid.position.x < -this.distanceMovement) {
        solid.multiplyX = 1;
      }
      if (solid.position.y > this.distanceMovement) {
        solid.multiplyY = -1;
      } else if (solid.position.y < -this.distanceMovement) {
        solid.multiplyY = 1;
      }
      if (solid.position.z > this.distanceMovement) {
        solid.multiplyZ = -1;
      } else if (solid.position.z < -this.distanceMovement) {
        solid.multiplyZ = 1;
      }
      solid.position.x += (solid.multiplyX * solid.speedPosition);
      solid.position.y += (solid.multiplyY * solid.speedPosition);
      solid.position.z += (solid.multiplyZ * solid.speedPosition);
      // solid.position.x += (solid.multiplyX * (Math.cos(time) * solid.speedPosition) + (1 + Math.sin(time * 0.32)));
      // solid.position.y += ((Math.sin(time) * solid.speedPosition) + (1 + Math.cos(time * 0.5)));
      // solid.position.z += ((Math.sin(time) * solid.speedPosition) + (1 + Math.cos(time * 2.5)));
    }
  }
}
