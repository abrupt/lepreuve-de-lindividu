import * as TOOLS from '../tools/tools.js';

const bg = TOOLS.randomNb(1, 10);

export default [
  {
    name: 'environmentMapTexture',
    type: 'cubeTexture',
    path:
      [
        `three/textures/environmentMap/${bg}/px.jpg`,
        `three/textures/environmentMap/${bg}/nx.jpg`,
        `three/textures/environmentMap/${bg}/py.jpg`,
        `three/textures/environmentMap/${bg}/ny.jpg`,
        `three/textures/environmentMap/${bg}/pz.jpg`,
        `three/textures/environmentMap/${bg}/nz.jpg`
      ]
  },
  {
    name: 'rock',
    type: 'gltfModel',
    path: 'three/models/rock.gltf'
  }
];
