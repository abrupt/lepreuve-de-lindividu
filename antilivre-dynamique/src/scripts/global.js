import Header from './Layout/Header';
import Text from './Layout/Text';

const menuBtn = document.querySelector('.button--menu');
const menuEl = document.querySelector('.menu');

const header = new Header(menuBtn, menuEl);
const text = new Text();
