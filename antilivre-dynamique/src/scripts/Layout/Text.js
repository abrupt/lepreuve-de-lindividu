import * as THREE from 'three';
import SimpleBar from 'simplebar';
import ForceGraph3D from '3d-force-graph';
import * as TOOLS from '../tools/tools.js';

export default class Text {
  constructor() {
    this.container = document.querySelector('.container');
    this.texts = [...document.querySelectorAll('.text')];
    this.links = [...document.querySelectorAll('a[data-link$="-a"]')];
    this.textContainer = document.querySelector('.texts');
    this.textsOpen = false;
    this.graph = null;
    this.footnotes = document.querySelector('.footnotes');
    this.allNotes = document.querySelectorAll('.footnotes li');

    // SimpleBar for the menu
    this.simpleBarEl = new SimpleBar(this.textContainer);
    // Disable scroll simplebar for print
    window.addEventListener('beforeprint', () => {
      this.simpleBarEl.unMount();
    });
    window.addEventListener('afterprint', () => {
      this.simpleBarEl = new SimpleBar(this.textContainer);
    });

    // let camPos;

    this.data = { nodes: [], links: [] };

    this.textSource();

    // smoothscroll between anchors
    const anchors = document.querySelectorAll('a[href^="#"]');
    for (const anchor of anchors) {
      anchor.addEventListener('click', (e) => {
        const hash = anchor.getAttribute('href');
        const destination = document.querySelector(hash);
        destination.scrollIntoView({
          behavior: 'smooth',
          block: 'start'
        });
        history.pushState(null, null, hash);
        e.preventDefault();
      });
    }

    this.graph = ForceGraph3D({
      controlType: 'trackball',
    })(this.container)
      .graphData(this.data)
      // .jsonUrl('./js/praxis.json')
      .backgroundColor('#ffffff')
      .showNavInfo(false)
      .linkOpacity(0.62)
      .linkColor(() => '#000000')
      .linkCurvature('curvature')
      .linkCurveRotation('rotation')
      .linkResolution(20)
      .nodeResolution(20)
      .nodeOpacity(1)
      .nodeVal(20)
      .nodeThreeObject((node) => {
        const textureMap = new THREE.TextureLoader().load(`./images/mask${node.id}.png`);
        textureMap.colorSpace = THREE.SRGBColorSpace;

        const material = new THREE.SpriteMaterial({
          map: textureMap,
          transparent: true,
        });
        const sprite = new THREE.Sprite(material);
        sprite.scale.set(12, 18.86);
        return sprite;

        return new THREE.Mesh(
          new THREE.PlaneGeometry(10, 10),
          // new THREE.SphereGeometry(7),
          new THREE.MeshPhongMaterial({
            // color: 0xffffff,
            map: textureMap,
            // shininess: 50,
            side: THREE.DoubleSide,
            transparent: true,
            // opacity: 0.75
          })
        );
      })
      .onNodeDragEnd((node) => {
        node.fx = node.x;
        node.fy = node.y;
        node.fz = node.z;
      })
      .nodeLabel((node) => {
        const texteChoisi = document.querySelector(`[data-id="${node.id}"]`);
        const titleEl = texteChoisi.querySelector('h1');
        const title = titleEl.textContent;
        return `<span class="label-hover">${title}</span>`;
      })
      .onNodeClick((node) => {
        const texteChoisi = document.querySelector(`[data-id="${node.id}"]`);
        // this.graph.enableNavigationControls(false);
        // camPos = this.graph.cameraPosition();
        // const footnoteref = texteChoisi.querySelectorAll('[data-footnote-ref]');
        if (this.textsOpen) {
          this.texts.forEach((el) => {
            el.classList.add('show');
          });
          texteChoisi.scrollIntoView();
          // document.querySelector('.text.show').scrollIntoView();
          this.footnotes.classList.add('show');
          // if (footnoteref.length > 0) {
          //   this.footnotes.classList.add('show');
          //   const listNb = this.footnotes.querySelector('ol');
          //   listNb.start = footnoteref[0].innerHTML;
          //   this.allNotes.forEach((el) => {
          //     el.classList.add('hide');
          //   });
          //   footnoteref.forEach((el) => {
          //     const refNb = el.hash;
          //     const footnoteContent = this.footnotes.querySelector(`[id^="${refNb.substring(1)}"]`);
          //     footnoteContent.classList.remove('hide');
          //   });
          // } else {
          //   this.footnotes.classList.remove('show');
          // }
        } else {
          this.textContainer.classList.add('show');
          this.footnotes.classList.add('show');
          this.texts.forEach((el) => {
            el.classList.add('show');
          });
          texteChoisi.scrollIntoView();
          // texteChoisi.classList.add('show');
          // if (footnoteref.length > 0) {
          //   this.footnotes.classList.add('show');
          //   const listNb = this.footnotes.querySelector('ol');
          //   listNb.start = footnoteref[0].innerHTML;
          //   this.allNotes.forEach((el) => {
          //     el.classList.add('hide');
          //   });
          //   footnoteref.forEach((el) => {
          //     const refNb = el.hash;
          //     const footnoteContent = this.footnotes.querySelector(`[id^="${refNb.substring(1)}"]`);
          //     footnoteContent.classList.remove('hide');
          //   });
          // }
          this.textsOpen = !this.textsOpen;
        }
      })
      .linkThreeObjectExtend(true)
      .linkWidth(0.3)
      .linkOpacity(1)
      .linkResolution(24)
      .linkLabel((node) => `<span class="label-hover">${node.name}</span>`)
      .onLinkClick((link) => {
        let texteChoisi;
        if (parseInt(link.id, 10) < 10) {
          texteChoisi = document.querySelector(`a[data-link="0${link.id}-a"]`);
        } else {
          texteChoisi = document.querySelector(`a[data-link="${link.id}-a"]`);
        }
        // camPos = this.graph.cameraPosition();
        if (this.textsOpen) {
          this.texts.forEach((el) => {
            el.classList.add('show');
          });
          texteChoisi.scrollIntoView();
          this.footnotes.classList.add('show');
        } else {
          this.textContainer.classList.add('show');
          this.footnotes.classList.add('show');
          this.texts.forEach((el) => {
            el.classList.add('show');
          });
          texteChoisi.scrollIntoView();
          this.textsOpen = !this.textsOpen;
        }
      });

    this.graph.controls().minDistance = 0;

    let linkDistance;
    let camDistance;
    if (TOOLS.isTouchDevice()) {
      linkDistance = TOOLS.randomNb(40, 120);
      camDistance = 500;
      this.graph.controls().maxDistance = 1500;
    } else {
      linkDistance = TOOLS.randomNb(80, 320);
      camDistance = 600;
      this.graph.controls().maxDistance = 3000;
    }

    this.graph.d3Force('link').distance(linkDistance);
    this.graph.cameraPosition({ x: 0, y: 0, z: camDistance });

    this.closeBtn = document.querySelector('.texts__close');
    this.closeBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.closeText();
      this.closeBtn.blur();
    });

    window.addEventListener('resize', () => {
      this.resizeGraph();
    });

    document.querySelector('.loading').classList.add('loading--done');
    document.querySelector('body').classList.add('pace--site-loaded');
    //
    // If hash in address
    //
    if (window.location.hash) {
      const hash = window.location.hash.substring(1);
      const texteChoisi = document.querySelector(`#${hash}`);
      texteChoisi.classList.add('selected');
      this.textContainer.classList.add('show');
      this.footnotes.classList.add('show');
      this.texts.forEach((el) => {
        el.classList.add('show');
      });
      texteChoisi.scrollIntoView();
      this.textsOpen = !this.textsOpen;
    }
  }

  resizeGraph() {
    if (this.graph) {
      const height = this.container.clientHeight;
      const width = this.container.clientWidth;
      this.graph.width(width);
      this.graph.height(height);
      const controls = this.graph.controls();
      controls.handleResize();
    }
  }

  textSource() {
    const curvatureMain = 0.1;
    this.texts.forEach((el) => {
      const id = parseInt(el.dataset.id, 10);
      const titleEl = el.querySelector('h1');
      const title = titleEl.textContent;
      const node = {
        id,
        title,
      };
      this.data.nodes.push(node);
    });
    this.texts.forEach((el) => {
      const id = parseInt(el.dataset.id, 10);
      const link = {
        id,
        source: id,
        target: (id < this.texts.length - 1) ? (id + 1) : 0,
        curvature: curvatureMain,
        rotation: 0,
      };
      // this.data.links.push(link);
    });

    this.links.forEach((el) => {
      const chapterNode = el.parentNode.parentNode;
      let chapter;
      const curvatureLink = TOOLS.randombNbFloat(0.1, 1);
      const rotationLink = TOOLS.randombNbFloat(0, 2);
      const linkName = el.textContent;

      if (chapterNode.nodeName === 'DIV') {
        chapter = parseInt(el.parentNode.parentNode.dataset.id, 10);
      } else {
        chapter = parseInt(el.parentNode.parentNode.parentNode.dataset.id, 10);
      }
      const link = el.dataset.link.slice(0, -2);

      const otherLink = document.querySelector(`a[data-link="${link}-b"]`);
      const otherChapter = parseInt(otherLink.parentNode.parentNode.dataset.id, 10);
      const linkObj = {
        id: parseInt(link, 10),
        name: linkName,
        source: chapter,
        target: otherChapter,
        curvature: curvatureLink,
        rotation: Math.PI * 1,
      };
      this.data.links.push(linkObj);
    });

    // this.data = {
    //   // nodes: [...Array(10).keys()].map((i) => ({ id: i })),
    //   links: [
    //     {
    //       source: 0, target: 1, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 1, target: 2, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 2, target: 3, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 3, target: 4, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 4, target: 5, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 5, target: 6, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 6, target: 7, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 7, target: 8, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 8, target: 9, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //     {
    //       source: 9, target: 0, curvature: curvatureNb, rotation: 0, hover: false
    //     },
    //   ]
    // };
  }

  closeText() {
    // this.graph.enableNavigationControls(true);
    document.querySelector('.text.show').scrollIntoView();
    this.textContainer.classList.remove('show');
    this.texts.forEach((el) => {
      el.classList.remove('show');
    });
    this.footnotes.classList.remove('show');
    this.allNotes.forEach((el) => {
      el.classList.remove('hide');
    });
    this.textsOpen = !this.textsOpen;
  }
}
