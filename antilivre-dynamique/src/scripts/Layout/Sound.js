export default class Sound {
  constructor() {
    this.soundBtn = document.querySelector('.button--sound');
    this.soundBtnOn = document.querySelector('.button--sound .button__svg--on');
    this.soundBtnOff = document.querySelector('.button--sound .button__svg--off');
    this.sound = document.querySelector('.sound--background');
    this.vol = 0;
    this.interval = 30;
    this.fadeInAudio = null;
    this.fadeOutAudio = null;
    this.silence = true;

    this.soundBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.soundBtn.blur();
      if (this.silence) {
        this.play(this.sound);
      } else {
        clearInterval(this.fadeInAudio);
        clearInterval(this.fadeOutAudio);
        this.sound.pause();
      }
      this.soundBtnOn.classList.toggle('none');
      this.soundBtnOff.classList.toggle('none');
      this.silence = !this.silence;
    });
  }

  play(track) {
    if (track.paused) {
      clearInterval(this.fadeInAudio);
      clearInterval(this.fadeOutAudio);
      track.volume = this.vol;
      track.play();
      this.fadeInAudio = setInterval(() => {
        if (track.volume < 0.99) {
          track.volume += 0.01;
        } else {
          clearInterval(this.fadeInAudio);
        }
      }, this.interval);
    } else {
      track.pause();
    }
  }
}
