## text extension (e.g. txt, md, markdown).
MEXT = md

## All markdown files in the working directory
# SRC = $(sort $(wildcard texte/introduction/*.$(MEXT))) $(sort $(wildcard texte/partie1/*.$(MEXT))) $(sort $(wildcard texte/partie2/*.$(MEXT))) $(sort $(wildcard texte/partie3/*.$(MEXT))) $(sort $(wildcard texte/conclusion/*.$(MEXT)))
SRC = $(sort $(wildcard texte/*.$(MEXT))) 

## Location of your working bibliography file
BIB = texte/0-bibliographie.bib

## CSL stylesheet (located in the csl folder of the PREFIX directory).
CSL = gabarit/abrupt.csl

# Nom du fichier
NOM = $(shell basename $(CURDIR))

# Template
GABARIT = gabarit/livre.tex


clean:
	latexmk -c && rm -f *.xml

cleanall:
	latexmk -c && rm -f *.xml && rm -f *.html *.idx *.tex *.pdf

html:
	pandoc -s -f markdown -t html --lua-filter gabarit/pandoc-quotes.lua --pdf-engine=lualatex --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --toc --toc-depth=6 -o $(NOM).html $(SRC)

tex:
	pandoc -s -f markdown -t latex --lua-filter gabarit/pandoc-quotes.lua --pdf-engine=lualatex --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --toc --toc-depth=6 --template=$(GABARIT) -o $(NOM).tex $(SRC)

doc:
	pandoc -s --pdf-engine=lualatex --top-level-division=chapter --template=$(GABARIT) --toc --toc-depth=6 -o $(NOM).docx $(SRC)

brut:
	pandoc -s -f markdown -t markdown --lua-filter gabarit/pandoc-quotes.lua --pdf-engine=lualatex --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --toc --toc-depth=6 -o $(NOM).md $(SRC)

epub:
	pandoc -s -f markdown -t epub2 --lua-filter gabarit/pandoc-quotes.lua --epub-cover-image=ebook/couverture.jpg --css=ebook/gabarit/epub.css --epub-embed-font=ebook/gabarit/Crimson-*.otf --template=ebook/gabarit/gabarit.epub2 --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --epub-chapter-level=1 --shift-heading-level-by=0 -o $(NOM).epub $(SRC) && ./ebook/gabarit/modification.sh


pdf:
	pandoc -s -f markdown -t latex --lua-filter gabarit/pandoc-quotes.lua --pdf-engine=lualatex --file-scope --top-level-division=chapter --citeproc --lua-filter gabarit/parentheses.lua --lua-filter gabarit/point-final.lua --lua-filter gabarit/typographie.lua --csl=$(CSL) --bibliography=$(BIB) --toc --toc-depth=6 --template=$(GABARIT) -o $(NOM).pdf $(SRC)

txt:
	pandoc -s -f markdown -t markdown --top-level-division=chapter --toc --toc-depth=6 --atx-headers --wrap=auto -o $(NOM).txt $(SRC)

livre:
	./make.sh
