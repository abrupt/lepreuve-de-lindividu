---
adresse: "https://abrupt.cc/marc-verlynde/lepreuve-de-lindividu"
author: Marc Verlynde
binaire: |
  010000110010011101100101011100110111010000100000011001000010011101101001011000110110100100100000011001000010011101101111110000111011100100100000011010100010011111000011101010010110001101110010011010010111001100101110
date: 2024
depot: premier trimestre
identifier:
- scheme: ISBN-13
  text: 978-3-0361-0219-1
lang: fr
miroir: Nous sommes de ceux qui ne peuvent rester tranquilles dans le vide.
publisher: Abrüpt
reference-section-title: Bibliographie
rights: © 2024 Abrüpt, CC BY-NC-SA
title: L'Épreuve de l'individu
---

