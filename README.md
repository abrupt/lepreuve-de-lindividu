# ~/ABRÜPT/MARC VERLYNDE/L’ÉPREUVE DE L’INDIVIDU/*

La [page de ce livre](https://abrupt.cc/marc-verlynde/lepreuve-de-lindividu/) sur le réseau.

## Sur l'ouvrage

L’individu est métaphore flottante, brouillée. Cet essai en fait l’épreuve. Loin de collecter les preuves de ses manifestations concrètes, Marc Verlynde fait apparaître les figurations de ses évanouissements, de ses virtualités. L’épreuve de l’individu miroite alors quelques-uns des moments de cette modernité plurielle, mais aussi des instants où un individu, dans la conscience de son corps, de sa mort, de son paysage ou des images où il se rêve et se dissipe, apparaît comme une esquisse éperdue d’outrepassement. Dans une écoute des voix et emprunts littéraires (à Kafka et Blanchot, à Caillois et Steiner, à Marías et Sebald, à Baudelaire et Rimbaud, à Tolstoï et Dostoïevski…), L’épreuve de l’individu esquive les silences, les revenances, de l’impossibilité de l’individu.

## Sur l'auteur

Lit souvent, écrit parfois surtout pour s'inventer des [viduités](https://viduite.wordpress.com/).


## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International ([CC-BY-NC-SA 4.0](LICENSE-TXT)).

La version HTML de cet antilivre est placée sous [licence MIT](LICENSE-MIT).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
